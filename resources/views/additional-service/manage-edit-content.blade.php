@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.additional-service.add-and-edit-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
		{!! Form::hidden('status','edit') !!}
		{!! Form::hidden('content_id',$content_id) !!}
		<h4 class="header-text">แก้ไขอาชีพเสริม</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-auto">
							<div class="form-group">
								<label>สีพื้นหลัง </label>
								{!! Form::color('color',old('color',$content->getDetailByTypeDetail('color')['detail_all'] ),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('color',$errors) !!}
							</div>
						</div>
						<div class="col-12">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>หัวข้ออาชีพเสริม (TH)</label>
										{!! Form::text('name_th',old('name_th',$content->name_th),['class'=> 'form-control', 'required' ]) !!}
										{!! customValidator('name_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>หัวข้ออาชีพเสริม (EN)</label>
										{!! Form::text('name_eng',old('name_eng',$content->name_eng),['class'=> 'form-control', 'required' ]) !!}
										{!! customValidator('name_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียดเบื้องต้น (TH)</label>
										{!! Form::textarea('detail_th',old('detail_th',$content->getDetailByTypeDetail('detail')['detail_th'] ),['class'=> 'form-control summernote', 'required' ]) !!}
										{!! customValidator('detail_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียดเบื้องต้น (EN)</label>
										{!! Form::textarea('detail_eng',old('detail_eng',$content->getDetailByTypeDetail('detail')['detail_eng']),['class'=> 'form-control summernote', 'required' ]) !!}
										{!! customValidator('detail_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียด (TH)</label>
										{!! Form::textarea('detail_wysiwyg_th',old('detail_wysiwyg_th',$content->getDetailByTypeDetail('detail_wysiwyg')['detail_th']),['class'=> 'form-control summernote', 'required' ]) !!}
										{!! customValidator('detail_wysiwyg_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียด (EN)</label>
										{!! Form::textarea('detail_wysiwyg_eng',old('detail_wysiwyg_eng',$content->getDetailByTypeDetail('detail_wysiwyg')['detail_eng']),['class'=> 'form-control summernote', 'required' ]) !!}
										{!! customValidator('detail_wysiwyg_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label>Facebook</label>
										{!! Form::text('facebook',old('facebook',$content->getDetailByTypeDetail('facebook')['detail_all']),['class'=> 'form-control ', 'required' ]) !!}
										{!! customValidator('facebook',$errors) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<div class="form-row">
						<div class="col-sm">
							<div class="form-group">
								<label>รูปภาพหน้าปก</label>
								<div class="file-loading">
									<input id="main_image" name="main_image[]" type="file" multiple>
								</div>
								{!! customValidator('main_image',$errors) !!}
							</div>
						</div>
						<div class="col-sm">
							<div class="form-group">
								<label>Hero image</label>
								<div class="file-loading">
									<input id="hero_image" name="hero_image[]" type="file" multiple>
								</div>
								{!! customValidator('hero_image',$errors) !!}
							</div>
						</div>
						<div class="col-12 mt-3">
							<div class="form-group">
								<label>รูปภาพท้ายบทความ</label>
								<div class="file-loading">
									<input id="gallery_image" name="gallery_image[]" type="file" multiple>
								</div>
								{!! customValidator('gallery_image',$errors) !!}
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-auto">
										<label>สไลด์รูปภาพ </label>
									</div>
									<div class="col-auto pr-0">
										<div class="custom-control custom-radio">
											{!! Form::radio('slide', 'slide', $content->getDetailByTypeDetail('slide')['detail_all'] == 'slide', ['id' => 'slide', 'class'=> 'custom-control-input']); !!}
											{!! Form::label('slide', 'ใช่', ['class'=> 'custom-control-label']) !!}
										</div>
									</div>
									<div class="col-auto">
										<div class="custom-control custom-radio">
											{!! Form::radio('slide', 'no slide', $content->getDetailByTypeDetail('slide')['detail_all'] == 'no slide', ['id' => 'no-slide', 'class'=> 'custom-control-input']); !!}
											{!! Form::label('no-slide', 'ไม่', ['class'=> 'custom-control-label']) !!}
										</div>
									</div>
								</div>
								{!! customValidator('slide',$errors) !!}
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
				</div>
			</div>
		{!! Form::close() !!}
	</div>

	<script >
		//  set data
		var additional_service = JSON.parse('<?php print_r(json_encode($additional_service)); ?>');
		now_manu = additional_service.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>

    $('.summernote').summernote({
			height: 200,
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['codeview', 'help']],
			],
			fontNames: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
			fontNamesIgnoreCheck: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
    });


		var images =  JSON.parse('<?php print_r(json_encode($content->images)); ?>');
    var urls_hero_image = [] , preview_configs_hero_image = [];
    var urls_main_image = [] , preview_configs_main_image = [];
    var urls_gallery_image = [] , preview_configs_gallery_image = [];
    for (i = 0; i < images.length; i++) {
			if(images[i]['detail'] == 'hero_image'){
				urls_hero_image[i] = '{{url("/")}}'+'/'+images[i]['path'] ;
				preview_configs_hero_image[i] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls_hero_image[i] , key: images[i]['id'] } ;
			}

			if(images[i]['detail'] == 'main_image'){
				urls_main_image[i] = '{{url("/")}}'+'/'+images[i]['path'] ;
				preview_configs_main_image[i] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls_main_image[i] , key: images[i]['id'] } ;
			}

			if(images[i]['detail'] == 'gallery_image'){
				urls_gallery_image[i] = '{{url("/")}}'+'/'+images[i]['path'] ;
				preview_configs_gallery_image[i] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls_gallery_image[i] , key: images[i]['id'] } ;
			}
    }


    $("#hero_image").fileinput({
			maxFileCount: 1,
			validateInitialCount: true,
			language: 'en',
			initialPreview: urls_hero_image,
			initialPreviewAsData: true,
			initialPreviewConfig: preview_configs_hero_image,
			deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
			deleteExtraData: {
					'_token': $('[name="_token"]').val(), // _token
			},
			fileActionSettings: {
					showDrag: false,
			},
			overwriteInitial: false,
		});

    $("#main_image").fileinput({
			maxFileCount: 1,
			validateInitialCount: true,
			language: 'en',
			initialPreview: urls_main_image,
			initialPreviewAsData: true,
			initialPreviewConfig: preview_configs_main_image,
			deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
			deleteExtraData: {
					'_token': $('[name="_token"]').val(), // _token
			},
			fileActionSettings: {
					showDrag: false,
			},
			overwriteInitial: false,
		});

    $("#gallery_image").fileinput({
			language: 'en',
			initialPreview: urls_gallery_image,
			initialPreviewAsData: true,
			initialPreviewConfig: preview_configs_gallery_image,
			deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
			deleteExtraData: {
					'_token': $('[name="_token"]').val(), // _token
			},
			fileActionSettings: {
					showDrag: false,
			},
			overwriteInitial: false,
		});

</script>
@endsection

