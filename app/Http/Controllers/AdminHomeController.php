<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;
use App\Image ;

class AdminHomeController extends Controller
{
    public function adminHome(){

        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }

        $send_email_contents = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.send_email')->where('name_eng','contact.send_email')->orderBy('id', 'DESC')->get();
        return view('admin-home.index', compact('send_email_contents')); 
    }

    public function deleteContent($content_id){
        Detail::where('content_id',$content_id)->delete();
        Image::where('content_id',$content_id)->delete();
        Content::where('id',$content_id)->delete();
        return redirect('admin/admin-home');  
    }
}
