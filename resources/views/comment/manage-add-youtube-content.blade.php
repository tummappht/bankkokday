@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.comment.add-and-edit-save-youtube-content', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
			{!! Form::hidden('status','add') !!}
			<h4 class="header-text">เพิ่มลิงก์ Youtube</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>ลิงก์ Youtube</label>
								{!! Form::text('path_youtube',old('path_youtube'),['class'=> 'form-control ', 'required' ]) !!}
								{!! customValidator('path_youtube',$errors) !!}
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>คำอธิบาย (TH)</label>
								{!! Form::textarea('detail_youtube_th',old('detail_youtube_th'),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('detail_youtube_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>คำอธิบาย (EN)</label>
								{!! Form::textarea('detail_youtube_eng',old('detail_youtube_eng'),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('detail_youtube_eng',$errors) !!}
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
				</div>
			</div>
		{!! Form::close() !!}
	</div>

	<script >
		//  set data
		var comment = JSON.parse('<?php print_r(json_encode($comment)); ?>');
		now_manu = comment.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>

    $('.summernote').summernote({
			height: 200,
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['codeview', 'help']],
			],
			fontNames: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
			fontNamesIgnoreCheck: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
    });

    $("#files").fileinput({
        language: 'en',
        showCaption: true,
        showPreview: true,
        showRemove: true,
        showUpload: false, // <------ just set this from true to false
        showCancel: true,
    		browseOnZoneClick: true,
        showUploadedThumbs: true
     });

</script>
@endsection

