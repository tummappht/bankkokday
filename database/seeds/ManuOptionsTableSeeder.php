<?php

use Illuminate\Database\Seeder;

class ManuOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('manu_options')->delete();
        DB::table('manu_options')->insert([
            ['id'=>1, 'name_th'=>'หน้าแรก' , 'name_eng'=>'header', 'link'=>'header' , 'detail_th'=>'บางกอกเดย์', 'detail_eng'=>'Bankok Day', 'number_order' => null, 'is_show' => '0' , 'font_id' => '1'],
            ['id'=>2, 'name_th'=>'ความเป็นมา' , 'name_eng'=>'Background', 'link'=>'background' , 'detail_th'=>'บริษัท บางกอกเดย์ กรุ๊ป ก่อตั้งปี 2000', 'detail_eng'=>'Bankok Day make 2000', 'number_order' => '1' , 'is_show' => '1' , 'font_id' => '1'],
            ['id'=>3, 'name_th'=>'มีดีมาโชว์' , 'name_eng'=>'Our Work', 'link'=>'our-work' , 'detail_th'=> null, 'detail_eng'=>null, 'number_order' => '2' , 'is_show' => '1' , 'font_id' => '1'],
            ['id'=>4, 'name_th'=>'ความเห็นจากลูกค้า' , 'name_eng'=>'Why Customers Choose Us', 'link'=>'testimonials', 'detail_th'=> null, 'detail_eng'=>null , 'number_order' => '3' , 'is_show' => '1' , 'font_id' => '1'],
            ['id'=>5, 'name_th'=>'อาชีพเสริม' , 'name_eng'=>'Additional Services', 'link'=>'additional-services' , 'detail_th'=> null, 'detail_eng'=>null , 'number_order' => '4' , 'is_show' => '1' , 'font_id' => '1'],
            ['id'=>6, 'name_th'=>'ติดต่อเรานะ' , 'name_eng'=>'Contact', 'link'=>'contact' , 'detail_th'=> null, 'detail_eng'=>null , 'number_order' => '5' , 'is_show' => '1' , 'font_id' => '1'],
        ]);


    }
}
