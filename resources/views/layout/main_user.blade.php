<!DOCTYPE html>
<html lang="en">

<head>
    @php
        $meta_title = 'Bangkok Day Group';
        $meta_desc = 'Incorporation Story to Design';
        $meta_img = url('images/shared_img.jpg');
    @endphp
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{!! $meta_desc !!}">
    <meta name="author" content="{!! $meta_title !!}">

    <meta property="og:title" content="{!! $meta_title !!}">
    <meta property="og:description" content="{!! $meta_desc !!}">
    <meta property="og:image" content="{!! $meta_img !!}">
    <meta property="og:url" content="{{ url()->full() }}">

    <meta name="twitter:title" content="{!! $meta_title !!}">
    <meta name="twitter:description" content=" {!! $meta_desc !!}">
    <meta name="twitter:image" content=" {!! $meta_img !!}">
    <meta name="twitter:card" content="summary_large_image">

    <title>Bangkok Day Group</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/glyphicons.css')}}">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	  <link rel="stylesheet" href="{{ asset('css/core.css') }}">

    <!-- Bootstrap core JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/intersection-observer@0.7.0/intersection-observer.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	  <script src="{{asset('js/helper.min.js')}}"></script>
    <script src="{{asset('js/lazysizes.min.js')}}" async></script>
    <!-- Menu Toggle Script -->
    @yield('style')
</head>

<script >
  var now_manu = '' ;
</script>

<body>  
    <?php $contact = App\Content::where('contents.manu_option_id', App\ManuOption::$contact_id )->where('name_th','contact.main')->where('name_eng','contact.main')->first(); ?>
    <div id="wrapper" class="main-wrapper">
        @include('layout.sidebar_user')
        <div id="page-content-wrapper">
          @include('layout.navbar_user')
          <!-- Page Content -->
          @yield('content')
          <!-- /#page-content-wrapper -->
        </div>
        @include('layout.footer')
    </div>
  <!-- /#wrapper -->
  @yield('script')
    <!-- set arr php to json js -->
  <script >
    var url = '{{url("/")}}' ;
    var sidebar_manu_option = JSON.parse('<?php print_r(json_encode(App\ManuOption::where('is_show' , '1' )->orderBy('number_order', 'asc')->get())); ?>');
    @if( session('status_manage') != null )
      var status_manage = "{{session('status_manage')}}";
      if(now_manu == 'contact') {
        let text = status_manage;
        if(text.includes('[TO]')){
          var temp = text.split("[TO]");
          if(sessionStorage.getItem("language") == 'th'){
            text = temp[0];
          }else{
            text = temp[1];
          } 
        }
        $('.toast-message').text(text);
        $('.noti-row').addClass('show');
        setTimeout(() => {
          $('.noti-row').removeClass('show');
        }, 5000);
      } else {
        submitNotification('success', status_manage);
      }
      <?php session(['status_manage' => null]); ?>
    @endif
  </script>
  <script src="{{asset('js/set-text-user.js')}}"></script>
</body>

</html>