<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;


class IntroductionController extends Controller
{
    public function index()
    {
        // dd('manageIntroduction');
        $introduction = ManuOption::where('manu_options.id' , ManuOption::$introduction_id )->first();
        $content1 = Content::where('contents.id' , Content::$introduction_contents_id_1 )->first();
        $content2 = Content::where('contents.id' , Content::$introduction_contents_id_2 )->first();
        $detail1 = Detail::where('details.content_id' , Content::$introduction_contents_id_1)->first();
        $detail2 = Detail::where('details.content_id' , Content::$introduction_contents_id_2)->first();
        // dd($introduction->main_image);
        // dd($introduction->contents[0]->detail);
        return view('introduction.show-introduction', compact('introduction','content1','content2','detail1','detail2'));
    }
    //manageIntroduction
    public function manageIntroduction()
    {
        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        // dd('manageIntroduction');
        $introduction = ManuOption::where('manu_options.id' , ManuOption::$introduction_id )->first();
        $fonts = Font::pluck('name', 'id');
        // dd($introduction->contents[0]->detail);
        return view('introduction.manage-introduction', compact('introduction','fonts'));
    }

    public function validatorFilesSave(Request $request){
        $introduction = ManuOption::where('manu_options.id' , ManuOption::$introduction_id )->first();
        if(count($introduction->images) + count($request->file($request->name_input_files) ) > 2 ){
            return back() 
            ->withErrors(['icon'=>'error','title_th'=>'ไม่สามารถเพิ่มรูปได้','text_th'=>'เนื่องจากมีรูปมากกว่า หรือเท่ากับ 2 รูป','title_eng'=>'you can not add images','text_eng' => 'images is full']);
        }else{
            (new HelperController())->filesSave($request);
        }
        return redirect('admin/background'); 
    }

    public function editIntroduction(Request $request){
        $validator = $this->validatorEditIntroduction($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $introduction = ManuOption::where('manu_options.id' , ManuOption::$introduction_id )->first();
        $introduction->detail_th = $request->detail_th ;
        $introduction->detail_eng = $request->detail_eng ;
        // $introduction->font_id = $request->font_id ;
        $introduction->save();
        for ($i=1; $i <= 2; $i++) { 
            $content = Content::where('contents.id' , $request['content_id'.$i])->first();
            $content->name_th = $request['content_name_th'.$i] ;
            $content->name_eng = $request['content_name_eng'.$i] ;
            $content->save();
            $detail = Detail::where('details.content_id' , $request['content_id'.$i])->first();
            $detail->detail_th = $request['content_detail_th'.$i] ;
            $detail->detail_eng = $request['content_detail_eng'.$i] ;
            $detail->save();
        }
        session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        return redirect('admin/background'); 
    }

    public function validatorEditIntroduction(Request $request)
    {
        // dd($request->test_period);
        $validator = Validator::make(
            $request->all(), [
            'detail_th' => 'required',
            'detail_eng' => 'required',
            // 'font_id' => 'required',

            'content_name_eng1' => 'required',
            'content_name_th1' => 'required',
            'content_detail_eng1' => 'required',
            'content_detail_th1' => 'required',

            'content_name_eng2' => 'required',
            'content_name_th2' => 'required',
            'content_detail_eng2' => 'required',
            'content_detail_th2' => 'required',
        ], [
            'detail_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
            'detail_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
            // 'font_id.required' => 'กรุณากรอกเหตุผลการลา[TO]The font field is required.',

            'content_name_eng1.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาอังกฤษ[TO]Sub title ENG field is required.',
            'content_name_th1.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาไทย[TO]Sub title TH field is required.',
            'content_detail_eng1.required' => 'กรุณากรอกเนื้อหาภาษาอังกฤษ[TO]Content field is required.',
            'content_detail_th1.required' => 'กรุณากรอกเนื้อหาภาษาไทย[TO]Content field is required.',

            'content_name_eng2.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาอังกฤษ[TO]Sub title ENG field is required.',
            'content_name_th2.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาไทย[TO]Sub title TH field is required.',
            'content_detail_eng2.required' => 'กรุณากรอกเนื้อหาภาษาอังกฤษ[TO]Content field is required.',
            'content_detail_th2.required' => 'กรุณากรอกเนื้อหาภาษาไทย[TO]Content field is required.',
        ]);
        return $validator;
    }
}
