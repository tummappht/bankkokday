<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image ;
use App\Content;
use App\Font;

class ManuOption extends Model
{
    static $header_id = '1';
    static $introduction_id = '2';
    static $doing_id = '3';
    static $comment_id = '4';
    static $additional_service_id = '5';
    static $contact_id = '6';

    static $default_image_path = '/images/default-image.png' ;

    public function getImagesAttribute()
    {
        $images = Image::where('manu_option_id' , $this->id)->get();

        foreach ($images as $key => $image) {
            $image = defaultImage($image);
        }

        return $images ;
    }

    public function getMainImageAttribute()
    {

        $image = Image::where('manu_option_id' , $this->id)->where('is_main','1')->first();
        
        $image = defaultImage($image);
        return $image ;
    
    }

    public function getNoMainImageAttribute()
    {
        $image = Image::where('manu_option_id' , $this->id)->where('is_main','!=','1')->first();
        // dd($image[0]);
        $image = defaultImage($image);

        return $image ;

    }

    public function getContentsAttribute()
    {
        return Content::where('manu_option_id' , $this->id)->get();
    }

    public function getFontAttribute()
    {
        return Font::where('id' , $this->font_id)->first();
    }

}
