@extends('layout.main_user')

@section('content')

	<div class="container content pt-3 pb-4 doing-container">
		<h1 class="header-text mb-3" id="doing_name"></h1>
		<div class="row row-p10 justify-content-md-center mb-4" id="category-contents-filter"></div>
		<div class="row row-p125 justify-content-center doing-detail">
			<div class="col-md col-lg mb-2 mb-md-0">
				<div class="slick-carousel main-carousel">
					@foreach( $content->images as $image )
					<div class="">
						<img data-src="{{url($image->path)}}" class="lazyload">
					</div>
					@endforeach
				</div>
				<div class="d-none d-md-block d-lg-none mt-3">
					<p class="language-th mb-0 font-SemiBold">{!! $content->name_th !!}</p>
					<p class="language-eng mb-0 font-SemiBold">{!! $content->name_eng !!}</p>
					<div class="row-divider mt-1 mb-3"></div>
					<div class="form-row row-p10 mb-1">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">ที่ตั้ง :</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Location :</p>
						</div>
						<div class="col pl-0">
							<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_th'] !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_eng'] !!}</p>
						</div>
					</div>
					<div class="form-row row-p10">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">ลักษณะงาน :</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Description :</p>
						</div>
						<div class="col pl-0">
							<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_th'] !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_eng'] !!}</p>
						</div>
					</div>
					{{-- <div class="form-row row-p10">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">งบประมาณ:</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Budget:</p>
						</div>
						<div class="col pl-0">
							@php
								$numberth = $content->getDetailByTypeDetail('budget')['detail_th'];
								$numbereng = $content->getDetailByTypeDetail('budget')['detail_eng'];
							@endphp
							<p class="language-th mb-0 font-Light">{!! $numberth !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $numbereng !!}</p>
						</div>
					</div> --}}
				</div>
			</div>
			<div class="col-md-auto col-lg-3 mb-3 slick-row">
				<div class="slick-carousel doing-carousel">
					@foreach( $content->images as $image )
					<div class="sub-image">
						<img data-src="{{url($image->path)}}" class="lazyload">
					</div>
					@endforeach
				</div>
				<div class="d-none d-lg-block doing-text mt-3">
					<p class="language-th mb-0 font-SemiBold">{!! $content->name_th !!}</p>
					<p class="language-eng mb-0 font-SemiBold">{!! $content->name_eng !!}</p>
					<div class="row-divider mt-1 mb-3"></div>
					<div class="form-row row-p10 mb-1">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">ที่ตั้ง :</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Location :</p>
						</div>
						<div class="col pl-0">
							<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_th'] !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_eng'] !!}</p>
						</div>
					</div>
					<div class="form-row row-p10">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">ลักษณะงาน :</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Description :</p>
						</div>
						<div class="col pl-0">
							<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_th'] !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_eng'] !!}</p>
						</div>
					</div>
					{{-- <div class="form-row row-p10">
						<div class="col-auto">
							<p class="language-th mb-0 font-Light label-minwidth">งบประมาณ:</p>
							<p class="language-eng mb-0 font-Light label-minwidth">Budget:</p>
						</div>
						<div class="col pl-0">
							@php
								$numberth = $content->getDetailByTypeDetail('budget')['detail_th'];
								$numbereng = $content->getDetailByTypeDetail('budget')['detail_eng'];
							@endphp
							<p class="language-th mb-0 font-Light">{!! $numberth !!}</p>
							<p class="language-eng mb-0 font-Light">{!! $numbereng !!}</p>
						</div>
					</div> --}}
				</div>
			</div>
			<div class="col-12 d-md-none">
				<p class="language-th mb-2 font-SemiBold">{!! $content->name_th !!}</p>
				<p class="language-eng mb-2 font-SemiBold">{!! $content->name_eng !!}</p>
				<div class="row-divider mt-1 mb-3"></div>
				<div class="form-row row-p10 mb-1">
					<div class="col-auto">
						<p class="language-th mb-0 font-Light label-minwidth">ที่ตั้ง :</p>
						<p class="language-eng mb-0 font-Light label-minwidth">Location :</p>
					</div>
					<div class="col pl-0">
						<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_th'] !!}</p>
						<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('location')['detail_eng'] !!}</p>
					</div>
				</div>
				<div class="form-row row-p10">
					<div class="col-auto">
						<p class="language-th mb-0 font-Light label-minwidth">ลักษณะงาน :</p>
						<p class="language-eng mb-0 font-Light label-minwidth">Description :</p>
					</div>
					<div class="col pl-0">
						<p class="language-th mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_th'] !!}</p>
						<p class="language-eng mb-0 font-Light">{!! $content->getDetailByTypeDetail('work_detail')['detail_eng'] !!}</p>
					</div>
				</div>
				{{-- <div class="form-row row-p10">
					<div class="col-auto">
						<p class="language-th mb-0 font-Light label-minwidth">งบประมาณ:</p>
						<p class="language-eng mb-0 font-Light label-minwidth">Budget:</p>
					</div>
					<div class="col pl-0">
						@php
							$numberth = $content->getDetailByTypeDetail('budget')['detail_th'];
							$numbereng = $content->getDetailByTypeDetail('budget')['detail_eng'];
						@endphp
						<p class="language-th mb-0 font-Light">{!! $numberth !!}</p>
						<p class="language-eng mb-0 font-Light">{!! $numbereng !!}</p>
					</div>
				</div> --}}
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script>
		//  set data
		now_manu = 'doing-detail' ;
		var doing = JSON.parse('{!! json_encode($doing) !!}');
		var category_contents = JSON.parse('{!! json_encode($category_contents) !!}');
		var content_category_content_id = JSON.parse('{!! json_encode($content->category_content_id) !!}');
		content_category_content_id = content_category_content_id == 7 ? 0 : content_category_content_id;

		$(document).ready(function () {
			$('.main-carousel').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				swipeToSlide: true,
				touchThreshold: 100,
				asNavFor: '.doing-carousel',
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 767,
						settings: {
							dots: true,
						}
					},
				]
			});

			$('.doing-carousel').slick({
  			asNavFor: '.main-carousel',
				focusOnSelect: true,
				slidesToShow: 4,
				slidesToScroll: 4,
				swipeToSlide: true,
				touchThreshold: 100,
				vertical: false,
				verticalSwiping: false,
				mobileFirst: true,
				dots: true,
				arrows: false,
				responsive: [
					{
						breakpoint: 576,
						settings: {
        			arrows: true,
							// slidesToShow: 4,
							// slidesToScroll: 4,
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 5,
							slidesToScroll: 5,
							vertical: true,
							verticalSwiping: true,
							dots: false,
        			arrows: true,
						}
					},
				]
			});
			
			setTimeout(() => {
				$('.slick-carousel').slick('slickNext');
			}, 7000);

			let sub_image = $('.sub-image');
			let doing_carousel = $('.doing-carousel');
			if(sub_image.length > 0) {
				$(window).resize(function () {
					set_min_sheight()
				});

				setInterval(() => {
					set_min_sheight();
				}, 250);

				function set_min_sheight() {
					let window_w = $(window).width();
					if(window_w > 576 && window_w < 768) {
						let half = (75/2)
						ah = half - 10
						doing_carousel.find('.slick-arrow').css('top', ah)
						doing_carousel.find('.slick-arrow').css('transform', 'unset')
					}
				}
			}
		});

		
	</script>
@endsection

