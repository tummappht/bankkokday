<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Incorporation Story to Design">
    <meta name="author" content="">

    <title>Bangkok Day Group</title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/glyphicons.css')}}">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="{{ asset('css/core.css') }}">
	  <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @yield('style')

    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	  <script src="{{asset('js/helper.min.js')}}"></script>
</head>

<body>  
  <div class="d-flex admin-page main-wrapper" id="wrapper">
    @include('layout.sidebar_admin')
    <div id="page-content-wrapper">
      @include('layout.navbar_admin')
      <!-- Page Content -->
        @yield('content')
      <!-- /#page-content-wrapper -->
    </div>
    @include('layout.footer_admin')
  </div>
  <!-- /#wrapper -->

  <!-- JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js"></script>
   @yield('script')
  <!-- set arr php to json js -->
  <script >
    var url = '{{url("/")}}' ;
    var sidebar_manu_option = JSON.parse('<?php print_r(json_encode(App\ManuOption::orderBy('number_order', 'asc')->get())); ?>');
    @if( session('status_manage') != null )
      var status_manage = "{{session('status_manage')}}";
      submitNotification('success', status_manage);
      <?php session(['status_manage' => null]); ?>
    @endif
  </script>
  <script src="{{asset('js/set-text-admin.js')}}"></script>
</body>

</html>