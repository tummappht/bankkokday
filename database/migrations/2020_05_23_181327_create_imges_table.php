<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->text('path')->nullable()->comment('path รูป');
            $table->string('is_main')->nullable()->comment('ถ้ามากกว่า2รูป ให้เลือกรูป 0 or 1');
            // $table->string('number_role')->nullable()->comment('ต้องการโชรูปตามลำดับ');
            $table->unsignedBigInteger('manu_option_id')->nullable()->comment('manu ที่ใช้รูปนี้');
            $table->foreign('manu_option_id')->references('id')->on('manu_options')->onDelete('cascade');
            
            $table->unsignedBigInteger('content_id')->nullable()->comment('content ที่ใช้รูปนี้');
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
