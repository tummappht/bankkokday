<nav class="header navbar navbar-expand-md">
	<div class="container">
		<button id="sidebarCollapse" class="btn btn-sidenav-toggle" aria-label="hamberger">
			{!! file_get_contents('images/icons/icon-hamberger.svg') !!}
		</button>
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav ml-auto align-items-center">
				<li class="nav-item">
					<a class="nav-link bt_language_eng btn"></a>
				</li>
				<span>|</span>
				<li class="nav-item">
					<a class="nav-link bt_language_th btn"></a>
				</li>
			</ul>
		</div>
	</div>
</nav>