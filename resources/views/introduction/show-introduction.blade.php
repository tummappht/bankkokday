@extends('layout.main_user')

@section('content')
	<div class="container content about-us pt-3">
		<h1 class="header-text" id="introduction_name"></h1>
		<div class="row">
			<div class="col-lg">
				<div class="form-row">
					<div class="col-lg pr-lg-0 mb-3 mb-sm-0">
						<img data-src="{{$introduction->main_image->path}}" class="img-lg lazyload">
					</div>
					<div class="col-sm-auto d-sm-none d-lg-block">
						<img data-src="{{$introduction->no_main_image->path}}" class="img-sm lazyload">
					</div>
				</div>
			</div>
			<div class="col-md col-lg-4 about-us-text d-flex align-items-center pt-3 py-lg-0">
				<div class="w-100">
					<img data-src="{{$introduction->no_main_image->path}}" class="img-sm d-none d-sm-block d-lg-none lazyload">
					<h6 id="introduction_detail" class="mb-4"></h6>
					<h6 id="content_1"></h6>
					<div class="row-divider"></div>
					<div class="language-th mb-3">{!! $detail1['detail_th'] !!}</div>
					<div class="language-eng mb-3">{!! $detail1['detail_eng'] !!}</div>
					<h6 id="content_2"></h6>
					<div class="row-divider"></div>
					<div class="language-th">{!! $detail2['detail_th'] !!}</div>
					<div class="language-eng">{!! $detail2['detail_eng'] !!}</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
    <script>
        //  set data
        var introduction = JSON.parse('<?php print_r(json_encode($introduction)); ?>');
        var content1 = JSON.parse('<?php print_r(json_encode($content1)); ?>');
        var content2 = JSON.parse('<?php print_r(json_encode($content2)); ?>');
        // var detail1 = JSON.parse('<?php print_r(json_encode($detail1)); ?>');
        // var detail2 = JSON.parse('<?php print_r(json_encode($detail2)); ?>');
        now_manu = introduction.link ;
    </script>
@endsection

