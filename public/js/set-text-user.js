console.log(sessionStorage.getItem('language'))

if (sessionStorage.getItem('language') == null) {
    sessionStorage.setItem('language', 'th'); // set thai
    setTextByLanguage(sessionStorage.getItem('language'));
} else {
    setTextByLanguage(sessionStorage.getItem('language'));
}

$('.bt_language_th').click(function () {
    sessionStorage.setItem('language', 'th'); // set thai
    setTextByLanguage(sessionStorage.getItem('language'));
    $('.header .nav-item').removeClass('active');
    $(this).parents('.nav-item').addClass('active');
});

$('.bt_language_eng').click(function () {
    sessionStorage.setItem('language', 'eng'); // set en
    setTextByLanguage(sessionStorage.getItem('language'));
    $('.header .nav-item').removeClass('active');
    $(this).parents('.nav-item').addClass('active');
});

function setTextByLanguage(language) {
    setNavbarUser();
    setSidebarUser();
    setFooterUser();
    if (now_manu == 'header') {
        setHeaderUser();
        setCategoryContentsUser(content_category_content_id);
    }
    if (now_manu == 'background') {
        setIntroductionUser();
        setIanguageByDivUser();
    }
    if (now_manu == 'our-work') {
        setDoingUser();
        setCategoryContentsUser(content_category_content_id);
    }
    if (now_manu == 'doing-detail') {
        setDoingUser();
        setIanguageByDivUser();
        setCategoryContentsUser(content_category_content_id);
    }
    if (now_manu == 'testimonials') {
        setIanguageByDivUser();
    }
    if (now_manu == 'additional-services') {
        setIanguageByDivUser();
    }
    if (now_manu == 'additional-service-detail') {
        setIanguageByDivUser();
    }
    if (now_manu == 'contact') {
        setIanguageByDivUser();
        setContactUser();
    }
}

function setNavbarUser() {
    let lang = sessionStorage.getItem('language');
    $(`.bt_language_${lang}`).parents('.nav-item').addClass('active')
    $('.bt_language_th').text('ไทย')
    $('.bt_language_eng').text('English')
    if (lang == 'th') { //th
        $('#bt_login').text('เข้าสู่ระบบ')
        $('#bt_register').text('สมัครสมาชิก')
    } else { //en
        $('#bt_login').text('Login')
        $('#bt_register').text('Register')
    }
}

function setSidebarUser() {
    if (sidebar_manu_option != null) {
        let lang = sessionStorage.getItem('language');
        let a = '';
        for (i = 0; i < sidebar_manu_option.length; i++) {
            a += '<li>'
            a += `<a href="${url}/${sidebar_manu_option[i]['link']}">${sidebar_manu_option[i]['name_' + lang]}</a>`;
            a += '</li>'
        }
        $('#dav_manu_option').html(a);
    }
}

function setFooterUser() {
    if (sidebar_manu_option != null) {
        let lang = sessionStorage.getItem('language');
        let a = '';
        for (i = 0; i < sidebar_manu_option.length; i++) {

            if (i != 0) {
                a += icon_dash;
            }

            a += '<div class="col-auto">';
            a += `<a href="${url + '/' + sidebar_manu_option[i]['link']}">${sidebar_manu_option[i]['name_' + lang]}</a>`;
            a += '</div>';

        }
        $('#footer-menu').html(a);
    }
}

// content

function setHeaderUser() {
    // $('#header_img').attr("src",header['path']);
    $('#header_detail').text(header['detail_' + sessionStorage.getItem("language")]);
    if (now_manu == 'header') {
        $('.header').addClass('position-fixed');
    }

    if (sessionStorage.getItem("language") == 'th') {
        $('.language-th').show();
        $('.language-eng').hide();
    } else {
        $('.language-th').hide();
        $('.language-eng').show();
    }

}

function setIntroductionUser() {
    $('#introduction_name').text(introduction['name_' + sessionStorage.getItem("language")]);
    $('#introduction_detail').text(introduction['detail_' + sessionStorage.getItem("language")]);
    $('#content_1').text(content1['name_' + sessionStorage.getItem("language")]);
    $('#content_2').text(content2['name_' + sessionStorage.getItem("language")]);
}

function setDoingUser() {
    $('#doing_name').text(doing['name_' + sessionStorage.getItem("language")]);
    // setCategoryContentsUser(null);
    setIanguageByDivUser();
}

function setIanguageByDivUser() {
    if (sessionStorage.getItem("language") == 'th') {
        $('.language-th').show();
        $('.language-eng').hide();
    } else {
        $('.language-th').hide();
        $('.language-eng').show();
    }
}


function setCategoryContentsUser(select_content_category_content_id) {
    if (category_contents != null) {
        let lang = sessionStorage.getItem('language');
        let a = '';
        let divider = '';
        divider += '<div class="col-auto">';
        divider += '<div class="col-divider">|</div>';
        divider += '</div>';
        for (i = 0; i < category_contents.length; i++) {
            let active = select_content_category_content_id == category_contents[i]['id'] ? 'active font-Regular' : '';
            if (i != 0) {
                a += divider;
            }
            
            a += '<div class="col-auto  font-italic font-SemiBold text-14px">';
            a += `<a href="${url + '/our-work-filter/' + category_contents[i]['id']}" class="${active}">${category_contents[i]['name_' + lang]}</a>`;
            a += '</div>';
        }
        $('#category-contents-filter').html(a);
    }
}

function setContactUser() {
    if (sessionStorage.getItem("language") == 'th') {
        $("[name='name']").attr("placeholder", "ชื่อ นามสกุล");
        $("[name='email']").attr("placeholder", "อีเมล");
        $("[name='tel']").attr("placeholder", "เบอร์ติดต่อ");
        $("[name='detail']").attr("placeholder", "รายละเอียด");
        $("#bt-send").val('ส่งข้อความ');
    } else {
        $("[name='name']").attr("placeholder", "Name");
        $("[name='email']").attr("placeholder", "Email");
        $("[name='tel']").attr("placeholder", "Tel");
        $("[name='detail']").attr("placeholder", "Details");
        $("#bt-send").val('Send Email');

    }

}
