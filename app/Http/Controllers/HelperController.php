<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image ;
use File ;
use App\Content;
use App\Detail;

class HelperController extends Controller
{
    public function fileDelete(Request $request){
        
        $obj = Image::where('id' , $request->key ) ;
        if(File::exists($obj->first()->path)) {
            File::delete($obj->first()->path);
        }
        return $obj->delete();
    }

    public function filesSave(Request $request ){ 
        // dd($request->is_main);
        // dd(Image::where('content_id',$request->content_id)->where('is_main',1)->first(),$request);
        //************************ request *************************/
        // name_input_files is ชื่อ input
        // name_file is ชือ folder ที่อยู่ใน public
        //*********************************************************/
        // dd($request ,$request->file($request->name_input_files));
        $name_images = [] ;
        // dd(  $request->file($name)[0] );
        if($request->file($request->name_input_files) != null){
            for ($i=0; $i < count($request->file($request->name_input_files)) ; $i++) {
                # code...
                if($request->hasFile($request->name_input_files)){ //save image
                    if( $request->file($request->name_input_files)[$i]->getMimeType() == "image/jpeg" ){
                        $image_path =  $request->name_file.'/'.time().$i.rand().'.jpg';
                    }else{
                        $image_path =  $request->name_file.'/'.time().$i.rand().'.png';
                    }
                    // dd(public_path(),$request->name_file);
                    $request->file($request->name_input_files)[$i]->move('/home/customer/www/bangkokdaygroup.com/public_html/'.$request->name_file, $image_path); // server?
                    // $request->file($request->name_input_files)[$i]->move(public_path($request->name_file), $image_path); //http://localhost/
                    //save Image
                    $image = new Image() ;
                    if($request->manu_option_id != null){   
                        $image->manu_option_id = $request->manu_option_id ;
                    }else{
                        $image->content_id = $request->content_id  ;
                    }

                    if($request->is_main == 1 || Image::where('manu_option_id',null)->where('content_id',$request->content_id)->where('is_main',1)->first() == null ){// default main
                        $image->is_main = 1 ;
                    }else{
                        $image->is_main = 0 ;
                    }

                    if($request->name_file == 'images/comment'){// comment
                        if($request->is_main == 1 ){ 
                            $image->is_main = 1 ;
                        }else{
                            $image->is_main = 0 ;
                        }
                    }


                    if($request->name_file == 'images/introduction'){// introduction
                        if($request->name_file == 'images/introduction' && Image::where('manu_option_id','2')->where('is_main',1)->first() == null){ 
                            $image->is_main = 1 ;
                        }else{
                            $image->is_main = 0 ;
                        }
                    }

                    $image->path = $image_path ;
                    if($request->detail_image){
                        $image->detail = $request->detail_image ;
                    }
                    
                    $image->save();
                    
                }else{
                    $image_path = null ;
                }
                
                // array_push($name_images,$image_path);
            }
        }
        
        unset($request->name_input_files);
        unset($request->name_file);
        unset($request->content_id);
        unset($request->detail_image);
        session(['status_manage' => 'เพิ่มรูปสำเร็จ']);
        return back() ;
    }

    public function contentSave($request , $manu_option_id ){
        if($request->status == 'add'){ $content = new Content(); }else{ $content = Content::where('id',$request->content_id)->first(); }

        $content->name_th = $request->name_th ;
        $content->name_eng = $request->name_eng ;
        $content->manu_option_id = $manu_option_id ;
        $content->save();
        return $content->id ;
    }
        
    public function detailSave($request , $content_id , $type_detail , $st_th_and_eng){
        if($request->status == 'add'){ $detail = new Detail(); }else{ $detail = Detail::where('content_id',$request->content_id)->where('type_detail', $type_detail )->first(); }
        if($detail == null && $request->status != 'add' ){ $detail = new Detail();  }
        $detail->content_id = $content_id;
        $detail->type_detail = $type_detail ;
        if($st_th_and_eng == 'th-and-eng'){
            $detail->detail_th =  $request[$type_detail.'_th'];
            $detail->detail_eng =  $request[$type_detail.'_eng'];
            unset($request[$type_detail.'_th']);
            unset($request[$type_detail.'_eng']);
        }else{
            $detail->detail_all = $request[$type_detail];
            unset($request[$type_detail]);
        }

        $detail->save();
    }
}
