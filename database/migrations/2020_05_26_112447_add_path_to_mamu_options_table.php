<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPathToMamuOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manu_options', function (Blueprint $table) {
            $table->string('link')->nullable()->comment('ชือลิงก์');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manu_options', function (Blueprint $table) {
            $table->dropColumn('link');
        });
    }
}
