<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;
use App\Image ;

class AdditionalServiceController extends Controller
{
    public function index(){

        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$additional_service_id )->orderByDesc('id')->paginate(8);
        // $contents = Content::where('contents.manu_option_id', ManuOption::$additional_service_id )->orderByDesc('id')->get();
        return view('additional-service.show-additional-service', compact('additional_service','contents'));
    }

    public function detail($content_id){
        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        $content = Content::where('id', $content_id )->first();
        return view('additional-service.detail', compact('additional_service','content'));
    }

    public function manageAdditionalService()
    {
        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        // dd('manageAdditionalService');
        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$additional_service_id )->get();
        $fonts = Font::pluck('name', 'id');

        return view('additional-service.manage-additional-service', compact('additional_service','fonts','contents'));
    }

    public function editAdditionalService(Request $request){
        // $validator = $this->validatorEditAdditionalService($request);
        // if ($validator->errors()->messages()) {
        //     return back()
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        // $additional_service->font_id = $request->font_id;
        $additional_service->save();

        return redirect('admin/additional-services'); 
    }

    // public function validatorEditAdditionalService($request){
    //     $validator = Validator::make(
    //         $request->all(), [
    //         'font_id' => 'required',
    //     ], [
    //         'font_id.required' => 'กรุณาเลือก font[TO]The font field is required.',
    //     ]);
    //     return $validator;
    // }

    public function addContent(){
        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$additional_service_id )->get();
        return view('additional-service.manage-add-content', compact('additional_service','contents'));
    }

    public function addAndEditSaveContent(Request $request){
        $validator = $this->validatorAddAndEditSaveContent($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $content_id = (new HelperController())->contentSave($request , ManuOption::$additional_service_id );

        (new HelperController())->detailSave($request , $content_id , 'detail' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'detail_wysiwyg' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'color' , 'no-th-and-eng');
        
        (new HelperController())->detailSave($request , $content_id , 'slide' , 'no-th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'facebook' , 'no-th-and-eng');

        $request->name_input_files = 'hero_image' ;
        $request->name_file = 'images/additional_service' ;
        $request->content_id = $content_id ;
        $request->detail_image = 'hero_image' ;
        (new HelperController())->filesSave($request);

        $request->name_input_files = 'main_image' ;
        $request->name_file = 'images/additional_service' ;
        $request->content_id = $content_id ;
        $request->detail_image = 'main_image' ;
        (new HelperController())->filesSave($request);

        $request->name_input_files = 'gallery_image' ;
        $request->name_file = 'images/additional_service' ;
        $request->content_id = $content_id ;
        $request->detail_image = 'gallery_image' ;
        (new HelperController())->filesSave($request);
        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        }
        return redirect('admin/additional-services/'.$content_id.'/edit-content'); 
        // return redirect('admin/additional-service'); 
    }

    public function editContent($content_id){
        // dd($content_id);
        $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        $content = Content::where('id',$content_id)->first();
        // dd( Detail::where('content_id',$content_id)->where('type_detail','detail_youtube')->first() );
        return view('additional-service.manage-edit-content', compact('additional_service','content','content_id'));
    }

    public function validatorAddAndEditSaveContent($request){
        if($request->status == 'add'){
            $validator = Validator::make(
                $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'detail_th' => 'required',
                'detail_eng' => 'required',
                'detail_wysiwyg_th' => 'required',
                'detail_wysiwyg_eng' => 'required',
                'hero_image' => 'required',
                'main_image' => 'required',
                'gallery_image' => 'required',
                'color' => 'required',
                'slide' => 'required',
                
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'detail_th.required' => 'กรุณากรอกคำอธิบายภาษาไทย[TO]Content field is required.',
                'detail_eng.required' => 'กรุณากรอกคำอธิบายภาษาอังกฤษ[TO]Content field is required.',
                'detail_wysiwyg_th.required' => 'กรุณากรอกคำอธิบาย WYSIWYG ภาษาไทย[TO]Content field is required.',
                'detail_wysiwyg_eng.required' => 'กรุณากรอกคำอธิบาย WYSIWYG ภาษาอังกฤษ[TO]Content field is required.',
                'hero_image.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
                'main_image.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
                'gallery_image.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
                'color.required' => 'กรุณาเลือกสี[TO]Content field is required.',
                'slide.required' => 'กรุณาเลือกสไลค์[TO]Content field is required.',
            ]);
        }else{
            $validator = Validator::make(
            $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'detail_th' => 'required',
                'detail_eng' => 'required',
                'detail_wysiwyg_th' => 'required',
                'detail_wysiwyg_eng' => 'required',
                'color' => 'required',
                'slide' => 'required',
        
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'detail_th.required' => 'กรุณากรอกคำอธิบายภาษาไทย[TO]Content field is required.',
                'detail_eng.required' => 'กรุณากรอกคำอธิบายภาษาอังกฤษ[TO]Content field is required.',
                'detail_wysiwyg_th.required' => 'กรุณากรอกคำอธิบาย WYSIWYG ภาษาไทย[TO]Content field is required.',
                'detail_wysiwyg_eng.required' => 'กรุณากรอกคำอธิบาย WYSIWYG ภาษาอังกฤษ[TO]Content field is required.',
                'color.required' => 'กรุณาเลือกสี[TO]Content field is required.',
                'slide.required' => 'กรุณาเลือกสไลค์[TO]Content field is required.',
            ]);
        }

        return $validator ;
    }

    public function deleteContent($content_id){
        Detail::where('content_id',$content_id)->delete();
        Image::where('content_id',$content_id)->delete();
        Content::where('id',$content_id)->delete();
        return redirect('admin/additional-services');  
    }
    
}
