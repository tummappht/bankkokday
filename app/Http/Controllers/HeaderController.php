<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;

class HeaderController extends Controller
{
    public function index()
    {
        // dd(\Illuminate\Support\Facades\Auth::user());
        //header
        // $header = ManuOption::where('manu_options.id' , ManuOption::$header_id )->first();

        // //introduction
        // $introduction = ManuOption::where('manu_options.id' , ManuOption::$introduction_id )->first();
        // $introduction_content1 = Content::where('contents.id' , Content::$introduction_contents_id_1 )->first();
        // $introduction_content2 = Content::where('contents.id' , Content::$introduction_contents_id_2 )->first();
        // $introduction_detail1 = Detail::where('details.content_id' , Content::$introduction_contents_id_1)->first();
        // $introduction_detail2 = Detail::where('details.content_id' , Content::$introduction_contents_id_2)->first();      
        
        // //doing
        // $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        // $doing_contents = Content::where('contents.manu_option_id', ManuOption::$doing_id )->paginate(8);
        // $doing_category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->get();

        // //comment
        // $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        // $comment_customer_contents = Content::where('contents.manu_option_id', ManuOption::$comment_id )
        // ->where('name_th','comment.customer')
        // ->where('name_eng','comment.customer')
        // ->inRandomOrder()
        // ->take(3)
        // ->get();    
        // $comment_customer_youtubes = Content::where('contents.manu_option_id', ManuOption::$comment_id )
        // ->where('name_th','comment.youtube')
        // ->where('name_eng','comment.youtube')
        // ->inRandomOrder()
        // ->take(3)
        // ->get();   

        // //additional_service
        // $additional_service = ManuOption::where('manu_options.id' , ManuOption::$additional_service_id )->first();
        // $additional_service_contents = Content::where('contents.manu_option_id', ManuOption::$additional_service_id )->paginate(8); 
        
        // return view('header.show-header', compact('header'));
        
        return view('header.show-header');
    }

    public function manageHeader(){

        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }

        $header = ManuOption::where('manu_options.id' , ManuOption::$header_id )->first();
        $fonts = Font::pluck('name', 'id');

        return view('header.manage-header', compact('header','fonts'));
    }

    public function editHeader(Request $request){
        $validator = $this->validatorEditHeader($request);
        if ($validator->errors()->messages()) {
            // dd($validator->errors()->messages());
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $header = ManuOption::where('manu_options.id' , $request->manu_option_id )->first();
        $header->detail_th = $request->detail_th ;
        $header->detail_eng = $request->detail_eng ;
        // $header->font_id = $request->font_id ;
        $header->save();

        for ($i=1; $i <= 3; $i++) { 
            $content = Content::where('contents.id' , $request['content_id'.$i])->first();
            // $content->name_th = $request['content_name_th'.$i] ;
            // $content->name_eng = $request['content_name_eng'.$i] ;
            $content->save();
            $detail = Detail::where('details.content_id' , $request['content_id'.$i])->first();
            $detail->detail_th = $request['content_detail_th'.$i] ;
            $detail->detail_eng = $request['content_detail_eng'.$i] ;
            $detail->save();
        }
        session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        return redirect('admin/header'); 
       
    }

    public function validatorEditHeader(Request $request)
    {
        // dd($request->test_period);
        $validator = Validator::make(
            $request->all(), [
            'detail_th' => 'required',
            'detail_eng' => 'required',
            // 'font_id' => 'required',

            // 'content_name_eng1' => 'required',
            // 'content_name_th1' => 'required',
            'content_detail_eng1' => 'required',
            'content_detail_th1' => 'required',

            // 'content_name_eng2' => 'required',
            // 'content_name_th2' => 'required',
            'content_detail_eng2' => 'required',
            'content_detail_th2' => 'required',

            // 'content_name_eng3' => 'required',
            // 'content_name_th3' => 'required',
            'content_detail_eng3' => 'required',
            'content_detail_th3' => 'required',
        ], [
            'detail_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
            'detail_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
            // 'font_id.required' => 'กรุณาเลือก font[TO]The font field is required.',

            // 'content_name_eng1.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาอังกฤษ[TO]Sub title ENG field is required.',
            // 'content_name_th1.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาไทย[TO]Sub title TH field is required.',
            'content_detail_eng1.required' => 'กรุณากรอกเนื้อหาภาษาอังกฤษ[TO]Content field is required.',
            'content_detail_th1.required' => 'กรุณากรอกเนื้อหาภาษาไทย[TO]Content field is required.',

            // 'content_name_eng2.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาอังกฤษ[TO]Sub title ENG field is required.',
            // 'content_name_th2.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาไทย[TO]Sub title TH field is required.',
            'content_detail_eng2.required' => 'กรุณากรอกเนื้อหาภาษาอังกฤษ[TO]Content field is required.',
            'content_detail_th2.required' => 'กรุณากรอกเนื้อหาภาษาไทย[TO]Content field is required.',

            // 'content_name_eng3.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาอังกฤษ[TO]Sub title ENG field is required.',
            // 'content_name_th3.required' => 'กรุณากรอกชื่อหัวข้อย่อยภาษาไทย[TO]Sub title TH field is required.',
            'content_detail_eng3.required' => 'กรุณากรอกเนื้อหาภาษาอังกฤษ[TO]Content field is required.',
            'content_detail_th3.required' => 'กรุณากรอกเนื้อหาภาษาไทย[TO]Content field is required.',

        ]);
        return $validator;
    }
}
