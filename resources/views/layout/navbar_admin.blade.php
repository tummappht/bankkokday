<nav class="header navbar px-0">
	<div class="container-fluid">
		<button id="sidebarCollapse" class="btn btn-sidenav-toggle d-md-none">
			{!! file_get_contents('images/icons/icon-hamberger.svg') !!}
		</button>
		<ul class="navbar-nav ml-auto align-items-center">
			<li class="nav-item d-flex justify-content-end">
				<a class="nav-link" href="{{ route('logout') }}" id ="bt_logout">{{ __('Logout') }}</a>
			</li>
		</ul>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
		</form>
	</div>
</nav>

<script>
	$(document).ready(function () {
		$('#bt_logout').click(function() {
			event.preventDefault();
			$('#logout-form').submit();
		});
	});
</script>