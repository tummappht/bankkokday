@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">จัดการติดต่อเรา</h4>
		<div class="card">
			<div class="card-body">
				{!! Form::open(['route'=>'admin.contact.edit', 'method', 'POST', 'class' => 'form']) !!}
					{!! Form::hidden('content_id',$content->id) !!}
					<div class="row">
						<div class="col-12">
							<h5>จัดการติดต่อเรา</h5>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>ลิงก์แผนที่</label>
								{!! Form::text('link_map',old('link_map', $content->getDetailByTypeDetail('link_map')['detail_all']),['class'=> 'form-control' ]) !!}
								{!! customValidator('link_map',$errors) !!}
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>ที่อยู่ (TH)</label>
								{!! Form::text('address_th',old('address_th', $content->getDetailByTypeDetail('address')['detail_th']),['class'=> 'form-control' ]) !!}
								{!! customValidator('address_th',$errors) !!}
							</div>
							<div class="form-group">
								<label>ที่อยู่ (EN)</label>
								{!! Form::text('address_eng',old('address_eng', $content->getDetailByTypeDetail('address')['detail_eng']),['class'=> 'form-control' ]) !!}
								{!! customValidator('address_eng',$errors) !!}
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Facebook Link</label>
								{!! Form::text('facebook',old('facebook', $content->getDetailByTypeDetail('facebook')['detail_all']),['class'=> 'form-control' ]) !!}
								{!! customValidator('facebook',$errors) !!}
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Line Link</label>
								{!! Form::text('line',old('line', $content->getDetailByTypeDetail('line')['detail_all']),['class'=> 'form-control' ]) !!}
								{!! customValidator('line',$errors) !!}
								<p class="m-0 text-12px">ลิงก์สำหรับ Add LINE ID ของผู้ใช้ทั่วไป > http://line.me/ti/p/~<span class="text-danger font-SemiBold">ID LINE ของคุณ</span></p>
								<p class="m-0 text-12px">ลิงก์สำหรับ Add LINE@ (ไลน์แอท) ของเพจหรือร้านค้าบน LINE > http://line.me/ti/p/%40<span class="text-danger font-SemiBold">ID LINE ของคุณ</span></p>
							</div>
						</div>
						<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-end">
					<a href="#" class="btn btn-secondary d-flex mb-3" data-toggle="modal" data-target="#modal_add_email">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่มอีเมลติดต่อ
					</a>
				</div>
				<table class="table table-striped table-with-image w-100 table-responsive">
					<thead>
						<th>ชื่อ</th>
						<th>อีเมลติดต่อ</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($email_contents as $email_content)
						<tr>
							<td>
								{{$email_content->getDetailByTypeDetail('name')['detail_th']}}
								<br>
								{{$email_content->getDetailByTypeDetail('name')['detail_eng']}}
							</td>
							<td>{!! $email_content->getDetailByTypeDetail('email')['detail_all'] !!}</td>
							<td class="d-flex">
							<a href="#" class="btn btn-light border border-dark mr-2" data-toggle="modal" data-target="#modal_edit_email" 
							data-content_id="{{$email_content->id}}" 
							data-name_th="{{$email_content->getDetailByTypeDetail('name')['detail_th']}}" 
							data-name_eng="{{$email_content->getDetailByTypeDetail('name')['detail_eng']}}" 
							data-email="{!! $email_content->getDetailByTypeDetail('email')['detail_all'] !!}">
								{{-- <a href="{{url('admin/contact/'.$email_content->id.'/edit-email-content')}}" class="btn btn-light border border-dark mr-2"> --}}
									<i class="far fa-edit"></i>
								</a>
								<a  href="#" onclick="deleteByURL('{{url('admin/contact/')}}' , '{{$email_content->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-end">
					<a href="{{url('/admin/contact/add-content-qr')}}" class="btn btn-secondary d-flex mb-3">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่ม QR Code
					</a>
				</div>
				<table class="table table-striped table-with-image w-100 table-responsive">
					<thead>
						<th>ชื่อ</th>
						<th>ประเภท</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($qr_contents as $qr_content)
						<tr>
							<td>
								{{$qr_content->getDetailByTypeDetail('name')['detail_th']}}
								<br>
								{{$qr_content->getDetailByTypeDetail('name')['detail_eng']}}
							</td>
							<td>{!! $qr_content->getDetailByTypeDetail('type')['detail_all'] !!}</td>
							<td class="d-flex">
								<a href="{{url('admin/contact/'.$qr_content->id.'/edit-qr-content')}}" class="btn btn-light border border-dark mr-2">
									<i class="far fa-edit"></i>
								</a>
								<a  href = "#" onclick="deleteByURL('{{url('admin/contact/')}}' , '{{$qr_content->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<!-- <div class="card">
			<div class="card-body">
				<table class="table table-striped table-with-image w-100 table-responsive">
					<thead>
						<th>ชื่อ</th>
						<th>อีเมล์</th>
						<th>เบอร์ติดต่อ</th>
						<th>สถานะ</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($send_email_contents as $send_email_content)
						<tr>
							<td>
								{{$send_email_content->getDetailByTypeDetail('name')['detail_all']}}
							</td>
							<td>
								{{$send_email_content->getDetailByTypeDetail('email')['detail_all']}}
							</td>
							<td>
								{{$send_email_content->getDetailByTypeDetail('tel')['detail_all']}}
							</td>
							<td>
								@if( $send_email_content->getDetailByTypeDetail('is_read')['detail_all'] == '0')
									ยังไม่ได้อ่าน
								@else
									อ่านแล้ว
								@endif
							</td>
							<td class="d-flex">
								<a href="{{url('admin/contact/'.$send_email_content->id.'/detail-send-email-content')}}" class="btn btn-light border border-dark mr-2">
									<i class="fa fa-search"></i>
								</a>
								<a  href = "#" onclick="deleteByURL('{{url('admin/contact/')}}' , '{{$send_email_content->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div> -->
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal_add_email" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">เพิ่มอีเมลติดต่อ</h5>
				</div>
				<div class="modal-body">
        {!! Form::open(['id' => 'form_add_email', 'route'=>'admin.contact.add-and-edit-email-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
					{!! Form::hidden('status','add') !!}
					<div class="form-group">
						<label for="">ชื่อ (TH)</label>
						{!! Form::text('name_th',old('name_th'),['class'=> 'form-control' ]) !!}
						{!! customValidator('name_th',$errors) !!}
					</div>
					<div class="form-group">
						<label for="">ชื่อ (EN)</label>
						{!! Form::text('name_eng',old('name_eng'),['class'=> 'form-control' ]) !!}
						{!! customValidator('name_eng',$errors) !!}
					</div>
					<div class="form-group">
						<label for="">อีเมล</label>
						{!! Form::text('email',old('email'),['class'=> 'form-control ' ]) !!}
						{!! customValidator('email',$errors) !!}
					</div>
        {!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="reset" form="form_add_email" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_add_email" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal_edit_email" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">แก้ไขอีเมลติดต่อ</h5>
				</div>
				<div class="modal-body">
        {!! Form::open(['id' => 'form_edit_email', 'route'=>'admin.contact.add-and-edit-email-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
        {!! Form::open(['id' => 'form_edit_email', 'route'=>'admin.contact.add-and-edit-email-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
					{!! Form::hidden('status','edit') !!}
					{!! Form::hidden('content_id') !!}
					<div class="form-group">
						<label for="">ชื่อ (TH)</label>
						{!! Form::text('name_th',old('name_th'),['class'=> 'form-control' ]) !!}
						{!! customValidator('name_th',$errors) !!}
					</div>
					<div class="form-group">
						<label for="">ชื่อ (EN)</label>
						{!! Form::text('name_eng',old('name_eng'),['class'=> 'form-control' ]) !!}
						{!! customValidator('name_eng',$errors) !!}
					</div>
					<div class="form-group">
						<label for="">อีเมล</label>
						{!! Form::text('email',old('email'),['class'=> 'form-control ' ]) !!}
						{!! customValidator('email',$errors) !!}
					</div>
        {!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="reset" form="form_edit_email" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_edit_email" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>
	
	<script >
		//  set data
		var contact = JSON.parse('<?php print_r(json_encode($contact)); ?>');
		now_manu = contact.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('.table').DataTable({
			'columnDefs': [ {
				'targets'  : 'no-sort',
				'orderable': false,
			}],
			'bLengthChange' : false,
			'bInfo': false,
			'bPaginate': false,
			'bFilter': false,
		});

		$('#modal_edit_email').on('show.bs.modal', function (event) {
			
			let button = $(event.relatedTarget);
			let name_th = button.data('name_th');
			let name_eng = button.data('name_eng');
			let email = button.data('email');
			let content_id = button.data('content_id');
			let modal = $(this);
			modal.find('[name="name_th"]').val(name_th);
			modal.find('[name="name_eng"]').val(name_eng);
			modal.find('[name="email"]').val(email);
			modal.find('[name="content_id"]').val(content_id);
			console.log('sss',button.data('content_id'));
		});
	});

    @if( session('editEmailContent') != null )
		// var email_id = "{{session('editEmailContent')}}" ;
		// console.log(email_id);
		// $("[data-content_id='"+email_id+"']").click();
    @endif
</script>
@endsection

