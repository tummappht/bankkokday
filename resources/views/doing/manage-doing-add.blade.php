@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.doing.add-and-edit-save','method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
			{!! Form::hidden('status','add') !!}
			<h4 class="header-text">เพิ่มมีดีมาโชว์</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label>หมวดหมู่</label>
								{!! Form::select('category_content_id', $category_contents, old('category_content_id'), ['class'=> 'form-control ']) !!}
								{!! customValidator('category_content_id',$errors) !!}
							</div>
						</div>
						<!-- <div class="w-100"></div> -->
						<!-- <div class="col-6">
							<div class="form-group">
								<label>งบประมาณ (TH)</label>
								{!! Form::text('budget_th',old('budget_th'),['class'=> 'form-control ', 'required' ]) !!}
								{!! customValidator('budget_th',$errors) !!}
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label>งบประมาณ (EN)</label>
								{!! Form::text('budget_eng',old('budget_eng'),['class'=> 'form-control ', 'required' ]) !!}
								{!! customValidator('budget_eng',$errors) !!}
							</div>
						</div> -->
						<div class="col-12">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>หัวข้อ (TH)</label>
										{!! Form::text('name_th',old('name_th'),['class'=> 'form-control', 'required' ]) !!}
										{!! customValidator('name_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>หัวข้อ (EN)</label>
										{!! Form::text('name_eng',old('name_eng'),['class'=> 'form-control', 'required' ]) !!}
										{!! customValidator('name_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>ที่ตั้ง / งบประมาณ (TH)</label>
										{!! Form::text('location_th',old('location_th'),['class'=> 'form-control', 'required' ]) !!}
										{!! customValidator('location_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>ที่ตั้ง / งบประมาณ (EN)</label>
										{!! Form::text('location_eng',old('location_eng'),['class'=> 'form-control ', 'required' ]) !!}
										{!! customValidator('location_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>ลักษณะงาน (TH)</label>
										{!! Form::text('work_detail_th',old('work_detail_th'),['class'=> 'form-control ', 'required' ]) !!}
										{!! customValidator('work_detail_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>ลักษณะงาน (EN)</label>
										{!! Form::text('work_detail_eng',old('work_detail_eng'),['class'=> 'form-control ', 'required' ]) !!}
										{!! customValidator('work_detail_eng',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียด (TH)</label>
										{!! Form::textarea('detail_th',old('detail_th'),['class'=> 'form-control summernote' ]) !!}
										{!! customValidator('detail_th',$errors) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>รายละเอียด (EN)</label>
										{!! Form::textarea('detail_eng',old('detail_eng'),['class'=> 'form-control summernote' ]) !!}
										{!! customValidator('detail_eng',$errors) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<div class="file-loading">
						<input name="files[]" type="file" class = "files" accept="image/*" multiple>
					</div>
					{!! customValidator('files',$errors) !!}
					{!! customValidator('files.*',$errors) !!}
					<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
				</div>
			</div>
		{!! Form::close() !!}
	</div>
	<script >
		//  set data
		var doing = JSON.parse('<?php print_r(json_encode($doing)); ?>');
		now_manu = doing.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>

	$(".files").fileinput({
		language: 'en',
		showCaption: true,
		showPreview: true,
		showRemove: true,
		showUpload: false, // <------ just set this from true to false
		showCancel: true,
		showUploadedThumbs: true,
		autoOrientImage: false,
    browseOnZoneClick: true,
		allowedFileExtensions: ["jpg", "png"]
	});
	
	$('.summernote').summernote({
		height: 200,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['fontsize', ['fontsize']],
			['height', ['height']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['table', ['table']],
			['insert', ['link', 'picture', 'video']],
			['view', ['codeview', 'help']],
		],
		fontNames: ['Sarabun',
			'SILPAKORN70yr',
			'2547_DAYIN01N',
			'2548_D6N',
			'can_Rukdeaw01',
			'kt_smarn seree',
			'PSL245pro',
			'PSL256pro',
			'Pspimpdeed',
			'SILPAKORN70NEW',
			'SILPAKORN70NEW_Sh',
			'SitkaBanner',
			'SitkaHeading',
			'SitkaSubheading',
			'SitkaText',
			'SitkaSmall'],
		fontNamesIgnoreCheck: ['Sarabun',
			'SILPAKORN70yr',
			'2547_DAYIN01N',
			'2548_D6N',
			'can_Rukdeaw01',
			'kt_smarn seree',
			'PSL245pro',
			'PSL256pro',
			'Pspimpdeed',
			'SILPAKORN70NEW',
			'SILPAKORN70NEW_Sh',
			'SitkaBanner',
			'SitkaHeading',
			'SitkaSubheading',
			'SitkaText',
			'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
	});

</script>
@endsection

