<nav id="sidebar">
	<div class="icon-row p-0 justify-content-end d-md-none">
		<a href="#" id="dismiss" class="d-flex icon">
			{!! file_get_contents('images/icons/icon-times.svg') !!}
		</a>
	</div>
	<a href="/">
  	<div class="sidebar-heading mb-3">
			{!! file_get_contents('images/icons/icon-logo.svg') !!}
		</div>
	</a>
	<ul id="dav_manu_option" class="list-unstyled"></ul>
</nav>