<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;
use App\Image ;
use App\Order;


class CommentController extends Controller
{
    public function index(){

        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        $customer_contents = Content::where('contents.manu_option_id', ManuOption::$comment_id )
        ->where('name_th','comment.customer')
        ->where('name_eng','comment.customer')
        ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
        ->where('orders.type','comment')

        // ->orderByRaw('ISNULL(orders.number), orders.number ASC')
        // ->orderBy('orders.number', 'asc')
        ->orderByRaw('LENGTH(orders.number)', 'ASC')
        ->orderBy('orders.number', 'ASC')
        ->select(
            'contents.id as id',
            'contents.name_th as name_th',
            'contents.name_eng as name_eng' ,
            // 'contents.font_id as font_id' ,
            'contents.category_content_id as category_content_id' ,
            'orders.type as type',
            'orders.number as number'
            )
        // ->inRandomOrder()
        // ->take(3)
        ->get();
        // dd($customer_contents);
        $customer_youtubes = Content::where('contents.manu_option_id', ManuOption::$comment_id )
        ->where('name_th','comment.youtube')
        ->where('name_eng','comment.youtube')
        ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
        ->where('orders.type','youtube_doing')

        // ->orderByRaw('ISNULL(orders.number), orders.number ASC')
        // ->orderBy('orders.number', 'asc')
        ->orderByRaw('LENGTH(orders.number)', 'ASC')
        ->orderBy('orders.number', 'ASC')
        ->select(
            'contents.id as id',
            'contents.name_th as name_th',
            'contents.name_eng as name_eng' ,
            // 'contents.font_id as font_id' ,
            'contents.category_content_id as category_content_id' ,
            'orders.type as type',
            // 'orders.number as number'
            )
        // ->inRandomOrder()
        // ->take(3)
        ->get();    
        // dd($customer_youtubes);  
        return view('comment.show-comment', compact('comment','customer_contents','customer_youtubes'));
    }

    public function manageComment()
    {
        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        // dd('manageIntroduction');
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        $customer_contents = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                    ->where('name_th','comment.customer')
                                    ->where('name_eng','comment.customer')
                                    ->get();
        $customer_youtubes = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                    ->where('name_th','comment.youtube')
                                    ->where('name_eng','comment.youtube')
                                    ->get();
        $fonts = Font::pluck('name', 'id');
        // dd($introduction->contents[0]->detail);
        // dd( Content::where('contents.manu_option_id', ManuOption::$introduction_id )->get()[0]->getDoingContentDetailByTypeDetail(null)  );
        
        $sort = self::getContentYoutubeCommentOrder();
        $sort_comment = self::getContentCommentOrder();
        // dd(self::getContentCommentOrder());
    
        return view('comment.manage-comment', compact('comment','fonts','customer_contents','customer_youtubes','sort','sort_comment'));
    }

    public function editComment(Request $request){
        // $validator = $this->validatorEditComment($request);
        // if ($validator->errors()->messages()) {
        //     return back()
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        // $comment->font_id = $request->font_id;
        $comment->save();

        return redirect('admin/testimonials'); 
    }

    // public function validatorEditComment($request){
    //     $validator = Validator::make(
    //         $request->all(), [
    //         'font_id' => 'required',
    //     ], [
    //         'font_id.required' => 'กรุณาเลือก font[TO]The font field is required.',
    //     ]);
    //     return $validator;
    // }

    public function addContentCustomer(){
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$comment_id )->get();
        return view('comment.manage-add-customer-content', compact('comment','contents'));
    }

    public function addAndEditSaveContentCustomer(Request $request){

        $validator = $this->validatorAddAndEditSaveCommentCustomer($request);
        // dd($validator->errors()->messages());
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $content_name_th = $request->name_th ;
        $content_name_eng = $request->name_eng ;
        $request->name_th = 'comment.customer';
        $request->name_eng = 'comment.customer';

        $content_id = (new HelperController())->contentSave($request , ManuOption::$comment_id );

        $request->name_th = $content_name_th ;
        $request->name_eng = $content_name_eng ;

        (new HelperController())->detailSave($request , $content_id , 'name' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'position' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'comment' , 'th-and-eng');

        if($request->status == 'add'){  //set ท้ายสุด           
            $order = new Order();
            $order->type = 'comment' ;
            $order->number = '9999999' ;
            $order->content_id = $content_id;
            $order->save(); 
        }

        // (new HelperController())->detailSave($request , $content_id , 'detail_youtube' , 'th-and-eng');
        
        // (new HelperController())->detailSave($request , $content_id , 'path_youtube' , 'no-th-and-eng');

        $request->name_input_files = 'files' ;
        $request->name_file = 'images/comment' ;
        $request->content_id = $content_id ;
        $request->is_main = 1;
        (new HelperController())->filesSave($request);
        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        }
        
        return redirect('admin/testimonials/'.$content_id.'/edit-customer-content'); 
    }

    public function editContentCustomer($content_id){
        // dd($content_id);
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        $content = Content::where('id',$content_id)->first();
        // dd( Detail::where('content_id',$content_id)->where('type_detail','detail_youtube')->first() );
        return view('comment.manage-edit-customer-content', compact('comment','content','content_id'));
    }

    public function validatorAddAndEditSaveCommentCustomer($request){
        if($request->status == 'add'){
            $validator = Validator::make(
                $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'position_th' => 'required',
                'position_eng' => 'required',
                'comment_th' => 'required',
                'comment_eng' => 'required',
                // 'detail_youtube_th' => 'required',
                // 'detail_youtube_eng' => 'required',
                // 'path_youtube' => 'required',
                'files' => 'required',
        
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'position_th.required' => 'กรุณากรอกตำแหน่งภาษาไทย[TO]The font field is required.',
                'position_eng.required' => 'กรุณากรอกตำแหน่งภาษาอังกฤษ[TO]Sub title ENG field is required.',

                'comment_th.required' => 'กรุณากรอกความคิดเห็นภาษาไทย[TO]Sub title TH field is required.',
                'comment_eng.required' => 'กรุณากรอกความคิดเห็นภาษาอังกฤษ[TO]Content field is required.',
                // 'detail_youtube_th.required' => 'กรุณากรอกคำอธิบายคลิปภาษาไทย[TO]Content field is required.',
                // 'detail_youtube_eng.required' => 'กรุณากรอกคำอธิบายคลิปภาษาอังกฤษ[TO]Content field is required.',
                // 'path_youtube.required' => 'กรุณากรอกลิงก์คลิป[TO]Content field is required.',
                'files.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
            ]);
        }else{
            $validator = Validator::make(
                $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'position_th' => 'required',
                'position_eng' => 'required',
                'comment_th' => 'required',
                'comment_eng' => 'required',
                // 'detail_youtube_th' => 'required',
                // 'detail_youtube_eng' => 'required',
                // 'path_youtube' => 'required',
        
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'position_th.required' => 'กรุณากรอกตำแหน่งภาษาไทย[TO]The font field is required.',
                'position_eng.required' => 'กรุณากรอกตำแหน่งภาษาอังกฤษ[TO]Sub title ENG field is required.',

                'comment_th.required' => 'กรุณากรอกความคิดเห็นภาษาไทย[TO]Sub title TH field is required.',
                'comment_eng.required' => 'กรุณากรอกความคิดเห็นภาษาอังกฤษ[TO]Content field is required.',
                // 'detail_youtube_th.required' => 'กรุณากรอกคำอธิบายคลิปภาษาไทย[TO]Content field is required.',
                // 'detail_youtube_eng.required' => 'กรุณากรอกคำอธิบายคลิปภาษาอังกฤษ[TO]Content field is required.',
                // 'path_youtube.required' => 'กรุณากรอกลิงก์คลิป[TO]Content field is required.',
            ]);
        }

        return $validator ;
    }

    public function addContentYoutube(){
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        return view('comment.manage-add-youtube-content', compact('comment'));
    }

    public function editContentYoutube($content_id){
        $comment = ManuOption::where('manu_options.id' , ManuOption::$comment_id )->first();
        $content = Content::where('id',$content_id)->first();
        return view('comment.manage-edit-youtube-content', compact('comment','content','content_id'));
    }

    public function addAndEditSaveContentYoutube(Request $request){
        $validator = $this->validatorAddAndEditSaveCommentYoutube($request);
        // dd($validator->errors()->messages());
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $request->name_th = 'comment.youtube';
        $request->name_eng = 'comment.youtube';

        $content_id = (new HelperController())->contentSave($request , ManuOption::$comment_id );

        (new HelperController())->detailSave($request , $content_id , 'detail_youtube' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'path_youtube' , 'no-th-and-eng');

        if($request->status == 'add'){  //set ท้ายสุด           
            $order = new Order();
            $order->type = 'youtube_doing' ;
            $order->number = '9999999' ;
            $order->content_id = $content_id ;
            $order->save(); 
        }

        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        }
        return redirect('admin/testimonials/'.$content_id.'/edit-youtube-content'); 
        // return redirect('admin/comment'); 
    }

    public function validatorAddAndEditSaveCommentYoutube($request){

        $validator = Validator::make(
            $request->all(), [
            'detail_youtube_th' => 'required',
            'detail_youtube_eng' => 'required',
            'path_youtube' => 'required',
    
        ], [
            'detail_youtube_th.required' => 'กรุณากรอกคำอธิบายคลิปภาษาไทย[TO]Content field is required.',
            'detail_youtube_eng.required' => 'กรุณากรอกคำอธิบายคลิปภาษาอังกฤษ[TO]Content field is required.',
            'path_youtube.required' => 'กรุณากรอกลิงก์คลิป[TO]Content field is required.',
        ]);

        return $validator ;
    }

    public function deleteContent($content_id){
        Detail::where('content_id',$content_id)->delete();
        Order::where('content_id',$content_id)->delete();
        Image::where('content_id',$content_id)->delete();
        Content::where('id',$content_id)->delete();
        session(['status_manage' => 'ลบข้อมูลสำเร็จ']);
        return redirect('admin/testimonials');  
    }

    public function getContentYoutubeCommentOrder(){
        $contents_youtube_number_null = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                        ->where('name_th','comment.youtube')
                                        ->where('name_eng','comment.youtube')
                                        ->leftJoin('details', 'details.content_id', '=', 'contents.id')
                                        ->where('details.type_detail','path_youtube')
                                        ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                                        ->where('orders.number',null)
                                        ->select(
                                            'contents.id as id',
                                            'details.detail_all as name_th',
                                            'orders.type as type' ,
                                            'orders.number as number' ,
                                            )
                                        ->get()
                                        ->toarray();
        $contents_youtube_number_not_null = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                        ->where('name_th','comment.youtube')
                                        ->where('name_eng','comment.youtube')
                                        ->leftJoin('details', 'details.content_id', '=', 'contents.id')
                                        ->where('details.type_detail','path_youtube')
                                        ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                                        ->where('orders.type','youtube_doing')
                                        ->whereNotNull('orders.number')

                                        ->orderByRaw('LENGTH(orders.number)', 'ASC')
                                        ->orderBy('orders.number', 'asc')
                                        ->select(
                                            'contents.id as id',
                                            'details.detail_all as name_th',
                                            'orders.type as type' ,
                                            'orders.number as number' ,
                                            )
                                        ->get()
                                        ->toarray();
        return [$contents_youtube_number_null,$contents_youtube_number_not_null] ;
    }

    public function getContentCommentOrder(){


        $contents_comment_number_not_null = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                            ->where('name_th','comment.customer')
                                            ->where('name_eng','comment.customer')
                                            ->leftJoin('details', 'details.content_id', '=', 'contents.id')
                                            ->where('details.type_detail','name')
                                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                                            ->where('orders.type','comment')
                                            ->whereNotNull('orders.number')

                                            ->orderByRaw('LENGTH(orders.number)', 'ASC')
                                            ->orderBy('orders.number', 'asc')
                                            ->select(
                                                'contents.id as id',
                                                'details.detail_th as name_th',
                                                'details.detail_th as name_th',
                                                'orders.type as type' ,
                                                'orders.number as number' ,
                                                )
                                            ->get()
                                            ->toarray();
        $contents_comment_number_not_null_id = [] ;
        foreach ($contents_comment_number_not_null as $key => $value) {
            $contents_comment_number_not_null[$key]['comment'] = (Content::where('id',$value['id'])->first())->getDetailByTypeDetail('comment')['detail_th'];
            $contents_comment_number_not_null_id[$key] = $value['id'] ;
        }

        $contents_comment_number_null = Content::where('contents.manu_option_id', ManuOption::$comment_id )
                                        ->where('name_th','comment.customer')
                                        ->where('name_eng','comment.customer')
                                        ->whereNotIn('contents.id', $contents_comment_number_not_null_id)
                                        ->leftJoin('details', 'details.content_id', '=', 'contents.id')
                                        ->where('details.type_detail','name')
                                        // ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                                        // ->where('orders.number',null)
                                        ->select(
                                            'contents.id as id',
                                            'details.detail_th as name_th',
                                            // 'orders.type as type' ,
                                            // 'orders.number as number' ,
                                            )
                                        ->get()
                                        ->toarray();

        foreach ($contents_comment_number_null as $key => $value) {
            $contents_comment_number_null[$key]['comment'] = (Content::where('id',$value['id'])->first())->getDetailByTypeDetail('comment')['detail_th'];
        }
                                        
        return [$contents_comment_number_null,$contents_comment_number_not_null] ;
    }

    public function setYoutubeOrder(Request $request){
        // dd($request);
        Order::where('type', 'youtube_doing')->delete();
        if( $request->youtube_contents != null){
            foreach ($request->youtube_contents as $key => $content) {
                $order = new Order();
                $order->type = 'youtube_doing' ;
                $order->number = $key + 1 ;
                $order->content_id = $content ;
                $order->save();
            }
        }
        session(['status_manage' => 'แก้ไขลำดับข้อมูลสำเร็จ']);
        return redirect('admin/testimonials');  
    }

    public function setCommentOrder(Request $request){
        // dd($request);
        Order::where('type', 'comment')->delete();
        if( $request->comment_contents != null){
            foreach ($request->comment_contents as $key => $content) {
                $order = new Order();
                $order->type = 'comment' ;
                $order->number = $key + 1 ;
                $order->content_id = $content ;
                $order->save();
            }
        }
        session(['status_manage' => 'แก้ไขลำดับข้อมูลสำเร็จ']);
        return redirect('admin/testimonials');  
    }
}
