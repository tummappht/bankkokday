@extends('layout.main_admin')
@section('style')  

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">จัดการความเห็นจากลูกค้า</h4>
		<div class="card">
			<ul class="nav nav-tabs sub-tabs">
				<li class="nav-item">
					<button class="nav-link active" data-card="comment">ความเห็นจากลูกค้า</button>
				</li>
				<li class="nav-item">
					<button class="nav-link" data-card="youtube">Youtube</button>
				</li>
			</ul>
			<div id="comment-card" class="card-body sub-card table-responsive">
				<div class="d-flex justify-content-end">
					<a href="{{url('/admin/testimonials/add-customer-content')}}" class="btn btn-secondary d-flex mb-3 mr-2">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่มความเห็นจากลูกค้า
					</a>
					<button type="button" class="btn btn-secondary d-flex mb-3 align-items-center" data-toggle="modal" data-target="#modal_sort">
						<img src="{{asset('/images/icons/icon-sort.svg')}}" class="mr-2" style="width: 14px;">
						Sort
					</button>
				</div>
				<table class="table table-striped table-with-image" id="table-comment">
					<thead>
						<th class="thin-cell">วันที่</th>
						<th>ชื่อ</th>
						<th>ตำแหน่ง</th>
						<th>ความคิดเห็นจากลูกค้า</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($customer_contents as $customer_content)
						<tr>
							<td class="text-nowrap">
								<span class="d-none">{{strtotime($customer_content->created_at)}}</span>
								{{date_format($customer_content->created_at,"d/m/Y")}}
							</td>
							<td class="text-nowrap">
								{{$customer_content->getDetailByTypeDetail('name')['detail_th']}}
								<br>
								{{$customer_content->getDetailByTypeDetail('name')['detail_eng']}}
							</td>
							<td class="text-nowrap">
								{{$customer_content->getDetailByTypeDetail('position')['detail_th']}}
								<br>
								{{$customer_content->getDetailByTypeDetail('position')['detail_eng']}}
							</td>
							<td>
								{{strip_tags($customer_content->getDetailByTypeDetail('comment')['detail_th'])}}
								<br>
								{{strip_tags($customer_content->getDetailByTypeDetail('comment')['detail_eng'])}}
							</td>
							<td class="d-flex">
								<a href="{{url('admin/testimonials/'.$customer_content->id.'/edit-customer-content')}}" class="btn btn-light border border-dark mr-2">
									<i class="far fa-edit"></i>
								</a>
								<a  href = "#" onclick="deleteByURL('{{url('admin/testimonials/')}}' , '{{$customer_content->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="youtube-card" class="card-body sub-card d-none table-responsive">
				<div class="d-flex justify-content-end">
					<a href="{{url('/admin/testimonials/add-youtube-content')}}" class="btn btn-secondary d-flex mb-3 mr-2">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่ม Youtube
					</a>
					<button type="button" class="btn btn-secondary d-flex mb-3 align-items-center" data-toggle="modal" data-target="#ymodal_sort">
						<img src="{{asset('/images/icons/icon-sort.svg')}}" class="mr-2" style="width: 14px;">
						Sort
					</button>
				</div>
				<table class="table table-striped table-with-image" id="table-dashboard">
					<thead>
						<th>ลิงก์</th>
						<th>คำอธิบาย</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($customer_youtubes as $customer_youtube)
						@php
							$yt_link = $customer_youtube->getDetailByTypeDetail('path_youtube')['detail_all'];
							$yt_id = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", '$1', $yt_link);
							$ytimg = 'https://img.youtube.com/vi/'.$yt_id.'/sddefault.jpg';
						@endphp
						<tr>
							<td>
								<img src="{{$ytimg}}" alt="{{$yt_link}}" style="width: 60px; margin-right: 16px; ">
								{{$yt_link}}
							</td>
							<td>{!! substr(strip_tags($customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_th']),0,50) !!}</td>
							<td class="d-flex">
								<a href="{{url('admin/testimonials/'.$customer_youtube->id.'/edit-youtube-content')}}" class="btn btn-light border border-dark mr-2">
									<i class="far fa-edit"></i>
								</a>
								<a href = "#" onclick="deleteByURL('{{url('admin/testimonials/')}}' , '{{$customer_youtube->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{-- <div class="card">
					<div class="card-body">
						{!! Form::open(['route'=>'admin.comment.set-youtube-order', 'method', 'POST', 'class' => 'form']) !!}
						<div class="row">
							<div class="col-12">
								<h5>จัดการลำดับ Youtube</h5>
								<div class="form-group">
									<select class="form-control select2" multiple="multiple" id = 'youtube-content' name = 'youtube_contents[]' style="width: 100%" ></select>
								</div>
								<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div> --}}
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal_sort" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">จัดการลำดับ ความเห็นจากลูกค้า</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_sort', 'route'=>'admin.comment.set-comment-order', 'method', 'POST', 'class' => 'form']) !!} 
					<div class="list-group col sortlist">
						@php
							$index_no = [1, 0];
							$count = count($sort_comment[0]) + count($sort_comment[1]);
						@endphp
						@foreach ($index_no as $i)
							@foreach ($sort_comment[$i] as $key => $list)
								@php
									if($key == 0) {
										$sort_class = 'sort-down';
									} else if($key == ($count - 1)) {
										$sort_class = 'sort-up';
									} else {
										$sort_class = 'sort';
									}
									$comment_name_th = $list['name_th'];
									$comment_comment = $list['comment'];
								@endphp
								<div class="list-group-item d-flex align-items-center justify-content-between">
									<div class="form-row w-100">
										<div class="col-auto">
											<p class="m-0">{{$comment_name_th}}</p>
										</div>
										<div class="col mr-3">
											<p class="m-0 text-truncate">{!! $comment_comment !!}</p>
										</div>
									</div>
									<i class="fas fa-{{$sort_class}}"></i>
									<input type="hidden" name="comment_contents[]" value="{{$list['id']}}">
								</div>
							@endforeach
						@endforeach
					</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_sort" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="ymodal_sort" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">จัดการลำดับ Youtube</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'yform_sort', 'route'=>'admin.comment.set-youtube-order', 'method', 'POST', 'class' => 'form']) !!}
					<div class="list-group col sortlist">
						@php
							$index_no = [1, 0];
							$count = count($sort[0]) + count($sort[1]);
						@endphp
						@foreach ($index_no as $i)
							@foreach ($sort[$i] as $key => $list)
								@php
									if($key == 0) {
										$sort_class = 'sort-down';
									} else if($key == ($count - 1)) {
										$sort_class = 'sort-up';
									} else {
										$sort_class = 'sort';
									}
									$yt_link = $list['name_th'];
									$yt_id = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", '$1', $yt_link);
									$ytimg = 'https://img.youtube.com/vi/'.$yt_id.'/sddefault.jpg';
								@endphp
								<div class="list-group-item d-flex align-items-center justify-content-between">
									<div class="d-flex">
										<img src="{{$ytimg}}" alt="{{$yt_link}}" style="width: 60px; margin-right: 16px; ">
										<p class="m-0">{{$yt_link}}</p>
									</div>
									<i class="fas fa-{{$sort_class}}"></i>
									<input type="hidden" name="youtube_contents[]" value="{{$list['id']}}">
								</div>
							@endforeach
						@endforeach
					</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="yform_sort" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>

	<script >
		//  set data
		var comment = JSON.parse('<?php print_r(json_encode($comment)); ?>');
		var customer_contents = JSON.parse('<?php print_r(json_encode($customer_contents)); ?>');
		now_manu = comment.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
<script>
	$(document).ready(function() {
		// $('#sortlist').sortable({
    // 	animation: 150,
		// 	onChange: e => {
		// 		updateSort($('#sortlist'));
		// 	}
		// });

		$('.sortlist').sortable({
    	animation: 150,
			onChange: e => {
				updateSort($(e.srcElement));
			}
		});

		$('.table').DataTable({
			'order': [0, 'ASC'],
			'columnDefs': [ {
				'targets'  : 'no-sort',
				'orderable': false,
			}],
			'language': {
				'paginate': {
					'previous': '<',
					'next': '>'
				}
			}
		});

		$(document).on('click', '.sub-tabs .nav-link', function () {
			let card = $(this).data('card');
			$('.sub-tabs .nav-link').removeClass('active');
			$(this).addClass('active');
			$('.sub-card').addClass('d-none');
			$(`#${card}-card`).removeClass('d-none');
		});
	});

	function updateSort($this) {
		let i = 0;
		let fas = $this.find('.fas');
		let count = fas.length;
		fas.each(function() {
			let parent = $(this).parents('.list-group-item');
			let id = parent.attr('data-sort');
			$(this).removeClass('fa-sort-down');
			$(this).removeClass('fa-sort-up');
			$(this).removeClass('fa-sort');
			if(i == 0) {
				$(this).addClass('fa-sort-down');
			} else if (i == (count - 1)) {
				$(this).addClass('fa-sort-up');
			}
			$(this).addClass('fa-sort');
			i++;
		});
	}
</script>
@endsection

