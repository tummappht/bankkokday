<?php

use Illuminate\Database\Seeder;
use App\User ;

class AddFixData_UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->id = 1 ;
        $user->name = 'Bangkok Day Group' ;
        $user->email = 'bdg@email.com' ;
        $user->password = Hash::make('bdg123456') ;
        $user->status_active = '1' ;
        $user->save();
    }
}
