@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
@endsection
@section('content')

    <div class="container">
        {!! Form::open(['route'=>'admin.manage-admin.add-and-edit-admin-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
            {!! Form::hidden('status','add') !!}
            <div class="row">
                <div class="col-lg-6">
                    Name {!! Form::text('name',old('name'),['class'=> 'form-control' ]) !!}
                    {!! customValidator('name',$errors) !!}
                </div>
                <div class="col-lg-6">
                    Email {!! Form::text('email',old('email'),['class'=> 'form-control' , 'autocomplete'=>"off" ]) !!}
                    {!! customValidator('email',$errors) !!}
                </div>
                <div class="col-lg-8">
                    Password {!! Form::password('password',['class'=> 'form-control' ,'autocomplete'=>"off" ]) !!}
                    {!! customValidator('password',$errors) !!}
                </div>
                <div class="col-lg-4">
                    สถานะการเปิดใช้งาน 
                    <?php $status_active_select = [ null => 'กรุณาเลือกประเภท' ,'0' => 'ปิดใช้งาน ' , '1' => 'เปิดใช้งาน' ] ; ?>
                    {!! Form::select('status_active',$status_active_select,old('status_active'),['class'=> 'form-control ' ]) !!}
                    {!! customValidator('status_active',$errors) !!}
                </div>

                </br></br> </br></br>
                <div class="col-lg-12">
                    <input type="submit" class="btn btn-primary btn-primary-blue" value="เพิ่ม">
                </div>
            <div>

            
        {!! Form::close() !!}

    </div>


    <script >
        //  set data
        now_manu = 'manage-admin' ;
    </script>
@endsection
@section('script')

<script>
    $('option[value=""]').attr("disabled", true) 

</script>
@endsection
