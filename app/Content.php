<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image ;
use App\Font ;
use App\CategoryContent ;
use App\Detail ;

class Content extends Model
{

    static $introduction_contents_id_1 = '1';
    static $introduction_contents_id_2 = '2';
    public function getImagesAttribute()
    {
        $images = Image::where('content_id' , $this->id)->get();

        foreach ($images as $key => $image) {
            $image = defaultImage($image);
        }

        return $images ;
    }

    public function getMainImageAttribute()
    {
        $image = Image::where('content_id' , $this->id)->where('is_main','1')->first();
        $image = defaultImage($image);

        return $image ;
    }

    public function getNoMainImagesAttribute()
    {
        $image = Image::where('content_id' , $this->id)->where('is_main','!=','1')->get();

        $image = defaultImage($image);

        return $image ;
    }

    public function getHeroImageAttribute()
    {
        $image = Image::where('content_id' , $this->id)->where('detail','hero_image')->first();
        $image = defaultImage($image);
        return $image ;
    }

    public function getDetailIsMainImageAttribute()
    {
        $image = Image::where('content_id' , $this->id)->where('detail','main_image')->first();
        $image = defaultImage($image);
        return $image ;
    }

    public function getFontAttribute()
    {
        return Font::where('id' , $this->font_id)->first();
    }

    public function getCategoryContentAttribute()
    {
        return CategoryContent::where('id' , $this->category_content_id)->first();
    }

    public function getDetailAttribute()
    {
        return Detail::where('content_id' , $this->id)->first();
    }
    public function getDetailsAttribute()
    {
        return Detail::where('content_id' , $this->id)->get();
    }
    public function getDetailByTypeDetail($type_detail)
    {
        $value= (Detail::where('content_id' , $this->id)->where('type_detail' , $type_detail)->first()) ;
        if($value == null){
            $value['detail_all'] = null ;
            $value['detail_eng'] = null ;
            $value['detail_th'] = null ;
        }
        return $value;

    }

}
