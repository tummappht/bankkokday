<?php

use Illuminate\Database\Seeder;
use App\ManuOption ;
use Illuminate\Support\Facades\Storage;

class AddFixData_ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [ 'id'=>6, 'name_th'=>'contact.main' , 'name_eng'=>'contact.main' , 'manu_option_id' => ManuOption::$contact_id ],
        ]);

        DB::table('details')->insert([
            [ 'id'=>6, 'detail_all'=> 'www.google_map.com', 'content_id' => '6' , 'type_detail' => 'link_map'],
        ]);

        DB::table('details')->insert([
            [ 'id'=>7, 'detail_th'=> '166 ม.3 เชียงใหม่' , 'detail_eng'=>'166 m.3 cnx' , 'content_id' => '6' , 'type_detail' => 'address'],
        ]);

    }
}
