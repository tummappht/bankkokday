<nav id="sidebar">
	<div class="icon-row">
		<a id="dismiss" class="d-flex icon">
			{!! file_get_contents('images/icons/icon-times.svg') !!}
		</a>
		<a href="/" class="d-flex icon">
			{!! file_get_contents('images/icons/icon-home.svg') !!}
		</a>
	</div>
	<ul id="dav_manu_option" class="list-unstyled"></ul>
	<div class="form-row d-md-none">
		<div class="nav-item col-auto">
			<a class="nav-link bt_language_eng" href="#"></a>
		</div>
		<div class="col-auto">
			<span>|</span>
		</div>
		<div class="nav-item col-auto">
			<a class="nav-link bt_language_th" href="#"></a>
		</div>
	</div>
</nav>
