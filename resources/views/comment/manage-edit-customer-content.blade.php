@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.comment.add-and-edit-save-customer-content', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
			{!! Form::hidden('status','edit') !!}
			{!! Form::hidden('content_id',$content_id) !!}
			<h4 class="header-text">แก้ไขความเห็นจากลูกค้า</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>ชื่อ (TH)</label>
								{!! Form::text('name_th',old('name_th',$content->getDetailByTypeDetail('name')['detail_th']),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('name_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ชื่อ (EN)</label>
								{!! Form::text('name_eng',old('name_eng',$content->getDetailByTypeDetail('name')['detail_eng']),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('name_eng',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ตำแหน่ง (TH)</label>
								{!! Form::text('position_th',old('position_th', $content->getDetailByTypeDetail('position')['detail_th'] ),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('position_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ตำแหน่ง (EN)</label>
								{!! Form::text('position_eng',old('position_eng' , $content->getDetailByTypeDetail('position')['detail_eng'] ),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('position_eng',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ความคิดเห็น (TH)</label>
								{!! Form::textarea('comment_th',old('comment_th' , $content->getDetailByTypeDetail('comment')['detail_th'] ),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('comment_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ความคิดเห็น (EN)</label>
								{!! Form::textarea('comment_eng',old('comment_eng' ,$content->getDetailByTypeDetail('comment')['detail_eng'] ),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('comment_eng',$errors) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>รูปภาพ</label>
								<div class="file-loading">
									<input name="files[]" type="file" class="files" multiple>
								</div>
								{!! customValidator('files',$errors) !!}
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
				</div>
			</div>
		{!! Form::close() !!}
	</div>


	<script >
		//  set data
		var comment = JSON.parse('<?php print_r(json_encode($comment)); ?>');
		now_manu = comment.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>


    $('.summernote').summernote({
			height: 200,
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['codeview', 'help']],
			],
			fontNames: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
			fontNamesIgnoreCheck: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
    });

    var images =  JSON.parse('<?php print_r(json_encode($content->images)); ?>');
    var urls = [] , preview_configs = [];
    for (i = 0; i < images.length; i++) {
        urls[i] = '{{url("/")}}'+'/'+images[i]['path'] ;
        preview_configs[i] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls[i] , key: images[i]['id'] } ;
    }

    (urls , preview_configs,$('[name="_token"]').val());

    $(".files").fileinput({
			maxFileCount: 1,
			validateInitialCount: true,
			language: 'en',
			initialPreview: urls,
			initialPreviewAsData: true,
			initialPreviewConfig: preview_configs,
			deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
			deleteExtraData: {
					'_token': $('[name="_token"]').val(), // _token
			},
			fileActionSettings: {
					showDrag: false,
    	},
			overwriteInitial: false,
			browseOnZoneClick: true,
     });

</script>
@endsection

