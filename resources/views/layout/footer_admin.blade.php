<div class="sidebar-overlay"></div>
<script>
  var icon_dash = <?php print_r(json_encode(file_get_contents('images/icons/icon-dash.svg'))); ?> ;
  $(document).ready(function () {
    $('#dismiss, .sidebar-overlay').on('click', function () {
      $('#sidebar').removeClass('active');
      $('.sidebar-overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
      $('#sidebar').addClass('active');
      $('.sidebar-overlay').addClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  });
</script>