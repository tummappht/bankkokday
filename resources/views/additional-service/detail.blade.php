@extends('layout.main_user')

@section('content')

	<div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">
		<div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{url($content->hero_image->path)}}" alt="Hero Image">
      </div>
		</div>
	</div>
	<div class="container content pt-4">
    <div class="service-row">
      <h1 class="language-th header-text mb-3">{{$content->name_th}}</h1>
      <h1 class="language-eng header-text mb-3">{{$content->name_eng}}</h1>
      <div class="language-th">
        {!! $content->getDetailByTypeDetail('detail')['detail_th'] !!}
      </div>
      <div class="language-eng">
        {!! $content->getDetailByTypeDetail('detail')['detail_eng'] !!}
      </div>
      <div class="row-divider mx-auto"></div>
      <div class="language-th">
        {!! $content->getDetailByTypeDetail('detail_wysiwyg')['detail_th'] !!}
      </div>
      <div class="language-eng">
        {!! $content->getDetailByTypeDetail('detail_wysiwyg')['detail_eng'] !!}
      </div>
    </div>
  </div>
  <div class="content content-container">
    @if ($content->getDetailByTypeDetail('slide')['detail_all'] == 'no slide')
      <div class="form-row">
        @php
          $count = 0;
        @endphp
        @foreach( $content->images as $key => $image )
          @if ($image->detail == 'gallery_image')
          @php
            $class = $count == 0 ? 'col col-sm-6 col-lg-7' : 'col';
            $count++;
          @endphp
          <div class="{{$class}}">
            <div class="noslide-image">
              <img src="{{url($image->path)}}">
            </div>
          </div>
          @endif
        @endforeach
      </div>
    @else
      <div class="slick-carousel service-slick">
        @foreach( $content->images as $image )
          @if ($image->detail == 'gallery_image')
          <div class="image">
            <img src="{{url($image->path)}}">
          </div>
          @endif
        @endforeach
      </div>
    @endif
  </div>
  <div class="container">
    <p class="font-header text-center m-0 language-th">ติดตามเรานะ</p>
    <p class="font-header text-center m-0 language-eng">Follow us</p>
    <p class=" text-center text-20px">
      <a target="_blank" href="{{$content->getDetailByTypeDetail('facebook')['detail_all']}}">
        <i class="fab fa-facebook-square"></i>
      </a>
    </p>
  </div>
@endsection
@section('script')
  <script>
    now_manu = 'additional-service-detail';
    let slide = JSON.parse('{!! json_encode($content->images); !!}');
    let bg_color = '{{$content->getDetailByTypeDetail('color')['detail_all']}}';

		$(document).ready(function () {
      $('#page-content-wrapper').css('background', bg_color);
      $('.page-footer').css('background', bg_color);
			let header = $('.header');
			header.addClass('header-darken position-fixed');
			let carousel = $('#header-carousel');

			$(window).scroll(function() {
				let carousel_height = carousel[0].getBoundingClientRect().height;
				let page_top = $(window).scrollTop();
				if(page_top >= carousel_height) {
					header.removeClass('header-darken');
				} else {
					header.addClass('header-darken');
				}
      });
      
			$('.slick-carousel').slick({
				infinite: true,
				slidesToShow: 3,
				swipeToSlide: true,
        arrows: false,
				touchThreshold: 100,
        autoplay: true,
        autoplaySpeed: 2000,
      });

      // $('.slick-carousel').on('wheel', (function(e) {
      //   e.preventDefault();

      //   if (e.originalEvent.deltaY < 0) {
      //     $(this).slick('slickNext');
      //   } else {
      //     $(this).slick('slickPrev');
      //   }
      // }));
    });
  </script>
@endsection