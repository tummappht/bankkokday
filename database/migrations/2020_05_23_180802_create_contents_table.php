<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->string('name_th')->nullable();
            $table->string('name_eng')->nullable();
            
            $table->unsignedBigInteger('manu_option_id')->nullable()->comment('content ที่อยู่ใน manu_option');
            $table->foreign('manu_option_id')->references('id')->on('manu_options')->onDelete('cascade');

            $table->unsignedBigInteger('font_id')->nullable()->comment('font ที่ใช้');
            $table->foreign('font_id')->references('id')->on('fonts');
            
            $table->unsignedBigInteger('category_content_id')->nullable()->comment('ประเภท content');
            $table->foreign('category_content_id')->references('id')->on('category_contents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
