@extends('layout.main_user')

@section('content')

	@php
		$header = App\ManuOption::where('manu_options.id' , App\ManuOption::$header_id )->first();
	@endphp
	<div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
		<div class="carousel-inner">
			@foreach ($header->images as $idx => $header_image)
				@php
						$active = $idx == 0 ? 'active' : '';
						$src = $idx == 0 ? 'src="'.url($header_image->path).'"' : 'data-src="'.url($header_image->path).'"';
				@endphp
				<div class="carousel-item {{$active}}">
					<img {!! $src !!} alt="Hero-image{{$idx}}" class="lazyload">
				</div>
			@endforeach
		</div>
		<div class="hero-name-text">
			<h1 class="font-header" id ="header_detail"></h1>
		</div>
	</div>
	<div class="container header-text mb-5" data-aos="fade-up" data-aos-duration="500">
		<div class="row">
			@foreach ($header->contents as $key => $content)
				<div class="col-md-4">
					<div class="form-row">
						<div class="col-auto">
							{!! file_get_contents('images/icons/icon-horizon-line.svg') !!}
						</div>
						<div class="col">
							<!-- <p class="m-0 language-th">{!! $content->name_th !!}</p>
							<p class="m-0 language-eng">{!! $content->name_eng !!}</p> -->
							<div class="m-0 language-th font-Medium text-12px">
								@php
									echo $content->detail->detail_th;
								@endphp
							</div>
							<div class="m-0 language-eng font-Medium text-12px">
								@php
									echo $content->detail->detail_eng;
								@endphp
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>

	{{-- Background --}}
	@php
		$introduction = App\ManuOption::where('manu_options.id' , App\ManuOption::$introduction_id )->first();
        $content1 = App\Content::where('contents.id' , App\Content::$introduction_contents_id_1 )->first();
        $content2 = App\Content::where('contents.id' , App\Content::$introduction_contents_id_2 )->first();
        $detail1 = App\Detail::where('details.content_id' , App\Content::$introduction_contents_id_1)->first();
        $detail2 = App\Detail::where('details.content_id' , App\Content::$introduction_contents_id_2)->first();
	@endphp
	<div class="container content about-us pt-4 mb-5" data-aos="fade-up" data-aos-duration="500">
		<h1 class="header-text language-th">{{$introduction->name_th}}</h1>
		<h1 class="header-text language-eng">{{$introduction->name_eng}}</h1>
		<div class="row">
			<div class="col-lg">
				<div class="form-row">
					<div class="col-lg pr-lg-0 mb-3 mb-sm-0" data-aos="fade-right" data-aos-duration="500" data-aos-delay="100">
						<img data-src="{{$introduction->main_image->path}}" class="img-lg lazyload" alt="main_image">
					</div>
					<div class="col-sm-auto d-sm-none d-lg-block" data-aos="fade-right" data-aos-duration="500">
						<img data-src="{{$introduction->no_main_image->path}}" class="img-sm lazyload" alt="no_main_images">
					</div>
				</div>
			</div>
			<div class="col-md col-lg-4 about-us-text d-flex align-items-center pt-3 py-lg-0">
				<div class="w-100">
					<img data-src="{{$introduction->no_main_image->path}}" class="img-sm d-none d-sm-block d-lg-none lazyload" alt="no_main_images2">
					<h6 class="mb-4 language-th">{{$introduction->detail_th}}</h6>
					<h6 class="mt-3 mb-4 language-eng">{{$introduction->detail_eng}}</h6>
					<h6 class="language-th">{{$content1->name_th}}</h6>
					<h6 class="language-eng">{{$content1->name_eng}}</h6>
					<div class="row-divider"></div>
					<div class="mb-3 language-th">{!! $detail1->detail_th !!}</div>
					<div class="mb-3 language-eng">{!! $detail1->detail_eng !!}</div>
					<h6 id="content_2"></h6>
					<h6 class="language-th">{!! $content2->name_th !!}</h6>
					<h6 class="language-eng">{!! $content2->name_eng !!}</h6>
					<div class="row-divider"></div>
					<div class="language-th">{!! $detail2->detail_th !!}</div>
					<div class="language-eng">{!! $detail2->detail_eng !!}</div>
				</div>
			</div>
		</div>
	</div>

	{{-- Our Work --}}
	@php
		$doing = App\ManuOption::where('manu_options.id' , App\ManuOption::$doing_id )->first();
		// $contents = App\Content::where('contents.manu_option_id', App\ManuOption::$doing_id )->limit(8)->get();
		$contents = App\Content::where('contents.manu_option_id', App\ManuOption::$doing_id )
												->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
												->where('orders.type','highlight_doing')
												->orderByRaw('LENGTH(orders.number)', 'ASC')
												->orderBy('orders.number', 'asc')
												->select(
														'contents.id as id',
														'contents.name_th as name_th',
														'contents.name_eng as name_eng' ,
														// 'contents.font_id as font_id' ,
														'contents.category_content_id as category_content_id' ,
														)
												->limit(8)->get();
		$category_contents = App\CategoryContent::where('manu_option_id', App\ManuOption::$doing_id )->where('name_eng','!=','No category')->get();
	@endphp
	@if(count($contents) != 0)
		<div class="container content pt-5 mb-5" data-aos="fade-up" data-aos-duration="500">
			<h1 class="header-text language-th mb-4">{{$doing->name_th}}</h1>
			<h1 class="header-text language-eng mb-4">{{$doing->name_eng}}</h1>
			<div class="row row-p10 justify-content-md-center mb-4" id="category-contents-filter"></div>
			<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-p10">
				@foreach ($contents as $content)
				<div class="col mb-4">
					<a href="{{url('/our-work/detail/'.$content->id)}}" class="card-link">
						<div class="card">
							<div class="card-img">
								@isset($content->images[0]->path)
									<img data-src="{{url($content->images[0]->path)}}" alt="{!! $content->name_eng !!}" class="lazyload">
								@endisset
								@isset($content->images[1]->path)
									<img data-src="{{url($content->images[1]->path)}}" alt="{!! $content->name_eng !!}" class="hover-img lazyload">
								@endisset
							</div>
							<div class="card-body">
								<h5 class="card-title language-th">{!! $content->name_th !!}</h5>
								<h5 class="card-title language-eng">{!! $content->name_eng !!}</h5>
								<hr>
								<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_th']) !!}</p>
								<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('work_detail')['detail_th']) !!}</p>
								<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_eng']) !!}</p>
								<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('work_detail')['detail_eng']) !!}</p>
								{{-- <p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_th'].' / งบประมาณ '.$content->getDetailByTypeDetail('budget')['detail_th'].' / '.$content->getDetailByTypeDetail('work_detail')['detail_th']) !!}</p>
								<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_eng'].' / Budget was '.$content->getDetailByTypeDetail('budget')['detail_eng'].' / '.$content->getDetailByTypeDetail('work_detail')['detail_eng']) !!}</p> --}}
							</div>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	@endif

	{{-- Voice of customer --}}
	@php
		$comment = App\ManuOption::where('manu_options.id' , App\ManuOption::$comment_id )->first();
		$customer_contents = App\Content::where('contents.manu_option_id', App\ManuOption::$comment_id )
							->where('name_th','comment.customer')
							->where('name_eng','comment.customer')
							->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
							->where('orders.type','comment')
							->orderByRaw('LENGTH(orders.number)', 'ASC')
							->orderBy('orders.number', 'ASC')
							->select(
								'contents.id as id',
								'contents.name_th as name_th',
								'contents.name_eng as name_eng' ,
								'contents.category_content_id as category_content_id' ,
								'orders.type as type',
								)
							->get();  
		// $customer_youtubes = App\Content::where('contents.manu_option_id', App\ManuOption::$comment_id )
		// 					->where('name_th','comment.youtube')
		// 					->where('name_eng','comment.youtube')
		// 					->inRandomOrder()
		// 					->take(3)
		// 					->get(); 
		$customer_youtubes = App\Content::where('contents.manu_option_id', App\ManuOption::$comment_id )
							->where('name_th','comment.youtube')
							->where('name_eng','comment.youtube')
							->leftJoin('orders', 'orders.content_id', '=', 'contents.id')

							//->orderByRaw('ISNULL(orders.number), orders.number ASC')
							//->orderBy('orders.number', 'asc')
							->orderByRaw('LENGTH(orders.number)', 'ASC')
        					->orderBy('orders.number', 'ASC')
							->select(
									'contents.id as id',
									'contents.name_th as name_th',
									'contents.name_eng as name_eng' ,
									// 'contents.font_id as font_id' ,
									'contents.category_content_id as category_content_id' ,
									)
							->get();
	@endphp
	
	@if( count($customer_contents) != 0 && count($customer_youtubes) != 0)
	<div class="container content pt-5 mb-5" data-aos="fade-up" data-aos-duration="500">
		<h1 class="header-text language-th">{{$comment->name_th}}</h1>
		<h1 class="header-text language-eng">{{$comment->name_eng}}</h1>
		<div class="slick-carousel comment-carousel">
			@foreach ($customer_contents as $idx => $content)
				<div class="card card-customer-voices">
					<div class="card-body">
						<div class="person-img">
							<img data-src="{{url($content->main_image->path)}}" alt="customer_{{$idx}}" class="lazyload">
						</div>
						<div class="d-flex align-items-baseline mb-3">
							<h5 class="card-title language-th">{!! $content->getDetailByTypeDetail('name')['detail_th'] !!}</h5>
							<h5 class="card-title language-eng">{!! $content->getDetailByTypeDetail('name')['detail_eng'] !!}</h5>
							<div class="col-divider"></div>
							<p class="card-text language-th">{!! $content->getDetailByTypeDetail('position')['detail_th'] !!}</p>
							<p class="card-text language-eng">{!! $content->getDetailByTypeDetail('position')['detail_eng'] !!}</p>
						</div>
						<div class="language-th text-center">
							@php
								echo $content->getDetailByTypeDetail('comment')['detail_th'];
							@endphp
						</div>
						<div class="language-eng text-center">
							@php
								echo $content->getDetailByTypeDetail('comment')['detail_eng'];
							@endphp
						</div>
					</div>
				</div>
			@endforeach
		</div>
		{{-- <div class="row row-cols-1 row-cols-md-3 row-p10">
			@foreach ($customer_youtubes as $customer_youtube)
			<div class="col mt-3">
				<div class="card">
					<div class="card-youtube">
						@php
							$iframe = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe width=\"100%\" height=\"100%\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" title=\"youtubeiframe\" allowfullscreen></iframe>",$customer_youtube->getDetailByTypeDetail('path_youtube')['detail_all']);
							echo $iframe;
						@endphp
					</div>
					<div class="card-body pt-3 card-youtube-text">
						<div class="language-th">
						@php
							echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_th'];
						@endphp
						</div>
						<div class="language-eng">
						@php
							echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_eng'];
						@endphp
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div> --}}
		<div class="slick-carousel yt-carousel py-3">
			@foreach ($customer_youtubes as $customer_youtube)
				<div class="card">
					<div class="card-youtube">
						@php
							$iframe = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe width=\"100%\" height=\"100%\" data-src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" title=\"youtubeiframe\" allowfullscreen class=\"lazyload\"></iframe>",$customer_youtube->getDetailByTypeDetail('path_youtube')['detail_all']);
							echo $iframe;
						@endphp
					</div>
					<div class="card-body py-3 card-youtube-text">
						<div class="language-th">
						@php
							echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_th'];
						@endphp
						</div>
						<div class="language-eng">
						@php
							echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_eng'];
						@endphp
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	@endif

	{{-- Additional Services --}}
	@php
		$additional_service = App\ManuOption::where('manu_options.id' , App\ManuOption::$additional_service_id )->first();
		$contents = App\Content::where('contents.manu_option_id', App\ManuOption::$additional_service_id )->orderByDesc('id')->limit(4)->get(); 
	@endphp

	@if( count($contents) != 0 )
	<div class="container content additional-services pt-5 mb-5" data-aos="fade-up"
     data-aos-anchor-placement="top-bottom" data-aos-duration="500">
		<h1 class="header-text language-th">{{$additional_service->name_th}}</h1>
		<h1 class="header-text language-eng">{{$additional_service->name_eng}}</h1>
		<div class="row row-cols-1 row-cols-sm-2 row-cols-lg-4 row-p10">
			@foreach ($contents as $content)
			<div class="col d-flex justify-content-center mb-4">
				<a href="{{url('/additional-service/detail/'.$content->id)}}" class="card-link">
					<div class="card">
						<div class="card-img">
							<img src="{{url($content->detail_is_main_image->path)}}" alt="{!! $content->name_eng !!}">
							@isset($content->hero_image->path)
								<img data-src="{{url($content->hero_image->path)}}" alt="{!! $content->name_eng !!}" class="hover-img lazyload">
							@endisset
						</div>
						<div class="card-body">
							<h5 class="card-title language-th">{!! $content->name_th !!}</h5>
							<h5 class="card-title language-eng">{!! $content->name_eng !!}</h5>
							<hr>
							<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('detail')['detail_th']) !!}</p>
							<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('detail')['detail_eng']) !!}</p>
						</div>
					</div>
				</a>
			</div>
			@endforeach			
		</div>
	</div>
	@endif
@endsection

@section('script')
	<script>
		//  set data
		var header = JSON.parse('<?php print_r(json_encode($header)); ?>');
		var category_contents = JSON.parse('{!! json_encode($category_contents) !!}');
		// var content_category_content_id = JSON.parse('{!! json_encode($category_contents[0]->id) !!}');
		var content_category_content_id = 0 ;
		now_manu = header.link ;

		$(window).on('load', function() {
    	AOS.refresh();
		})

		$(document).ready(function () {
    	AOS.init();
			let header = $('.header');
			header.addClass('header-darken');
			let card_img = $('.additional-services .card-img');
			let carousel = $('#header-carousel');
			let card_youtube = $('.card-youtube');
			let yt_carousel = $('.yt-carousel');
			
			if(card_img.length > 0) {
				$(window).resize(function () {
					set_min_height()
				});

				setInterval(() => {
					set_min_height();
				}, 250);

				function set_min_height() {
					let width = card_img[0].getBoundingClientRect().width;
					card_img.css('height', width + 'px');
				}
			}
			
			if(card_youtube.length > 0) {
				$(window).resize(function () {
					set_min_yheight()
				});

				setInterval(() => {
					set_min_yheight();
				}, 250);

				function set_min_yheight() {
					let height = card_youtube[0].getBoundingClientRect().height
					let half = (height/2)
					ah = half + 10
					yt_carousel.find('.slick-arrow').css('top', ah)
					yt_carousel.find('.slick-arrow').css('transform', 'unset')
				}
			}

			$(window).scroll(function() {
				let carousel_height = carousel[0].getBoundingClientRect().height;
				let page_top = $(window).scrollTop();
				if(page_top >= carousel_height) {
					header.removeClass('header-darken');
				} else {
					header.addClass('header-darken');
				}
			});

			$('.comment-carousel').slick({
  			dots: true,
				infinite: true,
				slidesToShow: 1,
				swipeToSlide: true,
				touchThreshold: 100,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 576,
						settings: {
        			arrows: true,
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 767,
						settings: {
        			arrows: true,
							slidesToShow: 3,
						}
					},
				]
			});

			$('.yt-carousel').slick({
        arrows: false,
  			dots: true,
				infinite: true,
				slidesToShow: 1,
				swipeToSlide: true,
				touchThreshold: 100,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 576,
						settings: {
        			arrows: true,
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 767,
						settings: {
        			arrows: true,
							slidesToShow: 3,
						}
					},
				]
			});

      // $('.slick-carousel').on('wheel', (function(e) {
      //   e.preventDefault();

      //   if (e.originalEvent.deltaY < 0) {
      //     $(this).slick('slickNext');
      //   } else {
      //     $(this).slick('slickPrev');
      //   }
      // }));
		});
	</script>
@endsection

