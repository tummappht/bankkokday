@extends('layout.main_user')

@section('content')
	<div class="container content pt-3 pb-4">
		<h1 class="header-text mb-3" id="doing_name">มีดีมาโชว์</h1>
		<div class="row row-p10 justify-content-md-center mb-4" id="category-contents-filter"></div>
		@php
			$check_content = json_encode($contents);
			$check_content = json_decode($check_content);
			$check_content = (array)$check_content;
		@endphp
		@if (!empty($check_content['data']))
			<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-p10">
				@foreach ($contents as $content)
				<div class="col mb-4">
					<a href="{{url('/our-work/detail/'.$content->id)}}" class="card-link">
						<div class="card">
							<div class="card-img">
								@isset($content->images[0]->path)
									<img data-src="{{url($content->images[0]->path)}}" alt="{!! $content->name_eng !!}" class="lazyload">
								@endisset
								@isset($content->images[1]->path)
									<img data-src="{{url($content->images[1]->path)}}" alt="{!! $content->name_eng !!}" class="hover-img lazyload">
								@endisset
							</div>
							<div class="card-body">
								<h5 class="card-title language-th">{!! $content->name_th !!}</h5>
								<h5 class="card-title language-eng">{!! $content->name_eng !!}</h5>
								<hr>
								<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_th']) !!}</p>
								<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('work_detail')['detail_th']) !!}</p>
								<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('location')['detail_eng']) !!}</p>
								<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('work_detail')['detail_eng']) !!}</p>
							</div>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		@else
			<div class="col-12 py-5">
				<p class="text-center font-italic font-SemiBold text-12px language-th">ไม่มีข้อมูล</p>
				<p class="text-center font-italic font-SemiBold text-12px language-eng">No data available</p>
			</div>
		@endif
		{{ $contents->links() }}
	</div>
@endsection
@section('script')
	<script>
		//  set data
		var doing = JSON.parse('{!! json_encode($doing) !!}');
		var contents = JSON.parse('{!! json_encode($contents) !!}');
		var category_contents = JSON.parse('{!! json_encode($category_contents) !!}');
		var content_category_content_id = JSON.parse('{!! isset($filter_category_content_id) ? json_encode($filter_category_content_id) : 1 !!}');
		now_manu = doing.link;
	</script>
@endsection

