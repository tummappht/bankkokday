@extends('layout.main_user')

@section('content')

	<div class="container content additional-services pt-5 pb-4">
		<h1 class="header-text language-th">{{$additional_service->name_th}}</h1>
		<h1 class="header-text language-eng">{{$additional_service->name_eng}}</h1>
		<div class="row row-cols-1 row-cols-sm-2 row-cols-lg-4 row-p10">
			@foreach ($contents as $content)
			<div class="col d-flex justify-content-center mb-4">
				<a href="{{url('/additional-service/detail/'.$content->id)}}" class="card-link">
					<div class="card">
						<div class="card-img">
							<img src="{{url($content->detail_is_main_image->path)}}" alt="{!! $content->name_eng !!}">
							@isset($content->hero_image->path)
								<img src="{{url($content->hero_image->path)}}" alt="{!! $content->name_eng !!}" class="hover-img">
							@endisset
						</div>
						<div class="card-body">
							<h5 class="card-title language-th">{!! $content->name_th !!}</h5>
							<h5 class="card-title language-eng">{!! $content->name_eng !!}</h5>
							<hr>
							<p class="card-text language-th">{!! strip_tags($content->getDetailByTypeDetail('detail')['detail_th']) !!}</p>
							<p class="card-text language-eng">{!! strip_tags($content->getDetailByTypeDetail('detail')['detail_eng']) !!}</p>
						</div>
					</div>
				</a>
			</div>
			@endforeach
			{{ $contents->links() }}
		</div>
	</div>
@endsection
@section('script')
    <script>
		//  set data
		var additional_service = JSON.parse('{!! json_encode($additional_service) !!}');
        now_manu = additional_service.link;

		$(document).ready(function () {
			let card_img = $('.additional-services .card-img');

			$(window).resize(function () {
				set_min_height()
			});

			function set_min_height() {
				let win_height = window.innerHeight;
				let width = card_img[0].getBoundingClientRect().width;
				card_img.css('height', width + 'px');
			}

			setInterval(() => {
				set_min_height();
			}, 250);
		});
    </script>
@endsection