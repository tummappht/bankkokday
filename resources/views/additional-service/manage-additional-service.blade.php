@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
@endsection
@section('content')
    
	<div class="container-fluid">
		<h4 class="header-text">จัดการอาชีพเสริม</h4>
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-end">
					<a href="{{url('/admin/additional-services/add-content')}}" class="btn btn-secondary d-flex mb-3">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่มอาชีพเสริม
					</a>
				</div>
				<table class="table table-striped table-with-image w-100 table-responsive" id="table-additional">
					<thead>
						<th class="thin-cell no-sort sorting_disabled">ลำดับ</th>
						<th class="thin-cell no-sort">หัวข้ออาชีพเสริม</th>
						<th class="no-sort">รายละเอียดเบื้องต้น</th>
						<th class="thin-cell no-sort"></th>
					</thead>
					<tbody>
						@php
							$count = 1;
						@endphp
						@foreach ($contents as $content)
							<tr>
								<td class="text-center">
									{{$count++}}.
								</td>
								<td>
									{{$content->name_th}}
									<br>
									{{$content->name_eng}}
								</td>
								<td>
									{{strip_tags($content->getDetailByTypeDetail('detail')['detail_th'])}}
									<br>
									{{strip_tags($content->getDetailByTypeDetail('detail')['detail_eng'])}}
								</td>
								<td class="d-flex">
									<a href="{{url('admin/additional-services/'.$content->id.'/edit-content')}}" class="btn btn-light border border-dark mr-2">
										<i class="far fa-edit"></i>
									</a>
									<a  href = "#" onclick="deleteByURL('{{url('admin/additional-services/')}}' , '{{$content->id}}' , 'delete' );" class="btn btn-outline-danger">
										<i class="far fa-trash-alt"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script >
		//  set data
		var additional_service = JSON.parse('<?php print_r(json_encode($additional_service)); ?>');
		now_manu = additional_service.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#table-additional').DataTable({
			'columnDefs': [ {
				'targets'  : 'no-sort',
				'orderable': false,
			}],
			'bLengthChange' : false,
			'bInfo': false,
			'bPaginate': false,
			'bFilter': false,
		});
	});
</script>
@endsection

