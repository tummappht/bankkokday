<?php

use Illuminate\Database\Seeder;
use App\ManuOption ;

class CategoryContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_contents')->insert([
            [ 'id'=>1, 'name_th'=>'สิ่งปลูกสร้าง' , 'name_eng'=>'category 1' , 'manu_option_id'=>ManuOption::$doing_id ],
            [ 'id'=>2, 'name_th'=>'ที่อยู่' , 'name_eng'=>'category 2' , 'manu_option_id'=>ManuOption::$doing_id ],
            [ 'id'=>3, 'name_th'=>'ที่กินเที่ยว' , 'name_eng'=>'category 3' , 'manu_option_id'=>ManuOption::$doing_id ],
            [ 'id'=>4, 'name_th'=>'ที่ทำงาน' , 'name_eng'=>'category 4' , 'manu_option_id'=>ManuOption::$doing_id ],
            [ 'id'=>5, 'name_th'=>'ที่อยู่แนวสูง' , 'name_eng'=>'category 5' , 'manu_option_id'=>ManuOption::$doing_id ],
            [ 'id'=>6, 'name_th'=>'โรงแรมโรงพยาบาลและอื่นๆ' , 'name_eng'=>'category 6' , 'manu_option_id'=>ManuOption::$doing_id ]
        ]);
    }
}
