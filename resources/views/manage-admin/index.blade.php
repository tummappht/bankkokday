@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">จัดการผู้ดูแลระบบ</h4>
		<div class="card">
			<div class="card-body sub-card">
				<div class="d-flex justify-content-end">
					{{-- <a href="{{url('/admin/manage-admin/add-admin')}}" class="btn btn-secondary d-flex mb-3"> --}}
					<a href="#" class="btn btn-secondary d-flex mb-3" data-toggle="modal" data-target="#modal_add_admin">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่มผู้ดูแลระบบ
					</a>
				</div>
				<table class="table table-striped table-with-image w-100 table-responsive" id="table-comment">
					<thead>
						<th>ชื่อ</th>
						<th>อีเมล์</th>
						<th class="thin-cell">สถานะ</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td>
								<span class="d-none">{{$user->id}}</span>
								{{$user->name}}
							</td>
							<td>
								{{$user->email}}
							</td>
							<td class="text-nowrap">
								@if($user->status_active == 1)
										<span class="text-status success">เปิดใช้งาน</span>
								@else
										<span class="text-status danger">ปิดใช้งาน</span>
								@endif
							</td>
							<td class="d-flex">
								<a class="btn btn-light border border-dark mr-2" data-toggle="modal" data-target="#modal-edit-password" onclick="editPassword({{$user->id}});" >
									<i class="fas fa-unlock-alt"></i>
								</a>
								{{-- <a href="{{url('admin/manage-admin/'.$user->id.'/edit-manage-admin')}}" class="btn btn-light border border-dark mr-2" data-toggle="modal" data-target="#modal_edit_admin" data-json="{!! htmlspecialchars(json_encode($user)) !!}"> --}}
								<a href="#" class="btn btn-light border border-dark mr-2" data-toggle="modal" data-target="#modal_edit_admin" data-json="{!! htmlspecialchars(json_encode($user)) !!}">
									<i class="far fa-edit"></i>
								</a>
								@if( $user->id != '1' )
									<a  href = "#" onclick="deleteByURL('{{url('admin/manage-admin/')}}' , '{{$user->id}}' , 'delete' );" class="btn btn-outline-danger">
										<i class="far fa-trash-alt"></i>
									</a>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div> 

    <!-- Modal -->
	<div class="modal fade" id="modal-edit-password" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">แก้ไขรหัสผ่าน</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_edit_password', 'route'=>'admin.manage-admin.edit-password-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
						{!! Form::hidden('user_id' ) !!}
						<div class="form-group">
							<label for="">รหัสผ่านใหม่</label>
							<div class="input-password">
								{!! Form::password('password', ['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('password',$errors) !!}
								<i class="far fa-eye-slash toggle-password"></i>
								{{-- <i class="far fa-eye-slash"></i> --}}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="reset" form="form_edit_password" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_edit_password" class="btn btn-primary">แก้ไข</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="modal_add_admin" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">เพิ่มผู้ดูแลระบบ</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_add_admin', 'route'=>'admin.manage-admin.add-and-edit-admin-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
						{!! Form::hidden('status','add') !!}
							<div class="form-group">
								<label>ชื่อผู้ใช้</label>
								{!! Form::text('name',old('name'),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('name',$errors) !!}
							</div>
							<div class="form-group">
								<label>อีเมล (ใช้สำหรับการเข้าสู่ระบบ)</label>
								{!! Form::text('email',old('email'),['class'=> 'form-control' , 'autocomplete'=>"off", 'required' ]) !!}
								{!! customValidator('email',$errors) !!}
							</div>
							<div class="form-group">
								<label>รหัสผ่าน</label>
								{!! Form::password('password',['class'=> 'form-control' ,'autocomplete'=>"new-password", 'required' ]) !!}
								{!! customValidator('password',$errors) !!}
							</div>
							<div class="form-group">
								<label>สถานะ</label>
								<div class="custom-control custom-radio">
									{!! Form::radio('status_active', '1', true, ['id' => 'active', 'class'=> 'custom-control-input']); !!}
									{!! Form::label('active', 'เปิดใช้งาน', ['class'=> 'custom-control-label']) !!}
								</div>
								<div class="custom-control custom-radio">
									{!! Form::radio('status_active', '0', false, ['id' => 'deactive', 'class'=> 'custom-control-input']); !!}
									{!! Form::label('deactive', 'ปิดใช้งาน', ['class'=> 'custom-control-label']) !!}
								</div>
								{!! customValidator('status_active',$errors) !!}
							</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="reset" form="form_add_admin" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_add_admin" class="btn btn-primary">ยืนยัน</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="modal_edit_admin" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">แก้ไขผู้ดูแลระบบ</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_edit_admin', 'route'=>'admin.manage-admin.add-and-edit-admin-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
            {!! Form::hidden('status','edit') !!}
            {!! Form::hidden('user_id' ) !!}
						<div class="form-group">
							<label>ชื่อผู้ใช้</label>
							{!! Form::text('name',old('name'),['class'=> 'form-control', 'required' ]) !!}
							{!! customValidator('name',$errors) !!}
						</div>
						<div class="form-group">
							<label>สถานะ</label>
							<div class="custom-control custom-radio">
								{!! Form::radio('edit_status_active', '1', false, ['id' => 'eactive', 'class'=> 'custom-control-input', 'required']); !!}

								{!! Form::label('eactive', 'เปิดใช้งาน', ['class'=> 'custom-control-label']) !!}
							</div>
							<div class="custom-control custom-radio">
								{!! Form::radio('edit_status_active', '0', false, ['id' => 'edeactive', 'class'=> 'custom-control-input', 'required']); !!}

								{!! Form::label('edeactive', 'ปิดใช้งาน', ['class'=> 'custom-control-label']) !!}
							</div>
							{!! customValidator('edit_status_active',$errors) !!}
						</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="reset" form="form_edit_admin" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_edit_admin" class="btn btn-primary">ยืนยัน</button>
				</div>
			</div>
		</div>
	</div>

	<script >
		//  set data
		now_manu = 'manage-admin' ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
			$('.table').DataTable({
				'columnDefs': [ {
					'targets'  : 'no-sort',
					'orderable': false,
				}],
				'language': {
					'paginate': {
						'previous': '<',
						'next': '>'
					}
				}
			});

			$('#modal_edit_admin').on('show.bs.modal', event => {
				let button = $(event.relatedTarget);
				let modal = $('#modal_edit_admin');
				let json = button.data('json');
				for(var k in json) {
					let val = json[k];
					modal.find(`[name="${k}"]`).val(val);
				}
				if(json.status_active) {
					$('[name=edit_status_active][value=1]').prop('checked', true);
				} else {
					$('[name=edit_status_active][value=0]').prop('checked', true);
				}
				// user_id
				$('[name="user_id"]').val(json.id);
			});

			$(document).on('click', '.toggle-password', function() {
				$(this).toggleClass('show');
				$(this).toggleClass('show');
				let input = $(this).parents('.input-password').find('input');
				let type = input.attr('type');
				if(type == 'password') {
					$(this).removeClass('fa-eye-slash');
					$(this).addClass('fa-eye');
				} else {
					$(this).addClass('fa-eye-slash');
					$(this).removeClass('fa-eye');
				}
				input.attr('type', type == 'password' ? 'text' : 'password');
			});
    });

    function editPassword(user_id){
			$('[name="user_id"]').val(user_id);
    }

	// $('[name=status_active]').change(function() {
	// 	if (this.value == '0') {
	// 		alert("Allot Thai Gayo Bhai");
	// 	}
	// 	else if (this.value == '1') {
	// 		alert("Transfer Thai Gayo");
	// 	}
	// });
</script>
@endsection

