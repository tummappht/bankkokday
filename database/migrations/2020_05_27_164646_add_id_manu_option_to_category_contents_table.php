<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdManuOptionToCategoryContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_contents', function (Blueprint $table) {
            // id_manu_option
            $table->unsignedBigInteger('manu_option_id')->nullable()->comment('content ที่อยู่ใน manu_option');
            $table->foreign('manu_option_id')->references('id')->on('manu_options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_contents', function (Blueprint $table) {
            $table->dropColumn('manu_option_id');
        });
    }
}
