@extends('layout.main_user')

@section('content')
	<div class="container content about-us">
		<div class="container content pt-4 pb-4">
			<h1 class="header-text language-th">{{$comment->name_th}}</h1>
			<h1 class="header-text language-eng">{{$comment->name_eng}}</h1>
      <div class="slick-carousel comment-carousel">
				@foreach ($customer_contents as $content)
					
					<div class="card card-customer-voices">
						<div class="card-body">
							<div class="person-img">
								<img data-src="{{url($content->main_image->path)}}" class="lazyload">
							</div>
							<div class="d-flex align-items-baseline mb-3">
								<h5 class="card-title language-th">{!! $content->getDetailByTypeDetail('name')['detail_th'] !!}</h5>
								<h5 class="card-title language-eng">{!! $content->getDetailByTypeDetail('name')['detail_eng'] !!}</h5>
								<div class="col-divider"></div>
								<p class="card-text language-th">{!! $content->getDetailByTypeDetail('position')['detail_th'] !!}</p>
								<p class="card-text language-eng">{!! $content->getDetailByTypeDetail('position')['detail_eng'] !!}</p>
							</div>
							<div class="language-th text-center">
								@php
									echo $content->getDetailByTypeDetail('comment')['detail_th'];
								@endphp
							</div>
							<div class="language-eng text-center">
								@php
									echo $content->getDetailByTypeDetail('comment')['detail_eng'];
								@endphp
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<div class="row row-cols-1 row-cols-md-3 row-p10">
				@foreach ($customer_youtubes as $customer_youtube)
				<div class="col mt-3">
					<div class="card mb-3">
						<div class="card-youtube">
							@php
								$iframe = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe width=\"100%\" height=\"100%\" data-src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen class=\"lazyload\"></iframe>",$customer_youtube->getDetailByTypeDetail('path_youtube')['detail_all']);
								echo $iframe;
							@endphp
						</div>
						<div class="card-body py-3 card-youtube-text">
							<div class="language-th">
							@php
								echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_th'];
							@endphp
							</div>
							<div class="language-eng">
							@php
								echo $customer_youtube->getDetailByTypeDetail('detail_youtube')['detail_eng'];
							@endphp
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script>
	//  set data
		var comment = JSON.parse('{!! json_encode($comment) !!}');
		var customer_contents = JSON.parse('{!! json_encode($customer_contents) !!}');
		
		now_manu = comment.link;
		$(document).ready(function () {
			$('.slick-carousel').slick({
  			dots: true,
				infinite: true,
				slidesToShow: 1,
				swipeToSlide: true,
				touchThreshold: 100,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 576,
						settings: {
        			arrows: true,
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 767,
						settings: {
        			arrows: true,
							slidesToShow: 3,
						}
					},
				]
			});
			
			/*$('.slick-carousel').slick({
				infinite: true,
				slidesToShow: 1,
				swipeToSlide: true,
        arrows: false,
				touchThreshold: 100,
        autoplay: true,
        autoplaySpeed: 5000,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 3,
						}
					},
				]
      });*/

      // $('.slick-carousel').on('wheel', (function(e) {
      //   e.preventDefault();

      //   if (e.originalEvent.deltaY < 0) {
      //     $(this).slick('slickNext');
      //   } else {
      //     $(this).slick('slickPrev');
      //   }
      // }));
		});
	</script>
@endsection

