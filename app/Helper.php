<?php
use App\Image ;
use App\ManuOption ;

function customValidator($name,$errors)
{
    if($errors->has($name)){
        $text_error = '<span class="text-danger text-error-th language-th">'.explode("[TO]",$errors->first($name))[0].'</span> ' ;
        $text_error = $text_error.'<span class="text-danger text-error-eng language-eng">'.explode("[TO]",$errors->first($name))[1].'</span> ' ;
        return $text_error ;
        // return '<span class="text-danger">'.$errors->first($name).'</span>' ;
    }
}

function defaultImage($image){
    if($image == null ){
        // $image->path = null ;
        $image = new Image();
        $image->path = url(ManuOption::$default_image_path) ;
    }

    return $image ;
}

?>