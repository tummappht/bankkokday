@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
@endsection
@section('content')

    <div class="container">
        {!! Form::open(['route'=>'admin.contact.add-and-edit-email-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
            {!! Form::hidden('status','edit') !!}
            {!! Form::hidden('content_id',$content_id) !!}
            <div class="row">
                <div class="col-lg-6">
                    name th {!! Form::text('name_th',old('name_th', $content->getDetailByTypeDetail('name')['detail_th'] ),['class'=> 'form-control' ]) !!}
                    {!! customValidator('name_th',$errors) !!}
                </div>
                <div class="col-lg-6">
                    name eng {!! Form::text('name_eng',old('name_eng' , $content->getDetailByTypeDetail('name')['detail_eng'] ),['class'=> 'form-control' ]) !!}
                    {!! customValidator('name_eng',$errors) !!}
                </div>
                <div class="col-lg-12">
                    Email {!! Form::text('email',old('email' , $content->getDetailByTypeDetail('email')['detail_all'] ),['class'=> 'form-control ' ]) !!}
                    {!! customValidator('email',$errors) !!}
                </div>

                </br>
                <input type="submit" class="btn btn-primary btn-primary-blue" value="บันทึก">
            <div>

            
        {!! Form::close() !!}

    </div>


    <script >
        //  set data
        var contact = JSON.parse('<?php print_r(json_encode($contact)); ?>');
        now_manu = contact.link ;
    </script>
@endsection
@section('script')
<script>


</script>
@endsection

