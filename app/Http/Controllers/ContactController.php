<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;
use App\Image ;

class ContactController extends Controller
{
    public function index(){
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        $main_content = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.main')->where('name_eng','contact.main')->first();  
        $email_contents = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.email')->where('name_eng','contact.email')->get();
        // dd($email_contents);
        $qr_contents =Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.qr')->where('name_eng','contact.qr')->get();
        return view('contact.show-contact', compact('contact','email_contents','qr_contents','main_content'));
    }

    public function manageContact()
    {
        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        // dd('manageIntroduction');
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        $content = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.main')->where('name_eng','contact.main')->first();
        $email_contents = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.email')->where('name_eng','contact.email')->get();
        $qr_contents =Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.qr')->where('name_eng','contact.qr')->get();
        $send_email_contents =Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.send_email')->where('name_eng','contact.send_email')->get();
        $fonts = Font::pluck('name', 'id');
        // dd($contact);
        // dd( Content::where('contents.manu_option_id', ManuOption::$introduction_id )->get()[0]->getDoingContentDetailByTypeDetail(null)  );
        return view('contact.manage-contact', compact('contact','fonts','content','email_contents','qr_contents','send_email_contents'));
    }

    public function editContact(Request $request){

        $validator = $this->validatorEditContact($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        // dd($request->status);
        (new HelperController())->detailSave($request , $request->content_id , 'link_map' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $request->content_id , 'address' , 'th-and-eng');
        (new HelperController())->detailSave($request , $request->content_id , 'facebook' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $request->content_id , 'line' , 'no-th-and-eng');

        session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);

        return redirect('admin/contact'); 
    }

    public function validatorEditContact($request){
        $validator = Validator::make(
            $request->all(), [
            'link_map' => 'required',
            'address_th' => 'required',
            'address_eng' => 'required',
            'facebook' => 'required',
            'line' => 'required',
        ], [
            'address_th.required' => "กรุณากรอกที่อยู่ภาษาไทย[TO]The detail TH field is required.",
            'address_eng.required' => 'กรุณากรอกที่อยู่ภาษาอังกฤษ[TO]The detail ENG field is required.',
            'link_map.required' => 'กรุณากรอกลิงก์แผนที่[TO]The font field is required.',
            'facebook.required' => 'กรุณากรอก Facebook[TO]The facebook field is required.',
            'line.required' => 'กรุณากรอก Line[TO]The line field is required.',
        ]);

        return $validator ;
    }

    public function addContentEmail(){
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        return view('contact.manage-add-content-email', compact('contact'));
    }

    public function editEmailContent($content_id){
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        $content = Content::where('id',$content_id)->first();

        return view('contact.manage-edit-content-email', compact('content','contact','content_id'));
    }

    public function addAndEditEmailSaveContent(Request $request){
        // dd($request);
        $validator = $this->validatorAddAndEditEmailSaveContent($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $content_name_th = $request->name_th ;
        $content_name_eng = $request->name_eng ;
        $request->name_th = 'contact.email';
        $request->name_eng = 'contact.email';
        
        $content_id = (new HelperController())->contentSave($request , ManuOption::$contact_id );
// dd($request);
        $request->name_th = $content_name_th ;
        $request->name_eng = $content_name_eng ;

        (new HelperController())->detailSave($request , $content_id , 'name' , 'th-and-eng');

        (new HelperController())->detailSave($request , $content_id , 'email' , 'no-th-and-eng');
      
        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        }
        session(['editEmailContent' => $content_id ]);
        return redirect('admin/contact'); 
    }

    public function validatorAddAndEditEmailSaveContent($request){

        $validator = Validator::make(
            $request->all(), [
            'name_th' => 'required',
            'name_eng' => 'required',
            'email' => 'required',
        ], [
            'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
            'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
            'email.required' => 'กรุณากรอกอีเมล[TO]The font field is required.',
        ]);

        return $validator ;
    }

    public function deleteContent($content_id){
        Detail::where('content_id',$content_id)->delete();
        Image::where('content_id',$content_id)->delete();
        Content::where('id',$content_id)->delete();
        session(['status_manage' => 'ลบข้อมูลสำเร็จ']);
        return redirect('admin/contact');  
    }

    public function addContentQR(){
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        return view('contact.manage-add-content-qr', compact('contact'));
    }

    
    public function editContentQR($content_id){
        $contact = ManuOption::where('manu_options.id' , ManuOption::$contact_id )->first();
        $content = Content::where('id',$content_id)->first();
        // dd(public_path('images'));
        // dd($content->images);
        // dd($content->getDetailByTypeDetail('tel')['detail_all']);
        return view('contact.manage-edit-content-qr', compact('content','contact','content_id'));
    }

    public function addAndEditQRSaveContent(Request $request){
        // dd($request);
        $validator = $this->validatoraddAndEditQRSaveContent($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if( $request->status == 'edit' ){
            if(  $request->file('files') && count(Image::where('content_id' , $request->content_id)->get()) >= 1 ){
                // dd('ds');
                return back() 
                ->withInput()
                ->withErrors(['icon'=>'error','title_th'=>'ไม่สามารถเพิ่มรูปได้','text_th'=>'เนื่องจากมีรูปอยู่แล้ว','title_eng'=>'you can not add images','text_eng' => 'images is full']);
            }
        }

        if(  $request->file('files') && count( $request->file('files') ) > 1 ){
            return back()
            ->withInput() 
            ->withErrors(['icon'=>'error','title_th'=>'ไม่สามารถเพิ่มรูปได้','text_th'=>'เนื่องจากสามารถใส่ได้ 1 รูป','title_eng'=>'you can not add images','text_eng' => 'images is 1 ']);
        }
        

        $content_name_th = $request->name_th ;
        $content_name_eng = $request->name_eng ;
        $request->name_th = 'contact.qr';
        $request->name_eng = 'contact.qr';
        // dd($request->type == 'คน');
        $content_id = (new HelperController())->contentSave($request , ManuOption::$contact_id );

        (new HelperController())->detailSave($request , $content_id , 'name' , 'th-and-eng');
        $type = $request->type ;
        (new HelperController())->detailSave($request , $content_id , 'type' , 'no-th-and-eng');
        if($type == 'คน'){
            (new HelperController())->detailSave($request , $content_id , 'line' , 'no-th-and-eng');
            (new HelperController())->detailSave($request , $content_id , 'tel' , 'no-th-and-eng');
            (new HelperController())->detailSave($request , $content_id , 'email' , 'no-th-and-eng');
        }else{
            (new HelperController())->detailSave($request , $content_id , 'link' , 'no-th-and-eng');
        }


        $request->name_input_files = 'files' ;
        $request->name_file = 'images/contact' ;
        $request->content_id = $content_id ;
        (new HelperController())->filesSave($request);

        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        }
        
        return redirect('admin/contact/'.$content_id.'/edit-qr-content'); 
        // return redirect('admin/contact'); 
    }

    public function validatoraddAndEditQRSaveContent($request){
        // dd( $request->file('files') );
        if($request->type == null){
            $validator = Validator::make(
                $request->all(), [
                'type' => 'required',
            ], [
                'type.required' => "กรุณาเลือกประเภท[TO]The detail TH field is required.",
            ]);
    
            // return $validator ;
        }else{
            if($request->type == 'คน'){
                $validator = Validator::make(
                    $request->all(), [
                    'name_th' => 'required',
                    'name_eng' => 'required',
                    'line' => 'required',
                    'tel' => 'required',
                    'email' => 'required',
                    // 'files' => 'required',
                ], [
                    'name_th.required' => "กรุณากรอกชื่อภาษาไทย[TO]The detail TH field is required.",
                    'name_eng.required' => "กรุณากรอกชื่อภาษาอังกฤษ[TO]The detail TH field is required.",
                    'line.required' => "กรุณากรอก ID line[TO]The detail TH field is required.",
                    'tel.required' => "กรุณากรอกเบอร์โทรศัพท์[TO]The detail TH field is required.",
                    'email.required' => "กรุณากรอก Email[TO]The detail TH field is required.",
                    // 'files.required' => "กรุณาเลือกไฟล์[TO]The detail TH field is required.",
                ]);
        
                // return $validator ;
            }else{
                $validator = Validator::make(
                    $request->all(), [
                    'name_th' => 'required',
                    'name_eng' => 'required',
                    'link' => 'required',
                    // 'files' => 'required',
                ], [
                    'name_th.required' => "กรุณากรอกชื่อภาษาไทย[TO]The detail TH field is required.",
                    'name_eng.required' => "กรุณากรอกชื่อภาษาอังกฤษ[TO]The detail TH field is required.",
                    'link.required' => "กรุณากรอกลิงก์[TO]The detail TH field is required.",
                    // 'files.required' => "กรุณาเลือกไฟล์[TO]The detail TH field is required.",
                ]);
            }

            if( $request->file('files') == null && $request->status == 'add' ){
                $validator->errors()->add('files', 'กรุณาเลือกไฟล์[TO]The detail TH field is required'); // add err
            }
        }

        // if($request->files)

        return $validator ;
    }
    public function sendEmail(Request $request){
        // dd($request);
        $validator = $this->validatorSendEmail($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        // dd($request->all());
        $detail_email = $request->all();
        $email_contents = Content::where('contents.manu_option_id', ManuOption::$contact_id )->where('name_th','contact.email')->where('name_eng','contact.email')->get();
        
        // dd($detail_email->all());
        $content_id = $this->saveSendEmail($request);
        (new MailController())->basic_email($email_contents , $detail_email ,$content_id);
        
        // MailController
        return redirect('contact'); 
    }

    public function validatorSendEmail($request){
        $validator = Validator::make(
            $request->all(), [
            'name' => 'required',
            'email' => 'required',
            'tel' => 'required',
            'detail' => 'required',
        ], [
            'name.required' => "กรุณากรอกชื่อ[TO]The name field is required.",
            'email.required' => "กรุณากรอกชื่อ Email[TO]The email field is required.",
            'tel.required' => "กรุณากรอกเบอร์มือถือ[TO]The tel field is required.",
            'detail.required' => "กรุณากรอกรายละเอียด[TO]The detail field is required.",
        ]);

        return $validator ;
    }

    public function saveSendEmail($request){
        
        $request->request->add(['name_th' => 'contact.send_email']);
        $request->request->add(['name_eng' => 'contact.send_email']);
        $request->request->add(['status' => 'add']);
        $request->request->add(['is_read' => '0']);
        // dd($request);
        // dd($request->type == 'คน');
        $content_id = (new HelperController())->contentSave($request , ManuOption::$contact_id );

        (new HelperController())->detailSave($request , $content_id , 'name' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $content_id , 'email' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $content_id , 'tel' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $content_id , 'detail' , 'no-th-and-eng');
        (new HelperController())->detailSave($request , $content_id , 'is_read' , 'no-th-and-eng');
        return $content_id ;
    }

    public function detailSendEmailContent($content_id ){
        $detail = Detail::where('content_id',$content_id)->where('type_detail', 'is_read' )->first();
        $detail->detail_all = '1' ;
        $detail->save();
        $content = Content::where('id',$content_id)->first();
        return view('contact.manage-detail-content-send-email', compact('content'));
    }

}
