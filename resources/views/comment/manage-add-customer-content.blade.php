@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.comment.add-and-edit-save-customer-content', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
			{!! Form::hidden('status','add') !!}
			<h4 class="header-text">เพิ่มความเห็นจากลูกค้า</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>ชื่อ (TH)</label>
								{!! Form::text('name_th',old('name_th'),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('name_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ชื่อ (EN)</label>
								{!! Form::text('name_eng',old('name_eng'),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('name_eng',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ตำแหน่ง (TH)</label>
								{!! Form::text('position_th',old('position_th'),['class'=> 'form-control ', 'required' ]) !!}
									{!! customValidator('position_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ตำแหน่ง (EN)</label>
								{!! Form::text('position_eng',old('position_eng'),['class'=> 'form-control ', 'required' ]) !!}
								{!! customValidator('position_eng',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ความคิดเห็น (TH)</label>
								{!! Form::textarea('comment_th',old('comment_th'),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('comment_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>ความคิดเห็น (EN)</label>
								{!! Form::textarea('comment_eng',old('comment_eng'),['class'=> 'form-control summernote', 'required' ]) !!}
								{!! customValidator('comment_eng',$errors) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>รูปภาพ</label>
								<div class="file-loading">
									<input name="files[]" type="file" class="files" multiple>
								</div>
								{!! customValidator('files',$errors) !!}
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
				</div>
			</div>
		{!! Form::close() !!}
	</div>


	<script >
		//  set data
		var comment = JSON.parse('<?php print_r(json_encode($comment)); ?>');
		now_manu = comment.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
	
    $('.summernote').summernote({
			height: 200,
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['codeview', 'help']],
			],
			fontNames: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
			fontNamesIgnoreCheck: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
    });

		$(".files").fileinput({
			maxFileCount: 1,
			validateInitialCount: true,
			language: 'en',
			showCaption: true,
			showPreview: true,
			showRemove: true,
			showUpload: false, // <------ just set this from true to false
			showCancel: true,
			showUploadedThumbs: true,
			autoOrientImage: false,
			browseOnZoneClick: true,
			allowedFileExtensions: ["jpg", "png"]
		});
</script>
@endsection

