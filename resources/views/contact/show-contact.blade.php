@extends('layout.main_user')

@section('content')
	<div class="container content contact-us pt-5 pb-4">
		<h1 class="header-text language-th mb-4">{{$contact->name_th}}</h1>
		<h1 class="header-text language-eng mb-4">{{$contact->name_eng}}</h1>
		{!! Form::open(['route'=>'contact.send-email','method', 'POST', 'class' => 'form mb-3' , 'enctype' => "multipart/form-data"]) !!}
			<div class="form-group">
				{!! Form::text('name',old('name'),['class'=> 'form-control ' , 'placeholder'=>""  ]) !!}
				{!! customValidator('name',$errors) !!}
			</div>
			<div class="form-group">
				{!! Form::text('email',old('email'),['class'=> 'form-control ' , 'placeholder'=>"" ]) !!}
				{!! customValidator('email',$errors) !!}
			</div>
			<div class="form-group">
			{!! Form::text('tel',old('tel'),['class'=> 'form-control ' , 'placeholder'=>"" ]) !!}
			{!! customValidator('tel',$errors) !!}
			</div>
			<div class="form-group">
				{!! Form::textarea('detail',old('detail'),['class'=> 'form-control mb-10px' , 'placeholder'=>'' ]) !!}
				{!! customValidator('detail',$errors) !!}
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary btn-submit" value="บันทึก" id = 'bt-send'>
			</div>
			<div class="form-group noti-row">
				<div id="toast-container">
					<div class="toast toast-success" aria-live="polite" style="">
						<button type="button" class="toast-close-button" role="button">×</button>
						<div class="toast-message"></div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		<div class="d-flex justify-content-center align-items-center flex-column mb-3">
			{{-- <div> --}}
				@foreach( $email_contents as $email_content )
					<p class="text-10px font-Light mb-0">
						<span class="language-th">
							{{$email_content->getDetailByTypeDetail('name')['detail_th']}}
						</span>
						<span class="language-eng">
							{{$email_content->getDetailByTypeDetail('name')['detail_eng']}}
						</span>
						: 
						<a target="_blank" href = "mailto: {{$email_content->getDetailByTypeDetail('email')['detail_all']}}">{{$email_content->getDetailByTypeDetail('email')['detail_all']}}
						</a>
					</p>
				@endforeach
			{{-- </div> --}}
			
			<a target="_blank" href="{{$main_content->getDetailByTypeDetail('link_map')['detail_all']}}" class="link-primary">
				<p class="text-10px font-SemiBold">
					<span class="language-th">แผนที่ </span>
					<span class="language-eng">Map </span>
					{{$main_content->getDetailByTypeDetail('link_map')['detail_all']}}
				</p>
			</a>
		</div>

		<h1 class="text-13px font-SemiBold text-center language-th">บริษัท เดย์ ดีเวลลอป จำกัด</h1>
		<h1 class="text-13px font-SemiBold text-center language-eng">DAY develop co.,ltd.</h1>
		<h2 class="text-12px text-center font-Light font-italic language-th">(สำนักงานใหญ่)</h2>
		<h2 class="text-12px text-center font-Light font-italic language-eng">(Headquarters)</h2>
		<div class="location-text mb-5">
			<p class="text-12px font-Light mb-0">
				<span class="language-th">
					{{$main_content->getDetailByTypeDetail('address')['detail_th']}}
				</span> 
				<span class="language-eng">
					{{$main_content->getDetailByTypeDetail('address')['detail_eng']}}
				</span> 
			</p>
			{!! file_get_contents('images/icons/icon-horizon-line.svg') !!}
		</div>
		<div class="qr-card">
			<div class="qr-contrainer">
				@foreach( $qr_contents as $qr_content )
						<div class="card text-center pb-4">
							@if($qr_content->getDetailByTypeDetail('type')['detail_all'] == 'คน')
								@php
									$tel = $qr_content->getDetailByTypeDetail('tel')['detail_all'];
								@endphp
								<div class="qr-header">
									<a target="_blank" href="http://line.me/ti/p/~{{$qr_content->getDetailByTypeDetail('line')['detail_all']}}" class="qr-img">
										<img data-src="https://bangkokdaygroup.com/{{$qr_content->main_image->path}}" alt="" class="lazyload">
									</a>
								</div>
								<p class="text-12px font-SemiBold mb-0">
									<span class="language-th">{{$qr_content->getDetailByTypeDetail('name')['detail_th']}}</span>
									<span class="language-eng">{{$qr_content->getDetailByTypeDetail('name')['detail_eng']}}</span>
								</p>
								<p class="text-12px font-Light mb-0">Line ID : <a target="_blank" href="http://line.me/ti/p/~{{$qr_content->getDetailByTypeDetail('line')['detail_all']}}">{{$qr_content->getDetailByTypeDetail('line')['detail_all']}}</a></p>
								<p class="text-12px font-SemiBold mb-0">tel. <a target="_blank" href="tel:{!! $tel !!}">{!! $tel !!}</a></p>
							@else
								@php
									$type = '';
									switch($qr_content->getDetailByTypeDetail('type')['detail_all']) {
										case 'เฟสบุ๊ค':
										$type = 'Facebook';
											break;
										case 'ไอจี':
										$type = 'Instagram';
											break;
										case 'ทวิตเตอร์':
										$type = 'Twitter';
											break;
									}
								@endphp
								<div class="qr-header">
									<a target="_blank" href="{{$qr_content->getDetailByTypeDetail('link')['detail_all']}}" class="qr-img">
										<img data-src="https://bangkokdaygroup.com/{{$qr_content->main_image->path}}" alt="" class="lazyload">
									</a>
								</div>
								<p class="text-12px font-Light mb-0">{!! $type !!} : </p>
								<p class="text-12px font-SemiBold mb-0">
									<a target="_blank" href="{{$qr_content->getDetailByTypeDetail('link')['detail_all']}}">
										<span class="language-th">{{$qr_content->getDetailByTypeDetail('name')['detail_th']}}</span>
										<span class="language-eng">{{$qr_content->getDetailByTypeDetail('name')['detail_eng']}}</span>
									</a>
								</p>
							@endif
						</div>
				@endforeach
			</div>
		</div>

	</div>
@endsection
@section('script')
    <script>
		//  set data
		var contact = JSON.parse('{!! json_encode($contact) !!}');
				now_manu = contact.link;
    </script>
@endsection