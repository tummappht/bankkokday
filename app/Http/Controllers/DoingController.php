<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ManuOption ;
use App\Font ;
use App\Content;
use App\Detail;
use Validator;
use App\Image ;
use App\CategoryContent;
use App\Order;
use DB;

class DoingController extends Controller
{
    public function index(){
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$doing_id )
                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            ->where('orders.type','highlight_doing')
                            // ->where('orders.number','3')
                            ->orderByRaw('LENGTH(orders.number)', 'ASC')
                            ->orderBy('orders.number', 'ASC')
                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'contents.name_eng as name_eng' ,
                                // 'contents.font_id as font_id' ,
                                'contents.category_content_id as category_content_id' ,
                                )
                            ->take(8)
                            ->paginate(8);
                            // dd($contents);
        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->get();
        $filter_category_content_id = 0 ;
        return view('doing.show-doing', compact('doing','contents','category_contents','filter_category_content_id'));
    }
    public function doingFilter($filter_category_content_id){
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$doing_id )
                            ->where('category_content_id',$filter_category_content_id)
                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            ->where('orders.type','order_doing')
                            // ->where("orders.type","order_doing")
                            // ->orderByRaw('ISNULL(orders.number), orders.number ASC')
                            ->orderByRaw('LENGTH(orders.number)', 'ASC')
                            ->orderBy('orders.number', 'ASC')

                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'contents.name_eng as name_eng' ,
                                // 'contents.font_id as font_id' ,
                                'contents.category_content_id as category_content_id' ,
                                'orders.number'
                                )
                            ->paginate(8);
                            // ->get();
                            // dd($contents->all());
        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->get();
        return view('doing.show-doing', compact('doing','contents','category_contents', 'filter_category_content_id'));
    }

    public function detail($content_id){
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $content = Content::where('id',$content_id)->first();
        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->get();
        return view('doing.detail', compact('doing','content','category_contents'));
    }

    public function manageDoing()
    {
        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        // dd('manageIntroduction');
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$doing_id )->get();
        
        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->get();
        $new_list = array();
        foreach($category_contents as $key => &$category_content) {
            $new_list[$category_content->id] = $category_content;
        }
        $category_contents = $new_list;
        $fonts = Font::pluck('name', 'id');
        // dd($category_contents);
        // list($contents_highlight_number_null , $contents_highlight_number_not_null) = $this->getContentHighlightDoing();

        // dd('test');
        // dd($introduction->contents[0]->detail);
        // dd( Content::where('contents.manu_option_id', ManuOption::$introduction_id )->get()[0]->getDoingContentDetailByTypeDetail(null)  );
        $sort = self::getContentHighlightDoing();
        // dd($sort);
        return view('doing.manage-doing', compact('doing','fonts','contents','category_contents','sort'));
    }

    public function editDoing(Request $request){
        $validator = $this->validatorEditDoing($request);
        // dd($validator->errors()->messages() , $request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        // $doing->font_id = $request->font_id;
        $doing->save();

        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->orderBy('id')->get();
        // dd($category_contents);
        foreach ($category_contents as $key => &$category_content) {
            $category_content->name_th = $request['category_content_name_th'.(string)($key + 1)] ;
            $category_content->name_eng = $request['category_content_name_eng'.(string)($key + 1)] ;
            $category_content->save();
        }
        session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);
        return redirect('admin/our-work'); 
    }

    public function validatorEditDoing($request){
        $validator = Validator::make(
            $request->all(), [
            // 'font_id' => 'required',
            'category_content_name_th1' => 'required',
            'category_content_name_eng1' => 'required',
            'category_content_name_th2' => 'required',
            'category_content_name_eng2' => 'required',
            'category_content_name_th3' => 'required',
            'category_content_name_eng3' => 'required',
            'category_content_name_th4' => 'required',
            'category_content_name_eng4' => 'required',
            'category_content_name_th5' => 'required',
            'category_content_name_eng5' => 'required',
            'category_content_name_th6' => 'required',
            'category_content_name_eng6' => 'required',

    
        ], [
            // 'font_id.required' => 'กรุณาเลือก font[TO]The font field is required.',
            'category_content_name_th1.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng1.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
            'category_content_name_th2.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng2.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
            'category_content_name_th3.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng3.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
            'category_content_name_th4.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng4.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
            'category_content_name_th5.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng5.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
            'category_content_name_th6.required' => 'กรุณากรอกชื่อประเภทภาษาไทย[TO]The detail ENG field is required.',
            'category_content_name_eng6.required' => 'กรุณากรอกชื่อประเภทภาษาอังกฤษ[TO]The font field is required.',
        ]);
        return $validator;
    }

    public function addDoing(){
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $contents = Content::where('contents.manu_option_id', ManuOption::$doing_id )->get();
        
        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->pluck('name_th','id');

        return view('doing.manage-doing-add', compact('doing','contents','category_contents'));
    }

    public function validatorAddAndEditSaveDoing(Request $request)
    {   
        if($request->status == 'add'){
            $validator = Validator::make(
                $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'location_th' => 'required',
        
                'location_eng' => 'required',
                'work_detail_th' => 'required',
                'work_detail_eng' => 'required',
                // 'budget_th' => 'required',
                // 'budget_eng' => 'required',
                'detail_th' => 'required',
                'detail_eng' => 'required',
                'files' => 'required',
                'files.*' => 'required|mimes:jpeg,png|max:1024',
        
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'location_th.required' => 'กรุณากรอกสถานที่ภาษาไทย[TO]The font field is required.',
        
                'location_eng.required' => 'กรุณากรอกสถานที่ภาษาอังกฤษ[TO]Sub title ENG field is required.',
                'work_detail_th.required' => 'กรุณากรอกลักษณะงานภาษาไทย[TO]Sub title TH field is required.',
                'work_detail_eng.required' => 'กรุณากรอกลักษณะงานภาษาอังกฤษ[TO]Content field is required.',
                // 'budget_th.required' => 'กรุณากรอกวงเงินภาษาไทย[TO]Content field is required.',
                // 'budget_eng.required' => 'กรุณากรอกวงเงินภาษาอังกฤษ[TO]Content field is required.',
                'detail_th.required' => 'กรุณากรอกรายละเอียดภาษาไทย[TO]Sub title TH field is required.',
                'detail_eng.required' => 'กรุณากรอกรายละเอียดภาษาอังกฤษ[TO]Content field is required.',

                'files.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
                'files.*.mimes' => "กรุณาเลือกไฟส์ jpg,png[TO]Content field is required.",
                'files.*.max' => "ไฟล์ขนาดไม่เกิน 1MB (1024 KB).[TO]Content field is required."
        
            ]);
        }else{
            // dd($request->files);
            $validator = Validator::make(
                $request->all(), [
                'name_th' => 'required',
                'name_eng' => 'required',
                'location_th' => 'required',
        
                'location_eng' => 'required',
                'work_detail_th' => 'required',
                'work_detail_eng' => 'required',
                // 'budget_th' => 'required',
                // 'budget_eng' => 'required',
                'detail_th' => 'required',
                'detail_eng' => 'required',
                // 'files' => 'required',
                'files.*' => 'required|mimes:jpeg,png|max:1024',
            ], [
                'name_th.required' => "กรุณากรอกข้อมูลภาษาไทย[TO]The detail TH field is required.",
                'name_eng.required' => 'กรุณากรอกข้อมูลภาษาอังกฤษ[TO]The detail ENG field is required.',
                'location_th.required' => 'กรุณากรอกสถานที่ภาษาไทย[TO]The font field is required.',
        
                'location_eng.required' => 'กรุณากรอกสถานที่ภาษาอังกฤษ[TO]Sub title ENG field is required.',
                'work_detail_th.required' => 'กรุณากรอกลักษณะงานภาษาไทย[TO]Sub title TH field is required.',
                'work_detail_eng.required' => 'กรุณากรอกลักษณะงานภาษาอังกฤษ[TO]Content field is required.',
                // 'budget_th.required' => 'กรุณากรอกวงเงินภาษาไทย[TO]Content field is required.',
                // 'budget_eng.required' => 'กรุณากรอกวงเงินภาษาอังกฤษ[TO]Content field is required.',
                'detail_th.required' => 'กรุณากรอกรายละเอียดภาษาไทย[TO]Sub title TH field is required.',
                'detail_eng.required' => 'กรุณากรอกรายละเอียดภาษาอังกฤษ[TO]Content field is required.',

                // 'files.required' => 'กรุณาเลือกรูป[TO]Content field is required.',
                'files.*.mimes' => "กรุณาเลือกไฟส์ jpg,png[TO]Content field is required.",
                'files.*.max' => "ไฟล์ขนาดไม่เกิน 1MB (1024 KB).[TO]Content field is required."
            ]);
        }
        return $validator;
    }

    public function deleteDoing($content_id){
        Detail::where('content_id',$content_id)->delete();
        Order::where('content_id',$content_id)->delete();
        Image::where('content_id',$content_id)->delete();
        Content::where('id',$content_id)->delete();
        session(['status_manage' => 'ลบข้อมูลสำเร็จ']);
        return redirect('admin/our-work');  
    }

    public function addAndEditSaveDoing( Request $request ){
        // dd($request ,$request->file('files'));
        $validator = $this->validatorAddAndEditSaveDoing($request);
        // dd($validator->errors()->messages());
        if ($validator->errors()->messages()) {
            // dd( array_keys($validator->errors()->messages()) ,$validator->errors()->messages());
            // foreach ( array_keys($validator->errors()->messages()) as $key => $messages) {
            //     # code...
            // }
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if($request->status == 'add'){ $content = new Content(); }else{ $content = Content::where('id',$request->content_id)->first(); }
        $content->name_th = $request->name_th ;
        $content->name_eng = $request->name_eng ;
        $content->category_content_id = $request->category_content_id ;
        $content->manu_option_id = ManuOption::$doing_id ;
        $content->save();

        if($request->status == 'add'){ $detail = new Detail(); }else{ $detail = Detail::where('content_id',$request->content_id)->where('type_detail','detail')->first(); }
        // $detail = new Detail();
        $detail->content_id = $content->id ;
        $detail->type_detail = 'detail' ;
        $detail->detail_th =  $request->detail_th;
        $detail->detail_eng =  $request->detail_eng;
        $detail->save();

        if($request->status == 'add'){ $detail = new Detail(); }else{ $detail = Detail::where('content_id',$request->content_id)->where('type_detail','location')->first(); }
        // $detail = new Detail();
        $detail->content_id = $content->id ;
        $detail->type_detail = 'location' ;
        $detail->detail_th =  $request->location_th;
        $detail->detail_eng =  $request->location_eng;
        $detail->save();

        if($request->status == 'add'){ $detail = new Detail(); }else{ $detail = Detail::where('content_id',$request->content_id)->where('type_detail','work_detail')->first(); }
        // $detail = new Detail();
        $detail->content_id = $content->id ;
        $detail->type_detail = 'work_detail' ;
        $detail->detail_th =  $request->work_detail_th;
        $detail->detail_eng =  $request->work_detail_eng;
        $detail->save();

        // if($request->status == 'add'){ $detail = new Detail(); }else{ $detail = Detail::where('content_id',$request->content_id)->where('type_detail','budget')->first(); }
        // // $detail = new Detail();
        // $detail->content_id = $content->id ;
        // $detail->type_detail = 'budget' ;
        // $detail->detail_th =  $request->budget_th;
        // $detail->detail_eng =  $request->budget_eng;
        // $detail->save();

        if($request->status == 'add'){  //set ท้ายสุด           
            $order = new Order();
            $order->type = 'order_doing' ;
            $order->number = '9999999' ;
            $order->content_id = $content->id ;
            $order->save(); 
        }

        // dd($request ,$request->file('files'));
        unset($request->name_th);
        unset($request->name_eng);
        unset($request->category_content_id);
        unset($request->location_th);
        unset($request->location_eng);
        unset($request->work_detail_th);
        unset($request->work_detail_eng);
        unset($request->category_content_id);
        // unset($request->budget_th);
        // unset($request->budget_eng);
        $request->name_input_files = 'files' ;
        $request->name_file = 'images/doing' ;
        $request->content_id = $content->id ;
        (new HelperController())->filesSave($request);
        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มข้อมูลสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขข้อมูลสำเร็จ']);    
        }
        // return redirect('admin/doing');  
        return redirect('admin/our-work/'.$content->id.'/edit-content-doing') ;

    }

    public function editContentDoing($content_id){
        $doing = ManuOption::where('manu_options.id' , ManuOption::$doing_id )->first();
        $content = Content::where('id',$content_id)->first();

        $category_contents = CategoryContent::where('manu_option_id', ManuOption::$doing_id )->where('name_eng','!=','No category')->pluck('name_th','id');
        return view('doing.manage-doing-edit', compact('doing','content','category_contents','content_id'));
    }

    public function getContentDoingByIdCategoryContent($id_category_content){
        // dd($id_category_content);
        $contents_number_null = Content::where('contents.category_content_id',$id_category_content)
                            // ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            // ->where('orders.type','order_doing')
                            // 
                            ->leftJoin('orders', function($leftJoin)
                            {
                                $leftJoin->on('orders.content_id', '=', 'contents.id');
                                        // ->where('orders.type', '=', )
                                        // ->where('orders.number', '=', null);
                            })
                            ->where('orders.number',null)
                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'orders.type as type' ,
                                'orders.number as number' ,
                                )
                            ->get()
                            ->toarray();
                            // $contents_number_null = [] ;
                        
        $contents_number_not_null = Content::where('contents.category_content_id',$id_category_content)
                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            ->where('orders.type','order_doing')
                            ->whereNotNull('orders.number')

                            ->orderByRaw('LENGTH(orders.number)', 'ASC')
                            ->orderBy('orders.number', 'ASC')
                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'orders.type as type' ,
                                'orders.number as number' ,
                                )
                            ->get()
                            ->toarray();
                            // dd($contents_number_not_null);
        return [$contents_number_null,$contents_number_not_null] ;
    }

    public function getContentHighlightDoing(){
        // dd($id_category_content);
        $contents_highlight_number_null = Content::where('contents.manu_option_id', ManuOption::$doing_id )
                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            // ->where('orders.number',null)
                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'orders.type as type' ,
                                'orders.number as number' ,
                                )
                            ->get()
                            ->toarray();
        $contents_highlight_number_not_null = Content::where('contents.manu_option_id', ManuOption::$doing_id )
                            ->leftJoin('orders', 'orders.content_id', '=', 'contents.id')
                            ->where('orders.type','highlight_doing')
                            ->whereNotNull('orders.number')

                            ->orderByRaw('LENGTH(orders.number)', 'ASC')
                            ->orderBy('orders.number', 'ASC')
                            ->select(
                                'contents.id as id',
                                'contents.name_th as name_th',
                                'orders.type as type' ,
                                'orders.number as number' ,
                                )
                            ->get()
                            ->toarray();

        return [$contents_highlight_number_null,$contents_highlight_number_not_null] ;
    }

    public function setOrder(Request $request){
        // dd($request);
        $contents = Content::where('contents.category_content_id',$request->id_category_content)->pluck('id') ;
        // dd($contents);
        Order::where('type', 'order_doing')->whereIn('content_id', $contents)->delete();
        if( $request->contents != null){
            foreach ($request->contents as $key => $content) {
                $order = new Order();
                $order->type = 'order_doing' ;
                $order->number = $key + 1 ;
                $order->content_id = $content ;
                $order->save();
            }
        }
        session(['status_manage' => 'แก้ไขลำดับข้อมูลสำเร็จ']);
        return redirect('admin/our-work');  
    }

    public function setHighlightOrder(Request $request){
        // dd($request->highlight_contents);
        Order::where('type', 'highlight_doing')->delete();
        if( $request->highlight_contents != null){
            foreach ($request->highlight_contents as $key => $content) {
                $order = new Order();
                $order->type = 'highlight_doing' ;
                $order->number = $key + 1 ;
                $order->content_id = $content ;
                $order->save();
            }
        }
        session(['status_manage' => 'แก้ไขลำดับข้อมูลสำเร็จ']);
        return redirect('admin/our-work');  
    }
}
