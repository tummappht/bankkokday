@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">ผู้ที่ติดต่อ</h4>

		<div class="card">
			<div class="card-body">
				<table class="table table-striped table-with-image w-100 table-responsive" data-order="[[ 0, 'DESC' ]]">
					<thead>
						<th>ชื่อ</th>
						<th>อีเมล์</th>
						<th>เบอร์ติดต่อ</th>
						<th>สถานะ</th>
						<th class="thin-cell"></th>
					</thead>
					<tbody>
						@foreach ($send_email_contents as $send_email_content)
						<tr>
							<td>
								<span class="d-none">{{$send_email_content['id']}}</span>
								{{$send_email_content->getDetailByTypeDetail('name')['detail_all']}}
							</td>
							<td>
								{{$send_email_content->getDetailByTypeDetail('email')['detail_all']}}
							</td>
							<td>
								{{$send_email_content->getDetailByTypeDetail('tel')['detail_all']}}
							</td>
							<td>
								@if( $send_email_content->getDetailByTypeDetail('is_read')['detail_all'] == '0')
									ยังไม่ได้อ่าน
								@else
									อ่านแล้ว
								@endif
							</td>
							<td class="d-flex">
								<a href="{{url('admin/contact/'.$send_email_content->id.'/detail-send-email-content')}}" class="btn btn-light border border-dark mr-2">
									<i class="fa fa-search"></i>
								</a>
								<a  href = "#" onclick="deleteByURL('{{url('admin/admin-home/')}}' , '{{$send_email_content->id}}' , 'delete' );" class="btn btn-outline-danger">
									<i class="far fa-trash-alt"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('.table').DataTable({
			'columnDefs': [ {
				'targets'  : 'no-sort',
				'orderable': false,
			}],
			'bLengthChange' : false,
			'bInfo': false,
			'bPaginate': false,
			'bFilter': false,
		});

	});
</script>
@endsection

