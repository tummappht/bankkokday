@extends('layout.main_admin')
@section('style')  

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">จัดการความเป็นมา</h4>
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>รูปภาพหลัก</label>
							{!! Form::open(['route'=>'admin.introduction.validator-files-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
								{!! Form::hidden('name_file','images/introduction') !!} 
								{!! Form::hidden('name_input_files','files_main') !!}
								{!! Form::hidden('manu_option_id',$introduction->id) !!}
								{!! Form::hidden('is_main','1') !!}
								<div class="file-loading">
										<input id="files_main" name="files_main[]" type="file" multiple>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>รูปภาพรอง</label>
							{!! Form::open(['route'=>'admin.introduction.validator-files-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
								{!! Form::hidden('name_file','images/introduction') !!} 
								{!! Form::hidden('name_input_files','files') !!}
								{!! Form::hidden('manu_option_id',$introduction->id) !!}
								{!! Form::hidden('is_main',0) !!}
								<div class="file-loading">
										<input id="files" name="files[]" type="file" multiple>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				{!! Form::open(['route'=>'admin.introduction.edit', 'method', 'POST', 'class' => 'form' , 'id'  => 'admin_introduction_edit' ]) !!}
					<div class="row">
						{!! Form::hidden('manu_option_id', $introduction->id ) !!}
						<div class="col-sm-6 col-md-12 col-lg-6">
							<div class="form-group">
								<label>หัวข้อ (TH)</label>
								{!! Form::text('detail_th',old('detail_th', $introduction->detail_th),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('detail_th',$errors) !!}
							</div>
						</div>
						<div class="col-sm-6 col-md-12 col-lg-6 mb-3">
							<div class="form-group">
								<label>หัวข้อ (EN)</label>
								{!! Form::text('detail_eng',old('detail_eng', $introduction->detail_eng),['class'=> 'form-control', 'required' ]) !!}
								{!! customValidator('detail_eng',$errors) !!}
							</div>
						</div>
						@foreach ($introduction->contents as $key => $content)
							@if($key < 2)
								{!! Form::hidden('content_id'.($key + 1), $content->id ) !!}
								<div class="col-12 mb-3">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>หัวข้อ (TH)</label>
												{!! Form::text('content_name_th'.($key + 1),old('content_name_th'.($key + 1), $content->name_th),['class'=> 'form-control' ]) !!}
												{!! customValidator('content_name_th'.($key + 1),$errors) !!}
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>หัวข้อ (EN)</label>
												{!! Form::text('content_name_eng'.($key + 1),old('content_name_eng'.($key + 1), $content->name_eng),['class'=> 'form-control' ]) !!}
												{!! customValidator('content_name_eng'.($key + 1),$errors) !!}
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>รายละเอียด (TH)</label>
												{!! Form::textarea('content_detail_th'.($key + 1),old('content_detail_th'.($key + 1), $content->detail->detail_th),['class'=> 'form-control summernote' ]) !!}
												{!! customValidator('content_detail_th'.($key + 1),$errors) !!}
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>รายละเอียด (EN)</label>
												{!! Form::textarea('content_detail_eng'.($key + 1),old('content_detail_eng'.($key + 1), $content->detail->detail_eng),['class'=> 'form-control summernote' ]) !!}
												{!! customValidator('content_detail_eng'.($key + 1),$errors) !!}
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
						<div class="col-12">
							<input type="button" class="btn btn-primary btn-submit" id = 'main_submit' value="บันทึก">
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
    
    <script >
			//  set data
			var introduction = JSON.parse('<?php print_r(json_encode($introduction)); ?>');
			now_manu = introduction.link ;
    </script>
@endsection
@section('script')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
	<script>
		$(document).ready(function() {
			var images =  JSON.parse('<?php print_r(json_encode($introduction->images)); ?>');
				console.log(images);
			var url_is_main_s = [] , urls = [] , preview_config_is_main_s = [] ,preview_configs = [] , m = 0 , n = 0;
			for (i = 0; i < images.length; i++) {
					if(images[i]['is_main'] == '1'){
							url_is_main_s[m] = '{{url("/")}}'+'/'+images[i]['path'] ;
							preview_config_is_main_s[m] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : url_is_main_s[m] , key: images[i]['id'] } ;
							m++;
					}else{
							urls[n] = '{{url("/")}}'+'/'+images[i]['path'] ;
							preview_configs[n] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls[n] , key: images[i]['id'] } ;
							n++;
					}
					// urls[i] = '{{url("/")}}'+'/'+images[i]['path'] ;
					// preview_configs[i] ={ caption: images[i]['path'].split("/").pop() , downloadUrl : urls[i] , key: images[i]['id'] } ;
			}

			$("#files").fileinput({
				maxFileCount: 1,
				validateInitialCount: true,
				initialPreview: urls,
				initialPreviewAsData: true,
				initialPreviewConfig: preview_configs,
				deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
				deleteExtraData: {
						'_token': $('[name="_token"]').val(), // _token
				},
				overwriteInitial: false,
				fileActionSettings: {
						showDrag: false,
				},
    		browseOnZoneClick: true,
				// maxFileSize: 100,
			});

			$("#files_main").fileinput({
				maxFileCount: 1,
				validateInitialCount: true,
				initialPreview: url_is_main_s,
				initialPreviewAsData: true,
				initialPreviewConfig: preview_config_is_main_s,
				deleteUrl: '{{url("/")}}'+'/'+"helper/file-delete",
				deleteExtraData: {
						'_token': $('[name="_token"]').val(), // _token
				},
				overwriteInitial: false,
				fileActionSettings: {
						showDrag: false,
				},
    		browseOnZoneClick: true,
				// maxFileSize: 100,
			});

			$('.summernote').summernote({
				height: 200,
				toolbar: [
					['style', ['style']],
					['font', ['bold', 'italic', 'underline', 'clear']],
					['fontname', ['fontname']],
					['fontsize', ['fontsize']],
					['height', ['height']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture', 'video']],
					['view', ['codeview', 'help']],
				],
				fontNames: ['Sarabun',
					'SILPAKORN70yr',
					'2547_DAYIN01N',
					'2548_D6N',
					'can_Rukdeaw01',
					'kt_smarn seree',
					'PSL245pro',
					'PSL256pro',
					'Pspimpdeed',
					'SILPAKORN70NEW',
					'SILPAKORN70NEW_Sh',
					'SitkaBanner',
					'SitkaHeading',
					'SitkaSubheading',
					'SitkaText',
					'SitkaSmall'],
				fontNamesIgnoreCheck: ['Sarabun',
					'SILPAKORN70yr',
					'2547_DAYIN01N',
					'2548_D6N',
					'can_Rukdeaw01',
					'kt_smarn seree',
					'PSL245pro',
					'PSL256pro',
					'Pspimpdeed',
					'SILPAKORN70NEW',
					'SILPAKORN70NEW_Sh',
					'SitkaBanner',
					'SitkaHeading',
					'SitkaSubheading',
					'SitkaText',
					'SitkaSmall'],
				styleTags: [
					'p',
					'h3',
					'h4',
					'h5',
					'h6',{
						tag : 'p',
						title : 'Font Light',
						style : 'Font Light',
						className : 'Font-Light',
						value : 'p'
					},{
						tag : 'p',
						title : 'Font Medium',
						style : 'Font Medium',
						className : 'Font-Medium',
						value : 'p'
					},{
						tag : 'p',
						title : 'Font Semibold',
						style : 'Font Semibold',
						className : 'Font-Semibold',
						value : 'p'
					}
				],
			});
		});

		popupOK( JSON.parse('{!! $errors !!}') );

		$( "#main_submit" ).click(function() {
			console.log($("#files").val());
			if( $("#files").val() != '' || $("#files_main").val() != '' ){

				Swal.fire({
					title: 'คุณต้องการที่จะบันทึกหรือไม่',
					text: 'โดยที่ยังไม่ได้ "Upload" รูปที่ค้างอยู่ ถ้าต้องการ "Upload" กรุณากดยกเลิก และเลื่อนขึ้นไปกด "Upload" รูปที่เลือก',
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'ยกเลิก',
					confirmButtonText: 'ใช่ , บันทึกเลย!'
				}).then((result) => {
					console.log(result.value)
					if(result.value == true){
						$( "#admin_introduction_edit" ).submit();
					}
					// $( "#admin_introduction_edit" ).submit();
				})

			}else{
				$( "#admin_introduction_edit" ).submit();
			}
			// console.log( $("#files").val() );
		});
	</script>
@endsection
