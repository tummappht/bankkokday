<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;

class ManageAdminController extends Controller
{
    public function manageAdmin(){

        if( \Illuminate\Support\Facades\Auth::user() ==  null){ 
            \Illuminate\Support\Facades\Auth::logout();
            return redirect('login'); 
        }
        
        $users = User::get();
        return view('manage-admin.index', compact('users'));
    }

    public function addAdmin(){
        return view('manage-admin.add-admin');
    }

    public function addAndEditAdminSave(Request $request){
        // dd($request->edit_status_active);
        $validator = $this->validatorAddAndEditAdmin($request);
        if ($validator->errors()->messages()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if( $request->status == 'add' ){
            $user = new User() ;
        } else {
            $user = User::where('id',$request->user_id)->first();
        }
        
        if( $request->status == 'add' ){ 
            $user->email = $request->email ;
            $user->password = Hash::make($request->password) ;
            $user->status_active = $request->status_active ;
        }else{
            $user->status_active = $request->edit_status_active ;
        }

        $user->name = $request->name ;
        // dd($user->all());
        $user->save();

        if($request->status == 'add'){
            session(['status_manage' => 'เพิ่มผู้ดูแลระบบสำเร็จ']);
        }else{
            session(['status_manage' => 'แก้ไขผู้ดูแลระบบสำเร็จ']);
        }

        return redirect('admin/manage-admin'); 
    }

    public function validatorAddAndEditAdmin($request){
        if( $request->status == 'add' ){ 
            $validator = Validator::make(
                $request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'status_active' => 'required',
            ], [
                'name.required' => 'กรุณากรอกชื่อ[TO]The font field is required.',
                'email.required' => 'กรุณากรอก Email[TO]The detail ENG field is required.',
                'email.email' => 'กรุณากรอกให้ถูกต้อง[TO]The detail ENG field is required.',
                'email.unique' => 'มีผู้ใช้ Email แล้ว[TO]The detail ENG field is required.',
                'password.required' => 'กรุณากรอก Password[TO]The font field is required.',
                'status_active.required' => 'กรุณาเลือกสถานะการใช้งาน[TO]The font field is required.',
            ]);
        }else{
            $validator = Validator::make(
                $request->all(), [
                'name' => 'required',
                // 'email' => 'required|email|unique:users',
                'edit_status_active' => 'required',
            ], [
                'name.required' => 'กรุณากรอกชื่อ[TO]The font field is required.',
                // 'email.required' => 'กรุณากรอก Email[TO]The detail ENG field is required.',
                // 'email.email' => 'กรุณากรอกให้ถูกต้อง[TO]The detail ENG field is required.',
                // 'email.unique' => 'มีผู้ใช้ Email แล้ว[TO]The detail ENG field is required.',
                'edit_status_active.required' => 'กรุณาเลือกสถานะการใช้งาน[TO]The font field is required.',
            ]);
        }
        return $validator;
    }

    public function deleteAdmin($user_id){
        if($user_id != 1){
            User::where('id',$user_id)->delete();
            session(['status_manage' => 'ลบผู้ดูแลระบบสำเร็จ']);
        }else{
            session(['status_manage' => 'ไม่สามารถลบผู้ดูแลระบบนี้ได้']);
        }
        
        return redirect('admin/manage-admin'); 
    }

    public function editManageAdmin($user_id){
        $user = User::where('id',$user_id)->first();

        return view('manage-admin.edit-admin', compact('user'));
    }

    public function editPasswordSave(Request $request){
        // dd($request);
        $user = User::where('id',$request->user_id)->first();
        $user->password = Hash::make($request->password) ;
        $user->save();

        session(['status_manage' => 'แก้ไขรหัสผ่านสำเร็จ']);
        
        return redirect('admin/manage-admin'); 
    }

}
