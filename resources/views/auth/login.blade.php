@extends('layout.main_user')

@section('style')  
	<link rel="stylesheet" href="{{ asset('css/admin.css') }}">
@endsection
@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6 my-5 admin-page px-0" style="border-radius: 10px;">
				<div class="card mb-0">
					<div class="card-body">
						<h4 class="header-text text-center mb-5">เข้าสู่ระบบ</h4>
						<form method="POST" action="{{ route('login') }}">
							@csrf
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
								<div class="col-md-6">
										<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
								</div>
							</div>
							<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

									<div class="col-md-6">
											<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

											@error('password')
													<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
													</span>
											@enderror
									</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4 d-flex align-items-baseline justify-content-between">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
										<label class="form-check-label text-12px" for="remember">
											{{ __('Remember Me') }}
										</label>
									</div>
									@if (Route::has('password.request'))
										<a class="btn btn-link text-12px" href="{{ route('password.request') }}">
											{{ __('Forgot Your Password?') }}
										</a>
									@endif
								</div>
							</div>
							<div class="mt-4">
								<button type="submit" class="btn btn-primary btn-submit">
									{{ __('Login') }}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
