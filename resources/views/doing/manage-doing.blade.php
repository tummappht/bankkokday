@extends('layout.main_admin')
@section('style')  

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    
@endsection
@section('content')
	<div class="container-fluid">
		<h4 class="header-text">จัดการมีดีมาโชว์</h4>

		<div class="card">
			<div class="card-body">
				{!! Form::open(['route'=>'admin.doing.set-highlight-order', 'method', 'POST', 'class' => 'form']) !!}
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<div class="d-flex justify-content-between align-items-center mb-3">
								<h5 class="mb-0">จัดการ Highlight</h5>
								<button type="button" class="btn btn-secondary d-flex align-items-center" data-toggle="modal" data-target="#modal_hsort">
									<img src="{{asset('/images/icons/icon-sort.svg')}}" class="mr-2" style="width: 14px;">
									Sort
								</button>
							</div>
							<select class="form-control select2" multiple="multiple" id = 'highlight-content' name = 'highlight_contents[]' style="width: 100%" ></select>
						</div>
						<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				{!! Form::open(['route'=>'admin.doing.edit', 'method', 'POST', 'class' => 'form']) !!}
					<div class="row">
						<div class="col-12">
							<h5>จัดการหมวดหมู่</h5>
						</div>
						@foreach ($category_contents as $key => $category_content)
							@if($category_content->name_eng != 'No category')
							<div class="col-6">
								<div class="form-row">
									<div class="col-md">
										<div class="form-group">
											<label>หมวดหมู่ที่ {{$key }} (TH)</label>
											{!! Form::text('category_content_name_th'.($key),old('category_content_name_th'.($key), $category_content->name_th),['class'=> 'form-control' ]) !!}
											{!! customValidator('category_content_name_th'.($key),$errors) !!}
										</div>
									</div>
									<div class="col-md">
										<div class="form-group">
											<label>หมวดหมู่ที่ {{$key }} (EN)</label>
											{!! Form::text('category_content_name_eng'.($key),old('category_content_name_eng'.($key), $category_content->name_eng),['class'=> 'form-control' ]) !!}
											{!! customValidator('category_content_name_eng'.($key),$errors) !!}
										</div>
									</div>
									<div class="col-lg-auto d-flex align-items-end justify-content-end">
										<div class="form-group">
											<button type="button" class="btn btn-secondary d-flex align-items-center" onclick="getCategory({{$category_content->id}})">
												<img src="{{asset('/images/icons/icon-sort.svg')}}" class="mr-2" style="width: 14px;">
												Sort
											</button>
										</div>
									</div>
								</div>
							</div>
							@endif
						@endforeach
						<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
					</div>
				{!! Form::close() !!}
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-end">
					<a href="{{url('/admin/our-work/add')}}" class="btn btn-secondary d-flex mb-3">
						<img src="{{asset('/images/icons/icon-plus.svg')}}" class="mr-2">
						เพิ่มสิ่งที่ทำ
					</a>
				</div>
				<table class="table table-striped table-with-image w-100 table-responsive" id="table-contents">
					<thead>
						<th class="thin-cell">วันที่</th>
						<th>หัวข้อมีดีมาโชว์</th>
						<th>หมวดหมู่</th>
						<th class="thin-cell no-sort"></th>
					</thead>
					<tbody>
						@foreach ($contents as $content)
							@php
								$category_id = $content['category_content_id'];
							@endphp
							<tr>
								<td>
									<span class="d-none">{{strtotime($content->created_at)}}</span>
									{{date_format($content->created_at,"d/m/Y")}}
								</td>
								<td>
									{{$content->name_th}}
									<br>
									{{$content->name_eng}}
								</td>
								<td>
									{{$category_contents[$category_id]->name_th}}
									<br>
									{{$category_contents[$category_id]->name_eng}}
								</td>
								<td class="d-flex">
									<a href="{{url('admin/our-work/'.$content->id.'/edit-content-doing')}}" class="btn btn-light border border-dark mr-2">
										<i class="far fa-edit"></i>
									</a>
									<a href = "#" onclick="deleteByURL('{{url('admin/our-work/')}}' , '{{$content->id}}' , 'delete' );" class="btn btn-outline-danger">
										<i class="far fa-trash-alt"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal -->
	{{-- <div class="modal" id="modal_nsort">
    <div class="modal-dialog">
      <div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">จัดการลำดับ Highlight</h5>
				</div>
        <div class="modal-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label>บทความที่ลำดับ</label>
									{!! Form::open(['route'=>'admin.doing.set-order', 'method', 'POST', 'class' => 'form']) !!}
										<input type = 'hidden' name = 'id_category_content' >
										<select class="form-control select2" multiple="multiple" id = 'category-content' name = 'contents[]' style="width: 100%" ></select>
										</br></br>
										<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
									{!! Form::close() !!}
								</div>
							</div>
						</div>
					</div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
        </div>
        
      </div>
    </div>
	</div> --}}

	<div class="modal fade" id="modal_nsort" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">จัดการลำดับ</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_sort', 'route'=>'admin.doing.set-order', 'method', 'POST', 'class' => 'form']) !!}
					<input type="hidden" name="id_category_content">
					<div id="sortlist" class="list-group col sortlist"></div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_sort" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="modal_hsort" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="header-text m-0">จัดการลำดับ Highlight</h5>
				</div>
				<div class="modal-body">
					{!! Form::open(['id' => 'form_hsort', 'route'=>'admin.doing.set-highlight-order', 'method', 'POST', 'class' => 'form']) !!}
					<div id="hsortlist" class="list-group col sortlist">
						@php
							$count = count($sort[1]);
							$sort_count = 0;
						@endphp
						@if ($count > 0)
							@foreach ($sort[1] as $list)
								@php
									if($sort_count == 0) {
										$sort_class = 'sort-down';
									} else if($sort_count == ($count - 1)) {
										$sort_class = 'sort-up';
									} else {
										$sort_class = 'sort';
									}
									$sort_count++;
								@endphp
								<div class="list-group-item d-flex align-items-center justify-content-between">
									<p class="m-0">{{$list['name_th']}}</p>
									<i class="fas fa-{{$sort_class}}"></i>
									<input type="hidden" name="highlight_contents[]" value="{{$list['id']}}">
								</div>
							@endforeach
						@else
							<p class="text-center m-0">กรุณาเลือกรายการ Highlight</p>
						@endif
					</div>
					{!! Form::close() !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">ยกเลิก</button>
					<button type="submit" form="form_hsort" class="btn btn-primary">บันทึก</button>
				</div>
			</div>
		</div>
	</div>
	
	<script >
		//  set data
		var doing = JSON.parse('<?php print_r(json_encode($doing)); ?>');
		var contents = JSON.parse('<?php print_r(json_encode($contents)); ?>');
		var category_contents = JSON.parse('<?php print_r(json_encode($category_contents)); ?>');
		const categoryname = new Map();
		for (const i of Object.keys(category_contents)) {
			let el = category_contents[i];
			categoryname.set(el.id+'_th', el.name_th)
			categoryname.set(el.id+'_eng', el.name_eng)
		}
		now_manu = doing.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
<script>
	$(document).ready(function() {
		$('.sortlist').sortable({
    	animation: 150,
			onChange: e => {
				updateSort($(e.srcElement));
			}
		});

		$('#table-contents').DataTable({
			'order': [0, 'ASC'],
			'columnDefs': [ {
				'targets'  : 'no-sort',
				'orderable': false,
			}],
			'language': {
				'paginate': {
					'previous': '<',
					'next': '>'
				}
			}
		});

		// $('.select2').select2({width: 'resolve'});

		$('#highlight-content').select2({
			width: 'resolve',
			maximumSelectionLength: 8,
		});
		
		setHighligh();
	});
	
	function setHighligh(){
		axios.get("{{url('admin/our-work')}}"+'/get-content-highlight-doing')
		.then(function (response) {
			var contents_number_null = response.data[0];
			var contents_number = response.data[1];
			var is_not_null = 0 , option ;
			$('#highlight-content').find('option').remove().end();
			for (let index = 0; index < contents_number_null.length; index++) {
				option = new Option(contents_number_null[index].name_th, contents_number_null[index].id, true, false);	// ยังไม่ถูกเลือก
    			$('#highlight-content').append(option).trigger('change');
			}

			for (let index = 0; index < contents_number.length; index++) {
				option = new Option(contents_number[index].name_th, contents_number[index].id, false, true);	// ถูกเลือกแล้ว
				$('#highlight-content').append(option).trigger('change');
			}
		})
	}

	function getCategory(id_category_content){
		let modal = $('#modal_nsort');
		let header_text = modal.find('.header-text').text(`จัดการลำดับ ${categoryname.get(id_category_content+'_th')}(${categoryname.get(id_category_content+'_eng')})`)
		axios.get("{{url('admin/our-work')}}/"+id_category_content+'/get-content-doing-by-id-category-content')
		.then(function (response) {
			var contents_number_null = response.data[0];
			var contents_number = response.data[1];
			
			let count = contents_number.length + contents_number_null.length;
			let sort_count = 0;
			let html = '';
			for (let i = 1; i >= 0; i--) {
				const data = response.data[i];
				for (const el of data) {
					if(sort_count == 0) {
						sort_class = 'sort-down';
					} else if(sort_count == (count - 1)) {
						sort_class = 'sort-up';
					} else {
						sort_class = 'sort';
					}
					sort_count++;
					html += `
					<div class="list-group-item d-flex align-items-center justify-content-between">
						<p class="m-0">${el.name_th}</p>
						<i class="fas fa-${sort_class}"></i>
						<input type="hidden" name="contents[]" value="${el.id}">
					</div>
					`;
				}
			}
			modal.find('input[name="id_category_content"]').val(id_category_content)
			modal.find('.sortlist').html(html)
			modal.modal()
		})
	}

	$('.select2').on('select2:select', function (evt) {
		var element = evt.params.data.element;
		var $element = $(element);
		
		$element.detach();
		$(this).append($element);
		$(this).trigger("change");
	});

	function updateSort($this) {
		let i = 0;
		let fas = $this.find('.fas');
		let count = fas.length;
		fas.each(function() {
			let parent = $(this).parents('.list-group-item');
			$(this).removeClass('fa-sort-down');
			$(this).removeClass('fa-sort-up');
			$(this).removeClass('fa-sort');
			if(i == 0) {
				$(this).addClass('fa-sort-down');
			} else if (i == (count - 1)) {
				$(this).addClass('fa-sort-up');
			}
			$(this).addClass('fa-sort');
			i++;
		});
	}
</script>
@endsection

