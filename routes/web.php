<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HeaderController@index')->name('index');
Route::get('/background','IntroductionController@index')->name('introduction');
Route::get('/our-work','DoingController@index')->name('doing');
Route::get('/our-work-filter/{filter_category_content_id}','DoingController@doingFilter')->name('doing-filter');
Route::get('/our-work/detail/{content_id}','DoingController@detail')->name('doing.detail');
Route::get('/testimonials','CommentController@index')->name('comment');
Route::get('/additional-services','AdditionalServiceController@index')->name('additional-service');
Route::get('/additional-service/detail/{content_id}','AdditionalServiceController@detail')->name('additional-service.detail');
Route::get('/contact','ContactController@index')->name('contact');
Route::post('/contact/send-email','ContactController@sendEmail')->name('contact.send-email');
//['middleware'=>'auth'] , 
Route::name('admin.')->prefix('admin')->group(function () { 
    // Route::get('/','HeaderController@manageHeader')->name('manage-header');
    Route::name('header.')->prefix('header')->group(function () { 
        Route::get('/','HeaderController@manageHeader')->name('manage-header');
        Route::post('/edit','HeaderController@editHeader')->name('edit');
    });

    Route::name('introduction.')->prefix('background')->group(function () { 
        Route::get('/','IntroductionController@manageIntroduction')->name('manage-introduction');
        Route::post('/edit','IntroductionController@editIntroduction')->name('edit');
        Route::post('/validator-files-save','IntroductionController@validatorFilesSave')->name('validator-files-save');
    });

    Route::name('doing.')->prefix('our-work')->group(function () { 
        Route::get('/','DoingController@manageDoing')->name('manage-doing');
        Route::get('/add','DoingController@addDoing')->name('add');
        Route::post('/edit','DoingController@editDoing')->name('edit');
        Route::post('/add-and-edit-save','DoingController@addAndEditSaveDoing')->name('add-and-edit-save');
        Route::get('{content_id}/delete','DoingController@deleteDoing')->name('delete');
        Route::get('{content_id}/edit-content-doing','DoingController@editContentDoing')->name('edit-content-doing');

        Route::get('{id_category_content}/get-content-doing-by-id-category-content','DoingController@getContentDoingByIdCategoryContent')->name('get-content-doing-by-id-category-content');
        Route::post('/set-order','DoingController@setOrder')->name('set-order');

        Route::get('get-content-highlight-doing','DoingController@getContentHighlightDoing')->name('get-content-highlight-doing');
        Route::post('/set-highlight-order','DoingController@setHighlightOrder')->name('set-highlight-order');
    });
    Route::name('comment.')->prefix('testimonials')->group(function () { 
        Route::get('/','CommentController@manageComment')->name('manage-comment');
        Route::post('/edit-comment','CommentController@editComment')->name('edit-comment');

        Route::get('/add-customer-content','CommentController@addContentCustomer')->name('add-content');
        Route::get('{content_id}/edit-customer-content','CommentController@editContentCustomer')->name('edit-customer-content');
        Route::post('/add-and-edit-save-customer-content','CommentController@addAndEditSaveContentCustomer')->name('add-and-edit-save-customer-content');

        Route::get('/add-youtube-content','CommentController@addContentYoutube')->name('add-youtube');
        Route::get('{content_id}/edit-youtube-content','CommentController@editContentYoutube')->name('edit-youtube-content');
        Route::post('/add-and-edit-save-youtube-content','CommentController@addAndEditSaveContentYoutube')->name('add-and-edit-save-youtube-content');

        Route::get('{content_id}/delete','CommentController@deleteContent')->name('delete');


        Route::get('get-content-youtube-comment-order','CommentController@getContentYoutubeCommentOrder')->name('get-content-youtube-comment-order');
        Route::post('/set-youtube-order','CommentController@setYoutubeOrder')->name('set-youtube-order');
        Route::post('/set-comment-order','CommentController@setCommentOrder')->name('set-comment-order');
        
    });

    Route::name('additional-service.')->prefix('additional-services')->group(function () { 
        Route::get('/','AdditionalServiceController@manageAdditionalService')->name('manage-additional-service');
        Route::post('/edit-additional-service','AdditionalServiceController@editAdditionalService')->name('edit-additional-service');
        Route::get('/add-content','AdditionalServiceController@addContent')->name('add-content');
        Route::get('{content_id}/edit-content','AdditionalServiceController@editContent')->name('edit-content');
        Route::post('/add-and-edit-save','AdditionalServiceController@addAndEditSaveContent')->name('add-and-edit-save');
        Route::get('{content_id}/delete','AdditionalServiceController@deleteContent')->name('delete');
    });

    Route::name('contact.')->prefix('contact')->group(function () { 
        Route::get('/','ContactController@manageContact')->name('manage-contact');
        Route::post('/edit','ContactController@editContact')->name('edit');

        Route::get('/add-content-email','ContactController@addContentEmail')->name('add-content-email');
        Route::get('{content_id}/edit-email-content','ContactController@editEmailContent')->name('edit-email-content');
        Route::post('/add-and-edit-email-save','ContactController@addAndEditEmailSaveContent')->name('add-and-edit-email-save');
        Route::get('{content_id}/delete','ContactController@deleteContent')->name('delete');

        Route::get('/add-content-qr','ContactController@addContentQR')->name('add-content-qr');
        Route::get('{content_id}/edit-qr-content','ContactController@editContentQR')->name('edit-qr-content');
        Route::post('/add-and-edit-qr-save','ContactController@addAndEditQRSaveContent')->name('add-and-edit-qr-save');

        Route::get('{content_id}/detail-send-email-content','ContactController@detailSendEmailContent')->name('detail-send-email-content');
    });
    
    Route::name('manage-admin.')->prefix('manage-admin')->group(function () { 
        Route::get('/','ManageAdminController@manageAdmin')->name('manage-admin');
        Route::get('/add-admin','ManageAdminController@addAdmin')->name('add-admin');
        Route::post('/add-and-edit-admin-save','ManageAdminController@addAndEditAdminSave')->name('add-and-edit-admin-save');
        Route::get('{user_id}/delete','ManageAdminController@deleteAdmin')->name('delete-admin');
        Route::get('{user_id}/edit-manage-admin','ManageAdminController@editManageAdmin')->name('edit-manage-admin');
        Route::post('/edit-password-save','ManageAdminController@editPasswordSave')->name('edit-password-save');
    });

    Route::get('/','AdminHomeController@adminHome')->name('admin-home');
    
    Route::name('admin-home.')->prefix('admin-home')->group(function () {
        Route::get('/','AdminHomeController@adminHome')->name('admin-home');
        Route::get('{content_id}/delete','AdminHomeController@deleteContent')->name('delete');
    });
});
Route::get('/home','AdminHomeController@adminHome')->name('admin-home');
Route::name('helper.')->prefix('helper')->group(function () { 
    Route::post('/file-delete','HelperController@fileDelete')->name('file-delete');
    Route::post('/files-save','HelperController@filesSave')->name('files-save');
});
// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes(['register' => false , 'home' => false]);

// Route::get('/home', 'HomeController@index')->name('home');
