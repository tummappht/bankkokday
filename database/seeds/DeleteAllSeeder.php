<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem ;

class DeleteAllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('details')->delete();
        DB::table('images')->delete();
        DB::table('contents')->delete();
        DB::table('category_contents')->delete();
        DB::table('manu_options')->delete();
        DB::table('fonts')->delete();
        DB::table('users')->delete();
        File::deleteDirectory(public_path('images/header'));
        File::deleteDirectory(public_path('images/introduction'));
        File::deleteDirectory(public_path('images/additional_service'));
    }
}
