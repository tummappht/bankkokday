<?php

use Illuminate\Database\Seeder;
use App\ManuOption ;
use Illuminate\Support\Facades\Storage;

class AddFixData_headerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [ 'id'=>3, 'name_th'=>'จุดเริ่มต้น' , 'name_eng'=>'start' , 'manu_option_id' => ManuOption::$header_id ],
            [ 'id'=>4, 'name_th'=>'แนวคิดที่เรายึดมั่นมาตลอด 20 ปี' , 'name_eng'=>'The legend that we have been adhering to for 20 years.' , 'manu_option_id' => ManuOption::$header_id ],
            [ 'id'=>5, 'name_th'=>'งานที่ดี' , 'name_eng'=>'Good job.' , 'manu_option_id' => ManuOption::$header_id ],
        ]);

        $detail_th_1 = '<p class="m-0">เริ่มต้นจากบริษัทออกแบบที่มีพนักงานเพียง 2 คน แต่ปัจจุบันเราย้ายสำนักงานใหญ่มาที่อาคาร "บางกอกเดย์ราม่า" บนถนนนครอินทร์</p>' ;
        $detail_th_2 = '<ol class="pl-3 mb-0">
                            <li>มุ่งมั่น และตั้งใจ</li>
                            <li>เน้นการออกแบบที่มี STORY & CONCEPT</li>
                            <li>เน้นงานรับเหมาที่มีคุณภาพและราคาที่เหมาะสม</li>
                            <li>ตรงต่อเวลา</li>
                        </ol>' ;
        $detail_th_3 = '<p class="m-0">งานที่ดีไม่ได้มาจากผู้ออกแบบและงานก่อสร้างที่ดีเท่านั้น แต่มาจากเจ้าของงานที่เข้าใจในงาน ด้วยความมุ่งมั่นของเรา และประสบการณ์กว่า 20 ปี ขอให้เราได้เป็นส่วนหนึ่งในความสำเร็จของคุณ</p>' ;

        DB::table('details')->insert([
            [ 'id'=>3, 'detail_th'=> $detail_th_1 , 'detail_eng'=>'text eng 1' , 'content_id' => '3' , 'type_detail' => 'footer'],
            [ 'id'=>4, 'detail_th'=> $detail_th_2 , 'detail_eng'=>'text eng 2' , 'content_id' => '4' , 'type_detail' => 'footer'],
            [ 'id'=>5, 'detail_th'=> $detail_th_3 , 'detail_eng'=>'text eng 3' , 'content_id' => '5' , 'type_detail' => 'footer'],
        ]);
        DB::table('images')->insert([
            [ 'id'=>1, 'path'=>'images/header/header_main.jpg', 'is_main'=>'1' , 'manu_option_id' => '1' ]
        ]);
        // copy fix_images
        $move = Storage::disk('local_public')->copy('fix_images/header_main.jpg','images/header/header_main.jpg');
    }
}
