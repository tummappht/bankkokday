<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->text('type_detail')->nullable()->comment('ประเภท detail');
            $table->text('detail_th')->nullable()->comment('ข้อมูลไทย');
            $table->text('detail_eng')->nullable()->comment('ข้อมูล eng');
            $table->text('detail_all')->nullable()->comment('ข้อมูล ที่เป็นแบบเดียว');

            $table->unsignedBigInteger('content_id')->nullable()->comment('ข้อมูลของ content');
            $table->foreign('content_id')->references('id')->on('contents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
