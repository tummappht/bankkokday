@extends('layout.main_admin')
@section('style')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">จัดการหน้าแรก</h4>
		<div class="card">
			<div class="card-body">
				<div class="form-group">
					<label>Hero Image</label>
					{!! Form::open(['route'=>'helper.files-save', 'method', 'POST', 'class' => 'form' , 'enctype' =>
					"multipart/form-data"]) !!}
						{!! Form::hidden('name_file','images/header') !!}
						{!! Form::hidden('name_input_files','files') !!}
						{!! Form::hidden('manu_option_id','1') !!}
						<div class="file-loading">
							<input id="files" name="files[]" type="file" multiple>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				{!! Form::open(['route'=>'admin.header.edit', 'method', 'POST', 'class' => 'form']) !!}
					<div class="row">
						{!! Form::hidden('manu_option_id', $header->id ) !!}
						<div class="col-sm-6">
							<div class="form-group">
								<label>หัวข้อ (TH)</label>
								{!! Form::text('detail_th',old('detail_th', $header->detail_th),['class'=> 'form-control' ]) !!}
								{!! customValidator('detail_th',$errors) !!}
							</div>
						</div>
						<div class="col-sm-6 mb-3">
							<div class="form-group">
								<label>หัวข้อ (EN)</label>
								{!! Form::text('detail_eng',old('detail_eng', $header->detail_eng),['class'=> 'form-control' ]) !!}
								{!! customValidator('detail_eng',$errors) !!}
							</div>
						</div>
						
						@foreach ($header->contents as $key => $content)
							{!! Form::hidden('content_id'.($key + 1), $content->id ) !!}
							<!-- <div class="col-12">
								<h5>ย่อหน้าที่ {{$key + 1}}</h5>
							</div> -->
							<div class="col-12 mb-3">
								<div class="row">
									<!-- <div class="col-lg-6">
										<div class="form-group">
											<label>หัวข้อ (TH)</label>
											{!! Form::text('content_name_th'.($key + 1),old('content_name_th'.($key + 1), $content->name_th),['class'=> 'form-control' ]) !!}
											{!! customValidator('content_name_th'.($key + 1),$errors) !!}
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>หัวข้อ (EN)</label>
											{!! Form::text('content_name_eng'.($key + 1),old('content_name_eng'.($key + 1), $content->name_eng),['class'=> 'form-control' ]) !!}
											{!! customValidator('content_name_eng'.($key + 1),$errors) !!}
										</div>
									</div> -->
									<div class="col-lg-6">
										<div class="form-group">
											<label>รายละเอียด (TH)</label>
											{!! Form::textarea('content_detail_th'.($key + 1),old('content_detail_th'.($key + 1), $content->detail->detail_th),['class'=> 'form-control summernote' ]) !!}
											{!! customValidator('content_detail_th'.($key + 1),$errors) !!}
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>รายละเอียด (EN)</label>
											{!! Form::textarea('content_detail_eng'.($key + 1),old('content_detail_eng'.($key + 1), $content->detail->detail_eng),['class'=> 'form-control summernote' ]) !!}
											{!! customValidator('content_detail_eng'.($key + 1),$errors) !!}
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="col-12">
							<input type="submit" class="btn btn-primary btn-submit" value="บันทึก">
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<script>
		//  set data
		var header = JSON.parse('<?php print_r(json_encode($header)); ?>');
		now_manu = header.link;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
	$(document).ready(function () {
		var images = JSON.parse('<?php print_r(json_encode($header->images)); ?>');

		var urls = [],
				preview_configs = [];
		for (i = 0; i < images.length; i++) {
			urls[i] = '{{url("/")}}' + '/' + images[i]['path'];
			preview_configs[i] = {
				caption: images[i]['path'].split("/").pop(),
				downloadUrl: urls[i],
				key: images[i]['id']
			};
		}

		$("#files").fileinput({
			initialPreview: urls,
			initialPreviewAsData: true,
			initialPreviewConfig: preview_configs,
			deleteUrl: '{{url("/")}}' + '/' + "helper/file-delete",
			deleteExtraData: {
					'_token': $('[name="_token"]').val(), // _token
			},
			overwriteInitial: false,
			fileActionSettings: {
					showDrag: false,
			},
			browseOnZoneClick: true,
			// maxFileSize: 100,
		});

		$('.summernote').summernote({
			height: 200,
			toolbar: [
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['fontsize', ['fontsize']],
				['height', ['height']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['codeview', 'help']],
			],
			fontNames: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
			fontNamesIgnoreCheck: ['Sarabun',
				'SILPAKORN70yr',
				'2547_DAYIN01N',
				'2548_D6N',
				'can_Rukdeaw01',
				'kt_smarn seree',
				'PSL245pro',
				'PSL256pro',
				'Pspimpdeed',
				'SILPAKORN70NEW',
				'SILPAKORN70NEW_Sh',
				'SitkaBanner',
				'SitkaHeading',
				'SitkaSubheading',
				'SitkaText',
				'SitkaSmall'],
 			styleTags: [
				'p',
				'h3',
				'h4',
				'h5',
				'h6',{
					tag : 'p',
					title : 'Font Light',
					style : 'Font Light',
					className : 'Font-Light',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Medium',
					style : 'Font Medium',
					className : 'Font-Medium',
					value : 'p'
				},{
					tag : 'p',
					title : 'Font Semibold',
					style : 'Font Semibold',
					className : 'Font-Semibold',
					value : 'p'
				}
			],
		});
	});

</script>
@endsection
