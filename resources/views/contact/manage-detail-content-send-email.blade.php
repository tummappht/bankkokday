@extends('layout.main_admin')
@section('style')  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		<h4 class="header-text">แบบฟอร์มติดต่อ</h4>
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12 person-and-no-person" >
						<div class="form-group">
							<label class="m-0">ชื่อ</label>
							<p>{!! $content->getDetailByTypeDetail('name')['detail_all'] !!}</p>
						</div>
					</div>
					<div class="col-lg-12 person-and-no-person" >
						<div class="form-group">
							<label class="m-0">อีเมล์</label>
							<p>{!! $content->getDetailByTypeDetail('email')['detail_all'] !!}</p>
						</div>
					</div>
					<div class="col-lg-12 person-and-no-person" >
						<div class="form-group">
							<label class="m-0">เบอร์ติดต่อ</label>
							<p>{!! $content->getDetailByTypeDetail('tel')['detail_all'] !!}</p>
						</div>
					</div>
					<div class="col-lg-12 person-and-no-person" >
						<div class="form-group">
							<label class="m-0">รายละเอียด</label>
							<p>{!! $content->getDetailByTypeDetail('detail')['detail_all'] !!}</p>
						</div>
					</div>
				<div>
			<div>
		<div>
	</div>

@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

@endsection

