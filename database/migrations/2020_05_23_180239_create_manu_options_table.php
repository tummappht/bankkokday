<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManuOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manu_options', function (Blueprint $table) {
            $table->id();
            $table->string('name_th')->nullable()->comment('ชื่อ เมนู th');
            $table->string('name_eng')->nullable()->comment('ชื่อ เมนู eng');
            $table->text('detail_th')->nullable()->comment('ข้อมูล เมนู th');
            $table->text('detail_eng')->nullable()->comment('ข้อมูล เมนู eng');
            $table->string('number_order')->nullable()->comment('ลำดับเมนู');
            $table->string('is_show')->nullable()->comment('โชบน บา');

            $table->unsignedBigInteger('font_id')->nullable()->comment('font ที่ใช้');
            $table->foreign('font_id')->references('id')->on('fonts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manu_options');
    }
}
