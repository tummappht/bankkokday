<?php

use Illuminate\Database\Seeder;

class FontsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('fonts')->delete();
        DB::table('fonts')->insert([
            [ 'id'=>1, 'name'=>'standard' , 'detail'=>'test' ],
        ]);
    }
}
