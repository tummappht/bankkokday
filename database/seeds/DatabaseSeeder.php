<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeleteAllSeeder::class);
        $this->call(FontsTableSeeder::class);
        $this->call(ManuOptionsTableSeeder::class);
        $this->call(CategoryContentSeeder::class);
        $this->call(AddFixDataIntroductionSeeder::class);
        $this->call(AddFixData_headerSeeder::class);
        $this->call(AddFixData_ContactSeeder::class);
        $this->call(AddFixData_UserSeeder::class);
    }
}