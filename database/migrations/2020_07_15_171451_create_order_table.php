<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->comment('ประเภทลำดับ');
            $table->string('number')->nullable()->comment('ลำดับ');

            $table->unsignedBigInteger('content_id')->nullable()->comment('ข้อมูลของ content');
            $table->foreign('content_id')->references('id')->on('contents');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
