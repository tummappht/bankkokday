

if( sessionStorage.getItem("language") == null ){
    sessionStorage.setItem("language", "th"); // set thai
    setTextByLanguage( sessionStorage.getItem("language") );
}else{
    setTextByLanguage( sessionStorage.getItem("language") );
}

// $( ".bt_language_th" ).click(function() {
//     sessionStorage.setItem("language", "th"); // set thai
//     setTextByLanguage(sessionStorage.getItem("language"));
// });

// $( ".bt_language_eng" ).click(function() {
//     sessionStorage.setItem("language", "eng"); // set en
//     setTextByLanguage(sessionStorage.getItem("language"));
// });

function setTextByLanguage(language){   

    setNavbarAdmin('th');// fix this
    setSidebarAdmin('th');// fix this
    setTextError('th');// fix this

        // if( now_manu == 'header' ){ setHeaderUser(); }
    
}
// {{\Request::is(["promote"])?"active":""}}
// #827E73

function setSidebarAdmin(language) {
    if (sidebar_manu_option != null) {
    let lang = sessionStorage.getItem('language');
    let a = '';

    a +='<li>'
    a +=    `<a href="${url}/admin/admin-home">หน้าหลัก</a>`;    //หน้าหลัก
    a +='</li>'

    for (i = 0; i < sidebar_manu_option.length; i++) {
        a += '<li>'
        a += `<a href="${url}/admin/${sidebar_manu_option[i]['link']}">จัดการ ${sidebar_manu_option[i]['name_th']}</a>`;
        a += '</li>'
    }

    a +='<li>'
    a +=    `<a href="${url}/admin/manage-admin">จัดการผู้ดูแลระบบ</a>`;    //จัดการผู้ดูแลระบบ
    a +='</li>'
    
    $('#dav_manu_option').html(a);
}
}

function setNavbarAdmin(language) {
    $('.bt_language_th').text('ไทย')
    $('.bt_language_eng').text('English')
    if(language == 'th'){ //th
        $('#bt_logout').text('ออกจากระบบ')

    }else{  //en
        $('#bt_logout').text('Logout')
    }
}

function setTextError(language){

    if(language == 'th'){ //th
        $('.text-error-th').show();
        $('.text-error-eng').hide();
    }else{  //en
        $('.text-error-th').hide();
        $('.text-error-eng').show();
    }
}

