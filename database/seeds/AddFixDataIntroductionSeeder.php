<?php

use Illuminate\Database\Seeder;
use App\ManuOption ;
use Illuminate\Support\Facades\Storage;

class AddFixDataIntroductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [ 'id'=>1, 'name_th'=>'เสน่ห์ของอาชีพนี้ อยู่ตรงไหน?' , 'name_eng'=>'Introduction_contents_name_eng_1' , 'manu_option_id' => ManuOption::$introduction_id ],
            [ 'id'=>2, 'name_th'=>'ผมจะบอกลูกค้าเสมอว่า ...' , 'name_eng'=>'Introduction_contents_name_eng_2' , 'manu_option_id' => ManuOption::$introduction_id ],
        ]);

        $detail_id_1_th = '<div><i>อืม...จริงๆอยากจะบอกว่าอาชีพนี้เหนื่อยมากนะครับ</i></div><div><i>เคยได้ยินกันมาบ้างมั้ยว่า...สร้างบ้าน 1หลัง ปวดหัวขนาดไหน&nbsp;</i></div><div><i>แล้วนี่ปีหนึ่งทำตั้งกี่หลัง กี่งาน ลองคิดดู ว่าจะวุ่นวายขนาดไหน</i></div><div><i>ปัญหาในระหว่างการทำงาน เยอะขนาดไหน ไม่ต้องถามเยอะมาก 555&nbsp;</i></div><div><i>แต่ตอนงานเสร็จ...มันจะมีความสุขครับ ได้ทำความฝันของลูกค้าจากกระดาษ</i></div><div><i>เป็นของจริง แล้วเขามีความสุขกับสิ่งที่เราทำ</i></div>';
        $detail_id_2_th = '<div><i>การทำบ้านจากจุดเริ่มต้น ไปถึงจุดที่บ้านเสร็จ มันจะมีปัญหาระหว่างทางตลอดครับ&nbsp;</i></div><div><i>แต่เรามาช่วยกันสร้าง ช่วยกันแก้ปัญหาและทำให้งานออกมาสวยงามกันเถอะครับ</i></div><div><i>เราจะอยู่ด้วยกันจนงานเสร็จ และออกมาสวยงามครับ</i></div><div><i><br></i></div><div><i>ไม่มีของถูกและดี เพราะอย่างนี้ของดีและแพง จึงขายได้เสมอ</i></div><div><i><br></i></div><div><i>ราคาของผมไม่ได้ถูกที่สุด แต่ก็ไม่ได้แพง ถ้าจะเอาของถูกที่ถูกที่สุด ผมทำให้ไม่ได้</i><br></div>';
        DB::table('details')->insert([
            [ 'id'=>1, 'detail_th'=>$detail_id_1_th , 'detail_eng'=>'Introduction_contents_details_eng_1' , 'content_id' => '1' , 'type_detail' => 'content1'],
            [ 'id'=>2, 'detail_th'=>$detail_id_2_th , 'detail_eng'=>'Introduction_contents_details_eng_2' , 'content_id' => '2' , 'type_detail' => 'content2'],
        ]);

        DB::table('images')->insert([
            [ 'id'=>2, 'path'=>'images/introduction/introduction_main.jpg', 'is_main'=>'1' , 'manu_option_id' => '2' ],
            [ 'id'=>3, 'path'=>'images/introduction/introduction.jpg', 'is_main'=>'0' , 'manu_option_id' => '2' ]
        ]);
        // copy fix_images
        $move = Storage::disk('local_public')->copy('fix_images/introduction_main.jpg','images/introduction/introduction_main.jpg');
        $move = Storage::disk('local_public')->copy('fix_images/introduction.jpg','images/introduction/introduction.jpg');
    }
}
