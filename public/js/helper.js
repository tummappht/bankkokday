function popupOK(data_json) {
  console.log(data_json, sessionStorage.getItem("language"));

  if (data_json.length != 0) {
    Swal.fire({
      icon: data_json['icon'][0],
      title: data_json['title_' + sessionStorage.getItem("language")][0],
      text: data_json['text_' + sessionStorage.getItem("language")][0],
    })
  }
}

function deleteByURL(prefix_url, canter_url, end_url) {

  Swal.fire({
    title: 'คุณต้องการลบ ใช่หรือไม่?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'ยกเลิก',
    confirmButtonText: 'ใช่ , ลบเลย!'
  }).then((result) => {
    // console.log(result);
    if (result.value == true) {
      window.location.href = prefix_url + '/' + canter_url + '/' + end_url;
    }

  })
}

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
class notification {
  constructor(type, text) {
    this.type = type
    this.text = text
  }
  showNoti() {
    switch (this.type) {
      case 'success':
				toastr.success(this.text);
        break;
      case 'error':
				toastr.error(this.text)
        break;
    }
  }
}

function submitNotification(type, text) {
  
  if(text.includes('[TO]')){
    var temp = text.split("[TO]");
    if(sessionStorage.getItem("language") == 'th'){
       text = temp[0];
     
    }else{
       text = temp[1];
    } 

  }

  const noti = new notification(type, text);
  noti.showNoti();
  
	
	// ข้อมูลจัดส่งหาเจ้าหน้าที่เรียบร้อยแล้ว กรุณารอการติดต่อจากเรา
	// Thank you for your enquiry. We will get back to you as soon as possible.
}