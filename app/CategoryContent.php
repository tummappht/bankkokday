<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class CategoryContent extends Model
{
    public function getContentsAttribute()
    {
        return Content::where('category_content_id' , $this->id)->get();
    }
}
