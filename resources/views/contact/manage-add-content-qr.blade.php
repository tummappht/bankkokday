@extends('layout.main_admin')
@section('style')  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css">
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')

	<div class="container-fluid">
		{!! Form::open(['route'=>'admin.contact.add-and-edit-qr-save', 'method', 'POST', 'class' => 'form' , 'enctype' => "multipart/form-data"]) !!}
			{!! Form::hidden('status','add') !!}
			<h4 class="header-text">เพิ่ม QRcode</h4>
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>ประเภท QRcode</label>
								<?php $type_select = [ null => 'กรุณาเลือกประเภท' ,'คน' => 'บุคคล' , 'เฟสบุ๊ค' => 'เฟซบุ๊ก' , 'ไอจี' => 'อินสตาแกรม' , 'ทวิตเตอร์' => 'ทวิตเตอร์' ] ; ?>
								{!! Form::select('type',$type_select,old('type'),['class'=> 'form-control type' ]) !!}
								{!! customValidator('type',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6 person-and-no-person" style="display: none;">
							<div class="form-group">
								<label>ชื่อ (TH)</label>
								{!! Form::text('name_th',old('name_th'),['class'=> 'form-control' ]) !!}
								{!! customValidator('name_th',$errors) !!}
							</div>
						</div>
						<div class="col-lg-6 person-and-no-person" style="display: none;">
							<div class="form-group">
								<label>ชื่อ (EN)</label>
								{!! Form::text('name_eng',old('name_eng'),['class'=> 'form-control' ]) !!}
								{!! customValidator('name_eng',$errors) !!}
							</div>
						</div>
						<div class="col-md-6 col-lg-4 person" style="display: none;">
							<div class="form-group">
								<label>Line ID</label>
								{!! Form::text('line',old('line'),['class'=> 'form-control' ]) !!}
								{!! customValidator('line',$errors) !!}
							</div>
						</div> 
						<div class="col-md-6 col-lg-4 person" style="display: none;">
							<div class="form-group">
								<label>เบอร์โทรศัพท์</label>
								{!! Form::text('tel',old('tel'),['class'=> 'form-control' ]) !!}
								{!! customValidator('tel',$errors) !!}
							</div>
						</div>
						<div class="col-md-6 col-lg-4 person" style="display: none;">
							<div class="form-group">
								<label>อีเมล</label>
								{!! Form::text('email',old('email'),['class'=> 'form-control' ]) !!}
								{!! customValidator('email',$errors) !!}
							</div>
						</div>
						<div class="col-lg-12 no-person" style="display: none;">
							<div class="form-group">
								<label>Link</label>
								{!! Form::text('link',old('link'),['class'=> 'form-control' ]) !!}
								{!! customValidator('link',$errors) !!}
							</div>
						</div>
						<div class="col-lg-12 person-and-no-person" style="display: none;">
							<div class="form-group">
								<label>QRcode</label>
								<div class="file-loading">
									<input name="files[]" type="file" class = "files" multiple>
								</div>
								{!! customValidator('files',$errors) !!}
							</div>
							<input type="submit" class="btn btn-primary btn-submit mt-3" value="บันทึก">
						</div>
					<div>
				<div>
			<div>
		{!! Form::close() !!}

	</div>

	<script >
		//  set data
		var contact = JSON.parse('<?php print_r(json_encode($contact)); ?>');
		now_manu = contact.link ;
	</script>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>

    $(".files").fileinput({
			maxFileCount: 1,
			validateInitialCount: true,
			language: 'en',
			showCaption: true,
			showPreview: true,
			showRemove: true,
			showUpload: false, // <------ just set this from true to false
			showCancel: true,
    	browseOnZoneClick: true,
			showUploadedThumbs: true
    });

    $('.person').hide();
    $('.no-person').hide();
    $('.person-and-no-person').hide();

    $( ".type" ).change(function() {
        setByType();
    });

    setByType();

    function setByType(){
			if( $( ".type" ).val() != '' ){
				$('.person-and-no-person').show();
				if( $( ".type" ).val() == 'คน' ){
					$('.person').show();
					$('.no-person').hide();
				}else{
					$('.person').hide();
					$('.no-person').show();
				}
			}
    }

    $('[value=""]').attr("disabled", true) 

    popupOK( JSON.parse('{!! $errors !!}') );
</script>
@endsection

