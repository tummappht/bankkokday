<div class="sidebar-overlay"></div>
<footer class="page-footer">
  {{-- © 2020 Copyright: --}}
  <div class="logo">
    <a href="/" class="mb-3">
      {!! file_get_contents('images/icons/icon-logo.svg') !!}
    </a>
  </div>
  <div class="navbar" id ='footer-menu'></div>
  <div class="navbar-ss">
    <a href="{{$contact->getDetailByTypeDetail('line')['detail_all']}}" target="_blank" rel="noreferrer">
      {!! file_get_contents('images/icons/icon-line.svg') !!}
    </a>
    <a href="{{$contact->getDetailByTypeDetail('facebook')['detail_all']}}" target="_blank" rel="noreferrer">
      {!! file_get_contents('images/icons/icon-facebook.svg') !!}
    </a>
  </div>
</footer>

<script>
  var icon_dash = <?php print_r(json_encode(file_get_contents('images/icons/icon-dash.svg'))); ?> ;
  $(document).ready(function () {
    $('#dismiss, .sidebar-overlay').on('click', function () {
      $('#sidebar').removeClass('active');
      $('.sidebar-overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
      $('#sidebar').addClass('active');
      $('.sidebar-overlay').addClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  });
</script>